*Performs the authors' analysis by reconstructing service counts based on the claims data
*This version uses the original methodology and data for the time estimates (and list of flagged physicians)


*change npi to string in the output data from the time analysis for merging with carrier claims data
use "$savedir/flaggedlist_old.dta",clear // list of people flagged at 100 hrs with total hours
tostring npi,replace
save "$savedir/flaggedlist_old.dta",replace
use "$savedir/flaggedlist_npi_old.dta",clear // list of npi's of flagged at 100 hrs
tostring npi,replace
save "$savedir/flaggedlist_npi_old.dta",replace

use "$savedir/phy_serv_time_old.dta",clear
tostring npi,replace
save "$savedir/phy_serv_time_old.dta",replace
use "$savedir/phy_time_old.dta",clear
tostring npi,replace
save "$savedir/phy_time_old.dta",replace
gen flag20=0
replace flag20=1 if totHrsPerWk_2013>20
rename impFlag1_2013 flagged
keep npi flagged flag20
save "$savedir/flaggedlist_old1.dta",replace // list of all physicians from the Utilization data analysis


*************************************************
*** RECONSTRUCT AGGREGATE UTILIZATION FROM THE CARRIER CLAIMS

use "$savedir/cardata.dta",clear
keep hcpcs npi mdfr_cd1 mdfr_cd2 srvc_cnt linepmt lalowchg

merge m:1 npi using "$savedir/flaggedlist_old1.dta"
keep if _merge==3 // drop all docs that don't appear in the utilization file
drop _merge

merge m:1 hcpcs using "$savedir/servTimeListold.dta",keepusing(tTotal)
keep if _merge==3
drop _merge

keep if tTotal>0 & tTotal!=.
drop tTotal

gen partial=0
replace partial=3 if mdfr_cd1=="53" | mdfr_cd2=="53" // discontinued procedures for patient safety
replace partial=1 if mdfr_cd1=="26" | mdfr_cd2=="26" // professional component
replace partial=2 if mdfr_cd1=="TC" | mdfr_cd2=="TC" // technical component


merge m:1 hcpcs partial using "$savedir/timelistold_partial.dta",keep(match master) keepusing(hcpcs partial)  // merge with time list that has the values for the partial codes
replace partial=0 if _merge==1
drop _merge

merge m:1 hcpcs partial using "$savedir/timelistold_partial.dta",keep(match) nogen keepusing(bilat_adj mult_adj glob_days)

gen bilat=0
replace bilat=1 if mdfr_cd1=="50" | mdfr_cd2=="50" // claim lines flagged with the bilateral modifier
replace bilat=1 if (mdfr_cd1=="LT" & mdfr_cd2=="RT") | (mdfr_cd1=="RT" & mdfr_cd2=="LT") // alternative way to indicate bilateral


gen pre_only=0
gen intra_only=0
gen post_only=0

replace pre_only=1 if mdfr_cd1=="56" | mdfr_cd2=="56" //pre-op only, change service count to percent pre-op
replace intra_only=1 if mdfr_cd1=="54" | mdfr_cd2=="54" // surgery only, change service count to percent intra-op
replace post_only=1 if mdfr_cd1=="55" | mdfr_cd2=="55" // post-op only, change service count to post-op percent


gen mp=0
replace mp=1 if (mdfr_cd1=="51" | mdfr_cd2=="51") & mult_adj==1 // these are flagged multiple procedures for codes that get the adjustment


gen tc=0
replace tc=1 if partial==2

gen discontinued=0
replace discontinued=1 if partial==3

*these are the results in table 1
gen globe=(glob_days=="10" | glob_days=="90") 

su discontinued tc mp pre_only intra_only post_only bilat srvc_cnt if flagged==0
su discontinued tc mp pre_only intra_only post_only bilat srvc_cnt if flagged==0 & flag20==1
su discontinued tc mp pre_only intra_only post_only bilat srvc_cnt if flagged==1

su srvc_cnt if globe==1 & flagged==0
su srvc_cnt if globe==1 & flagged==0 & flag20==1
su srvc_cnt if globe==1 & flagged==1

*these are the results in table 2
su srvc_cnt linepmt lalowchg if hcpcs=="66984" & post_only==1 & flagged==0
su srvc_cnt linepmt lalowchg if hcpcs=="66984" & post_only==1 & flagged==0 & flag20==1
su srvc_cnt linepmt lalowchg if hcpcs=="66984" & post_only==1 & flagged==1
count if srvc_cnt==1 & hcpcs=="66984" & post_only==1 & flagged==0
count if srvc_cnt==1 & hcpcs=="66984" & post_only==1 & flagged==0 & flag20==1
count if srvc_cnt==1 & hcpcs=="66984" & post_only==1 & flagged==1
su srvc_cnt linepmt lalowchg if hcpcs=="66821" & post_only==1 & flagged==0
su srvc_cnt linepmt lalowchg if hcpcs=="66821" & post_only==1 & flagged==0 & flag20==1
su srvc_cnt linepmt lalowchg if hcpcs=="66821" & post_only==1 & flagged==1
count if srvc_cnt==1 & hcpcs=="66821" & post_only==1 & flagged==0
count if srvc_cnt==1 & hcpcs=="66821" & post_only==1 & flagged==0 & flag20==1
count if srvc_cnt==1 & hcpcs=="66821" & post_only==1 & flagged==1
su srvc_cnt linepmt lalowchg if hcpcs=="66982" & post_only==1 & flagged==0
su srvc_cnt linepmt lalowchg if hcpcs=="66982" & post_only==1 & flagged==0 & flag20==1
su srvc_cnt linepmt lalowchg if hcpcs=="66982" & post_only==1 & flagged==1
count if srvc_cnt==1 & hcpcs=="66982" & post_only==1 & flagged==0
count if srvc_cnt==1 & hcpcs=="66982" & post_only==1 & flagged==0 & flag20==1
count if srvc_cnt==1 & hcpcs=="66982" & post_only==1 & flagged==1


*generate the cumulative stats by npi for the 5% claims sample
use "$savedir/cardata.dta",clear

keep npi lalowchg

egen temp1=sum(lalowchg),by(npi)  // total allowed charges by provider

egen doc=tag(npi) // tag and keep unique docs
keep if doc

merge m:1 npi using "$savedir/flaggedlist_old1.dta"
keep if _merge==3 // drop all docs that don't appear in the utilization file
drop _merge


/* This creates ratios based on the provider utilization summary table and includes 100% of services */

merge 1:1 npi using "/medicare/physician times/phys_totals.dta"
keep if _merge==3
drop _merge

gen medallowratio=temp1/totalmedicareallowedamount

gen medallowmult=1/medallowratio

keep npi  medallowmult medallowratio
save "$savedir/convert1.dta",replace


*aggregate up to provider/service for all and only accepted with no adjustment
use "$savedir/cardata.dta",clear

drop lbenpmt lprvpmt ldedamt lprpdamt coinamt

merge m:1 npi using "$savedir/flaggedlist_old1.dta"
keep if _merge==3 // drop all docs that don't appear in the utilization file
drop _merge

egen tot_allow=sum(lalowchg),by(npi)
egen tot_serv=sum(srvc_cnt),by(npi hcpcs) // sum service count by procedure and doc

egen temp=tag(npi hcpcs) // keep uniqu doc/procedure

keep if temp==1

keep npi hcpcs tot_serv tot_allow

save "$savedir/carrcounts1.dta",replace // carrier service counts by npi/hcpcs with no adjustments

merge m:1 hcpcs using "$savedir/servTimeListold.dta" // merge with serve time data
keep if _merge == 3	// _m=2: no such hcpcs filed, _m=1: the hcpcs is not Level I, Category 1
drop _merge

gen intHrs   = tot_serv*tIntra/60 // convert to hours
gen totHrs   = tot_serv*tTotal/60

egen intHrs_yr=sum(intHrs),by(npi) // total by physician for the year
egen totHrs_yr=sum(totHrs),by(npi)
egen temp=tag(npi)

keep if temp==1
drop intHrs totHrs 

gen intHrsPerWk   = intHrs_yr/51 // weekly hours
gen totHrsPerWk   = totHrs_yr/51

preserve

keep npi totHrsPerWk tot_allow
rename totHrsPerWk totHrsPerWk_noadj_old

save "$savedir/carr_times_noadj_noconv_old.dta",replace //these are the times for just the 5% sample w/o service count adjustment
restore

merge 1:1 npi using "$savedir/convert1.dta" // merge with conversion data
keep if _merge==3
drop _merge

gen totHrsPerWk_noadj=totHrsPerWk*medallowmult  // estimate of total hours for 100% claims based on total hours for 5% claims

keep npi totHrsPerWk_noadj

save "$savedir/carr_times_noadj_old.dta",replace


******************************************

*aggregate up to provider/service for accepted claims with adjustments
use "$savedir/cardata.dta",clear

drop lbenpmt lprvpmt ldedamt lprpdamt coinamt


merge m:1 npi using "$savedir/flaggedlist_old1.dta"
keep if _merge==3 // drop all docs that don't appear in the utilization file
drop _merge

*select procedures where srvc_cnt>1 are not possible. This list only includes relatively important codes and is far, far,far,far from being comprehensive. There are many cases where service counts can only be one.
replace srvc_cnt=1 if hcpcs=="90960" // 1 month of dialysis services with 4 or more face to face visits
replace srvc_cnt=1 if hcpcs=="99232" | hcpcs=="99233" // subsequent hospital visits, can only bill one per day, longer visits get a different code
replace srvc_cnt=1 if hcpcs=="99214" // office visit
replace srvc_cnt=1 if hcpcs=="17311" | hcpcs=="17313" // if legitimately performed multiple times, needs to be reported on a separate line with -59 modifier


*for partial claims, need to consider the type
gen partial=0
replace partial=3 if mdfr_cd1=="53" | mdfr_cd2=="53" // discontinued procedures for patient safety
replace partial=1 if mdfr_cd1=="26" | mdfr_cd2=="26" // professional component
replace partial=2 if mdfr_cd1=="TC" | mdfr_cd2=="TC" // technical component


merge m:1 hcpcs partial using "$savedir/timelistold_partial.dta",keep(match master) keepusing(hcpcs partial)  // merge with time list that has the values for the partial codes
replace partial=0 if _merge==1
drop _merge

merge m:1 hcpcs partial using "$savedir/timelistold_partial.dta",keep(match) nogen



replace srvc_cnt=1 if srvc_cnt>1 & (glob_days=="10" | glob_days=="90") // for major surgeries only srvc_cnt of 1 is possible, quantity is indicated with add-on hcpcs codes that don't have a global_days definition


gen bilat=0
replace bilat=1 if mdfr_cd1=="50" | mdfr_cd2=="50" // claim lines flagged with the bilateral modifier
replace bilat=1 if (mdfr_cd1=="LT" & mdfr_cd2=="RT") | (mdfr_cd1=="RT" & mdfr_cd2=="LT") // alternative way to indicate bilateral

replace srvc_cnt=1.5 if bilat==1 & bilat_adj==2 // this could increase times as most docs report srvc_cnt of 1, marked bilateral and eligible code for bilat adjustment
replace srvc_cnt=1 if (srvc_cnt==2 | bilat==1) & bilat_adj==1 // codes ineligible for bilat adjustment, 
* should be reported with service count of 1. Any additional quantity needs to be reported on separate line with the -59 modifier.
* or these codes are defined as bilateral procedures, so only have a service count of 1
replace srvc_cnt=2 if bilat==1 & bilat_adj==3 // these codes are treated as two separate procedures with no adjustment

replace srvc_cnt=pre_op if mdfr_cd1=="56" | mdfr_cd2=="56" //pre-op only, change service count to percent pre-op
replace srvc_cnt=intra_op if mdfr_cd1=="54" | mdfr_cd2=="54" // surgery only, change service count to percent intra-op
replace srvc_cnt=post_op if mdfr_cd1=="55" | mdfr_cd2=="55" // post-op only, change service count to post-op percent

drop pre_op intra_op post_op

gen mp=0
replace mp=1 if mdfr_cd1=="51" | mdfr_cd2=="51" // these are flagged multiple procedures
*the mp adjustment is easy, just divide service count by 2

replace srvc_cnt=srvc_cnt/2 if mp==1 & mult_adj==1 // flagged multiple procedure codes that are eligible for the adjustment


save "$savedir/claims_adjold.dta",replace


*now aggregate up to the npi/hcpcs/partial,partial1 flag level

egen tot_serv=sum(srvc_cnt),by(npi hcpcs partial) // calculate total service counts


egen temp=tag(npi hcpcs partial)

keep if temp==1

keep npi hcpcs tot_serv tTotal

gen totHrs   = tot_serv*tTotal/60

egen totHrs_yr=sum(totHrs),by(npi)
egen temp=tag(npi)

keep if temp==1
drop totHrs 

gen totHrsPerWk   = totHrs_yr/51

preserve

keep npi totHrsPerWk
rename totHrsPerWk totHrsPerWk_adj_noconv_old

save "$savedir/carr_times_adj_noconv_old.dta",replace

restore

merge 1:1 npi using "$savedir/convert1.dta"
keep if _merge==3
drop _merge

gen totHrsPerWk1=totHrsPerWk*medallowmult

save "$savedir/carr_times_adjold.dta",replace
keep if totHrsPerWk1>100
keep npi
save "$tempdir/flagstill_old.dta",replace


use "$savedir/carr_times_adjold.dta",clear

merge 1:1 npi using "$savedir/carr_times_noadj_old.dta" // merge with the unadjusted times
keep if _merge==3
drop _merge

merge 1:1 npi using "$savedir/flaggedlist_npi_old.dta" // flagged docs in original paper

gen flag_old=0
replace flag_old=1 if _merge==3
drop _merge

save "$savedir/carr_times_old.dta",replace

*these are the counts that go into the table in the paper

count if totHrsPerWk_noadj>20 & totHrsPerWk_noadj!=.
count if totHrsPerWk_noadj>80 & totHrsPerWk_noadj!=.
count if totHrsPerWk_noadj>100 & totHrsPerWk_noadj!=.
count if totHrsPerWk_noadj>112 & totHrsPerWk_noadj!=.
count if totHrsPerWk_noadj>168 & totHrsPerWk_noadj!=.
count if totHrsPerWk_noadj!=.
count if totHrsPerWk_noadj>20 & totHrsPerWk_noadj!=. & flag_old==1
count if totHrsPerWk_noadj>80 & totHrsPerWk_noadj!=. & flag_old==1
count if totHrsPerWk_noadj>100 & totHrsPerWk_noadj!=. & flag_old==1
count if totHrsPerWk_noadj>112 & totHrsPerWk_noadj!=. & flag_old==1
count if totHrsPerWk_noadj>168 & totHrsPerWk_noadj!=. & flag_old==1
count if flag_old==1

count if totHrsPerWk1>20 & totHrsPerWk1!=.
count if totHrsPerWk1>80 & totHrsPerWk1!=.
count if totHrsPerWk1>100 & totHrsPerWk1!=.
count if totHrsPerWk1>112 & totHrsPerWk1!=.
count if totHrsPerWk1>168 & totHrsPerWk1!=.
count if totHrsPerWk1!=.

count if totHrsPerWk1>20 & totHrsPerWk1!=. & flag_old==1
count if totHrsPerWk1>80 & totHrsPerWk1!=. & flag_old==1
count if totHrsPerWk1>100 & totHrsPerWk1!=. & flag_old==1
count if totHrsPerWk1>112 & totHrsPerWk1!=. & flag_old==1
count if totHrsPerWk1>168 & totHrsPerWk1!=. & flag_old==1


merge 1:1 npi using "$savedir/phy_time_old.dta",keepusing(totHrsPerWk_2013)
keep if _merge==3
drop _merge

su totHrsPerWk1 totHrsPerWk_noadj totHrsPerWk_2013 medallowratio medallowmult

corr totHrsPerWk1 totHrsPerWk_noadj totHrsPerWk_2013


