*Performs the authors' analysis by reconstructing service counts based on the claims data
*This version uses the original methodology and data for the time estimates (and list of flagged physicians)


*run car_analysis_oldoldtimes.do first

*************************************************
*** RECONSTRUCT AGGREGATE UTILIZATION FROM THE CARRIER CLAIMS


*aggregate up to provider/service for all and only accepted with no adjustment
use "$savedir/cardata.dta",clear

drop lbenpmt lprvpmt ldedamt lprpdamt coinamt

merge m:1 npi using "$savedir/flaggedlist_old1.dta"
keep if _merge==3 // drop all docs that don't appear in the utilization file
drop _merge

egen tot_allow=sum(lalowchg),by(npi)
egen tot_serv=sum(srvc_cnt),by(npi hcpcs) // sum service count by procedure and doc

egen temp=tag(npi hcpcs) // keep uniqu doc/procedure

keep if temp==1

keep npi hcpcs tot_serv tot_allow

save "$savedir/carrcounts1.dta",replace // carrier service counts by npi/hcpcs with no adjustments

merge m:1 hcpcs using "$savedir/servTimeListold.dta" // merge with serve time data
keep if _merge == 3	// _m=2: no such hcpcs filed, _m=1: the hcpcs is not Level I, Category 1
drop _merge

gen intHrs   = tot_serv*tIntra/60 // convert to hours
gen totHrs   = tot_serv*tTotal/60

egen intHrs_yr=sum(intHrs),by(npi) // total by physician for the year
egen totHrs_yr=sum(totHrs),by(npi)
egen temp=tag(npi)

keep if temp==1
drop intHrs totHrs 

gen intHrsPerWk   = intHrs_yr/51 // weekly hours
gen totHrsPerWk   = totHrs_yr/51


keep npi totHrsPerWk tot_allow
rename totHrsPerWk totHrsPerWk_noadj

save "$savedir/carr_times_noadj_noconv_old.dta",replace


******************************************

*aggregate up to provider/service for accepted claims with adjustments
use "$savedir/claims_adjold.dta",clear

*now aggregate up to the npi/hcpcs/partial,partial1 flag level

egen tot_serv=sum(srvc_cnt),by(npi hcpcs partial) // calculate total service counts


egen temp=tag(npi hcpcs partial)

keep if temp==1

keep npi hcpcs tot_serv tTotal

gen totHrs   = tot_serv*tTotal/60

egen totHrs_yr=sum(totHrs),by(npi)
egen temp=tag(npi)

keep if temp==1
drop totHrs 

gen totHrsPerWk   = totHrs_yr/51


save "$savedir/carr_times_adjold_noconv.dta",replace

		    
merge 1:1 npi using "$savedir/carr_times_noadj_noconv_old.dta" // merge with the unadjusted times
keep if _merge==3
drop _merge

save "$savedir/carr_times_noconv.dta",replace

su totHrsPerWk totHrsPerWk_noadj,detail
centile totHrsPerWk totHrsPerWk_noadj,c(99.5 99.9)


gen qbias05=totHrsPerWk_noadj-totHrsPerWk

gen pqbias05=100*(totHrsPerWk_noadj-totHrsPerWk)/totHrsPerWk_noadj
replace pqbias05=0 if pqbias05==. & qbias==0

keep npi qbias05 pqbias05 totHrsPerWk_noadj totHrsPerWk tot_allow

save "$savedir/quantity_adj_bias.dta",replace

merge 1:1 npi using "$savedir/phy_time_old.dta",nogen


merge 1:1 npi using "$savedir/carr_times_old",nogen
mvencode totHrsPerWk_noadj_pay totHrsPerWk_noadj_allow totHrsPerWk_pay totHrsPerWk_allow medallowratio,mv(0) o
keep npi qbias05 pqbias05 nCode_2013 nServ_2013 nBene_2013 totHrsPerWk_2013 totPaid_2013 male phyCred isMD multiCred medSchool gradYear experYear phySpec phyType groupPAC nPhyInGroup hosAff inMedicare inERX inPQRS inEHR anyEM phyZip GPCIregion GPCIregion_vague addState medallowratio medpaymult medallowmult totHrsPerWk_allow totHrsPerWk_pay totHrsPerWk_noadj_allow totHrsPerWk_noadj_pay totHrsPerWk_noadj totHrsPerWk totAllow_2013



save "$savedir/bias_test.dta",replace


gen flag=(totHrsPerWk_2013>=100)
gen flag20=0
replace flag20=1 if flag==0 & totHrsPerWk_2013>=20

su qbias05 pqbias05 if flag==1
su qbias05 pqbias05 if flag==0
su qbias05 pqbias05 if flag==0 & flag20==1




