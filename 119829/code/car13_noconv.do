*Analysis of the 5% sample with no conversion

*using the old times - what's in the paper

use "$savedir/carr_times_noadj_noconv_old.dta",clear
rename totHrsPerWk_noadj_old totHrsPerWk_noadj_noconv_old
merge 1:1 npi using "$savedir/carr_times_adj_noconv_old.dta",nogen

save "$savedir/carr_times_noconv_old.dta",replace

*these are the results for table 3
su totHrsPerWk_adj_noconv_old totHrsPerWk_noadj_noconv_old,detail
centile totHrsPerWk_adj_noconv_old totHrsPerWk_noadj_noconv_old,c(99.5 99.9)


gen qbias05=totHrsPerWk_noadj_noconv_old-totHrsPerWk_adj_noconv_old

gen pqbias05=100*(totHrsPerWk_noadj_noconv_old-totHrsPerWk_adj_noconv_old)/totHrsPerWk_noadj_noconv_old
replace pqbias05=0 if pqbias05==. & qbias05==0

keep npi qbias05 pqbias05 totHrsPerWk_noadj_noconv_old totHrsPerWk_adj_noconv_old tot_allow

save "$savedir/quantity_adj_bias.dta",replace



merge 1:1 npi using "$savedir/phy_time_old.dta",nogen


merge 1:1 npi using "$savedir/carr_times_old",nogen
mvencode totHrsPerWk_noadj totHrsPerWk1 medallowratio,mv(0) o
keep npi qbias05 pqbias05 nCode_2013 nServ_2013 nBene_2013 totHrsPerWk_2013 totPaid_2013 male phyCred /*
*/ isMD multiCred medSchool gradYear experYear phySpec phyType groupPAC nPhyInGroup hosAff inMedicare inERX /*
*/ inPQRS inEHR anyEM phyZip GPCIregion GPCIregion_vague addState medallowratio medallowmult /*
*/ totHrsPerWk1 totHrsPerWk_noadj totHrsPerWk_noadj_noconv_old totHrsPerWk_adj_noconv_old totAllow_2013


save "$savedir/bias_test.dta",replace

*these are the results in table 4
gen flag=(totHrsPerWk_2013>=100)
gen flag20=0
replace flag20=1 if flag==0 & totHrsPerWk_2013>=20

su qbias05 pqbias05 if flag==1
su qbias05 pqbias05 if flag==0
su qbias05 pqbias05 if flag==0 & flag20==1




*using the new times (not reported)


use "$savedir/carr_times_noadj_noconv_new.dta",clear

merge 1:1 npi using "$savedir/carr_times_adj_noconv_new.dta",nogen

save "$savedir/carr_times_noconv_new.dta",replace

su totHrsPerWk_adj_noconv_new totHrsPerWk_noadj_noconv_new,detail
centile totHrsPerWk_adj_noconv_new totHrsPerWk_noadj_noconv_new,c(99.5 99.9)


gen qbias05_new=totHrsPerWk_noadj_noconv_new-totHrsPerWk_adj_noconv_new

gen pqbias05_new=100*(totHrsPerWk_noadj_noconv_new-totHrsPerWk_adj_noconv_new)/totHrsPerWk_noadj_noconv_new
replace pqbias05_new=0 if pqbias05_new==. & qbias05_new==0

keep npi qbias05_new pqbias05_new totHrsPerWk_noadj_noconv_new totHrsPerWk_adj_noconv_new tot_allow

save "$savedir/quantity_adj_bias_new.dta",replace



merge 1:1 npi using "$savedir/phy_time_2.dta",nogen


merge 1:1 npi using "$savedir/carr_times2",nogen

rename totHrsPerWk_all totHrsPerWk1
rename totHrsPerWk_noadj_all totHrsPerWk_noadj

mvencode totHrsPerWk_noadj totHrsPerWk1 medallowratio,mv(0) o
keep npi qbias05 pqbias05 nCode_2013 nServ_2013 nBene_2013 totHrsPerWk_2013 totPaid_2013 male phyCred /*
*/ isMD multiCred medSchool gradYear experYear phySpec phyType groupPAC nPhyInGroup hosAff inMedicare inERX /*
*/ inPQRS inEHR anyEM phyZip GPCIregion GPCIregion_vague addState medallowratio medallowmult /*
*/ totHrsPerWk1 totHrsPerWk_noadj totHrsPerWk_noadj_noconv_old totHrsPerWk_adj_noconv_old totAllow_2013



save "$savedir/bias_test_new.dta",replace

drop flag
gen flag=(totHrsPerWk_2013>=100)
gen flag20=0
replace flag20=1 if flag==0 & totHrsPerWk_2013>=20

su qbias05_new pqbias05_new if flag==1
su qbias05_new pqbias05_new if flag==0
su qbias05_new pqbias05_new if flag==0 & flag20==1
