clear

cd "/medicare/Extracted Files/car/car_2013"

import delimited car_linej_lds_5_2013.csv


rename v1 dsysrtky
rename v2 claimno
rename v3 line_num
rename v4 thru_dt
rename v5 clm_type
rename v6 prf_prfl
rename v7 prf_upin
rename v8 prf_npi
rename v9 prgrpnpi
rename v10 prv_type
rename v11 prvstate
rename v12 hcfaspcl
rename v13 prtcptg
rename v14 astnt_cd
rename v15 srvc_cnt
rename v16 typsrvcb
rename v17 plcsrvc
rename v18 lclty_cd
rename v19 expnsdt2
rename v20 hcpcs_cd
rename v21 mdfr_cd1
rename v22 mdfr_cd2
rename v23 betos
rename v24 linepmt
rename v25 lbenpmt
rename v26 lprvpmt
rename v27 ldedamt
rename v28 lprpaycd
rename v29 lprpdamt
rename v30 coinamt
rename v31 lsbmtchg
rename v32 lalowchg
rename v33 prcngind
rename v34 pmtindsw
rename v35 ded_sw
rename v36 mtus_cnt
rename v37 mtus_ind
rename v38 line_icd_dgns_cd
rename v39 line_icd_dgns_vrsn_cd
rename v40 hcthgbrs
rename v41 hcthgbtp
rename v42 lnndccd
rename v43 carr_line_clia_lab_num
rename v44 carr_line_ansthsa_unit_cnt

drop line_icd_dgns_vrsn_cd

save car_line_2013.dta,replace


