************************************
**
** Figure 1: distribution of hours-per-week
**
************************************
use "$datadir/phy_time.dta", clear

keep totHrsPerWk_2012 totHrsPerWk_2013
gen id = _n
reshape long totHrsPerWk_, i(id) j(year)
ren totHrsPerWk_ totHrsPerWk
drop id

gen totHPWhist = totHrsPerWk
replace totHPWhist=168 if totHPWhist>168 & totHPWhist!=.

* histogram
twoway__histogram_gen totHPWhist if totHPWhist>20, frequency width(1) gen(h x)
twoway bar h x, bstyle(histogram) ///
	xline(100) ///
	text(-900 168 "168", place(s) c(red)) ///
	xtitle("Estimated hours per week") ///
	note("Note: bandwidth=1; hours<=20 not shown; mass at 168 represents hours>=168.") ///
	title("(a) Distribution of estimated hours per week") ///
	graphregion(color(white)) plotregion(color(white)) scheme(s2mono) ///
	saving("$tempdir/fig1_pdf", replace)

* empirical cdf
keep if totHPWhist>20
local nGrids = 1000
local nPoints = `nGrids'+1
gen cdf = _n/`nGrids' if _n<=`nGrids'
pctile totHPW_cdf = totHPWhist, nq(`nPoints')

graph twoway line cdf totHPW_cdf, xline(100) ///
	xtitle("Estimated hours per week") ///
	title("(b) Empirical cdf of estimated hours per week") ///
	graphregion(color(white)) plotregion(color(white)) scheme(s2mono) ///
	saving("$tempdir/fig1_cdf", replace)

graph combine "$tempdir/fig1_pdf" "$tempdir/fig1_cdf", ///
	 xcommon cols(1) xsize(12) ysize(16) ///
	graphregion(color(white)) plotregion(color(white)) scheme(s2mono)
graph export "$tabdir/fig1_TimeDistn.eps", replace

