************************************
**
** Figure 2: distribution of HCPCS flag Index
**
************************************
use "$datadir/phy_serv_time_FI.dta", clear

local ixFlag2012 $flag_2012
local ixFlag2013 $flag_2013
local ixFlag = substr("$flag_2012",1,8)

* reduce npi-hcpcs level to hcpcs level
keep fpCode_`ixFlag2012' fpCode_`ixFlag2013' nServ* nBene* hcpcs
collapse (mean) fpCode* (sum) nServ* nBene*, by(hcpcs)

* add payment info
preserve
use "$datadir/phy_serv_merged.dta", clear
gen totPaid_2012 = nServ*meanPaid
gen totPaid_2013 = nServ2013*meanPaid2013
collapse (sum) totPaid_* , by(hcpcs)
save "$tempdir/hcpcsPayment.dta", replace
restore

merge 1:1 hcpcs using "$tempdir/hcpcsPayment.dta", keep(match master)
drop _merge

* merge payment info with fp
ren nServ nServ2012
ren nBene nBene2012
reshape long nServ nBene fpCode_`ixFlag'_ totPaid_, i(hcpcs) j(year)
ren fpCode_`ixFlag'_ fpCode
ren totPaid_ totPaid
replace totPaid = int(totPaid)

* only keep those with Index strictly between (0, 100)
keep if fpCode>0 & fpCode<100

* distribution
histogram fpCode , bin(50) ///
	title("(a) Unweighted") ///
	xtitle("Code Flag Index")  ///
	density ylabel(0(0.01)0.08) ///
	graphregion(color(white)) plotregion(color(white)) scheme(s2mono) ///
	saving("$tempdir/fig2_unwt", replace)

* ... weighted using volume
histogram fpCode [fweight = nServ], bin(50) ///
	title("(b) Weighted by serivce volume") ///
	xtitle("Code Flag Index") ///
	density ylabel(0(0.01)0.08) ///
	graphregion(color(white)) plotregion(color(white)) scheme(s2mono) ///
	saving("$tempdir/fig2_wt_vol", replace)

histogram fpCode [fweight = nBene], bin(50) ///
	title("(*) Weighted by number of patients") ///
	xtitle("Code Flag Index") ///
	density ylabel(0(0.01)0.08) ///
	graphregion(color(white)) plotregion(color(white)) scheme(s2mono) ///
	saving("$tempdir/fig2_wt_bene", replace)

histogram fpCode [fweight = totPaid], bin(50) ///
	title("(c) Weighted by total Medicare reimbursement") ///
	xtitle("Code Flag Index") ///
	density ylabel(0(0.01)0.08) ///
	graphregion(color(white)) plotregion(color(white)) scheme(s2mono) ///
	saving("$tempdir/fig2_wt_pay", replace)
	
graph combine "$tempdir/fig2_unwt" "$tempdir/fig2_wt_vol" "$tempdir/fig2_wt_pay", ///
	 title("")  ///
	 xcommon cols(1) xsize(10) ysize(20) ///
	graphregion(color(white)) plotregion(color(white)) scheme(s2mono)
graph export "$tabdir/fig3_CFI_dist_`ixFlag'.eps", replace
