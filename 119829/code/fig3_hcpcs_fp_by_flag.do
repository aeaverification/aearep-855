************************************
**
** Figure 3: distribution of hcpcs Flag Index by group
**
************************************
local ixFlag = substr("$flag_2012",1,8)

use "$datadir/phy_serv_time_FI.dta", clear

* restructure data
keep hcpcs fpCode_`ixFlag'_2012 fpCode_`ixFlag'_* $flag_2012 $flag_2013 nServ* nBene*
gen id = _n
ren nServ nServ2012
ren nBene nBene2012
reshape long `ixFlag'_ fpCode_`ixFlag'_ nServ nBene, i(id hcpcs) j(year)
ren `ixFlag'_ `ixFlag'
ren fpCode_`ixFlag'_ fpCode

* reduce npi-hcpcs level to hcpcs level
collapse (mean) fpCode (sum) nServ nBene, by(hcpcs year `ixFlag')

* only keep those with Index strictly between (0, 100)
keep if fpCode>0 & fpCode<100

twoway__histogram_gen fpCode if `ixFlag'==1 [fweight=nServ], density width(5) gen(h1 x1)
twoway__histogram_gen fpCode if `ixFlag'==0 [fweight=nServ], density width(5) gen(h0 x0)
	
twoway (line h1 x1, lwidth(0.5)) ///
	(line h0 x0, lwidth(0.5)), xtitle("Code Flag Index", ) ///
	title("") ///
	ylabel(,angle(0)) ///
	legend(label(1 "Flagged physicians") label(2 "Unflagged physicians")) ///
	graphregion(color(white)) plotregion(color(white)) scheme(s2mono)
graph export "$tabdir/fig4_CFIby_`ixFlag'.eps", replace


/* This histogram is ploting the dist'n of FIndex of hcpcs codes filed by flagged physicians,
vs. that of codes filed by other physicians, 
both weighted by TOTAL service volume furnished by ALL physicians in that group (flagged or not).
*/
