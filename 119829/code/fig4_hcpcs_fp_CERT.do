************************************
**
** Figure 4: HCPCS flag Index vs CERT result
**
************************************

local ixFlag = substr("$flag_2012",1,8)

* prepare hcpcs-level total payment data
* ... list of physicians who billed over 20 hours in any year
use "$datadir/phy_time.dta", clear
keep if totHrsPerWk_2012 > 20 | totHrsPerWk_2013 > 20
duplicates drop npi, force
save "$tempdir/subSample_above20hrs.dta", replace

* ... npi-hcpcs-level payment sums
use "$datadir/phy_serv_merged.dta", clear
merge m:1 npi using "$tempdir/subSample_above20hrs.dta", keep(matched)
drop _merge
collapse (sum) nServ totPaid_2012, by(hcpcs)
save "$tempdir/totPaid_2012_hcpcs.dta", replace

* MASTER file: npi-hcpcs level data with flag index
use "$datadir/phy_serv_time_FI.dta", clear

* reduce npi-hcpcs level to hcpcs level [ONLY 2012 - CERT 2013 decisions not final yet]
keep hcpcs prFlag_`ixFlag'_2012 fpCode_`ixFlag'_2012
duplicates drop hcpcs, force

merge 1:1 hcpcs using "$tempdir/totPaid_2012_hcpcs.dta", keep(match master) keepusing(totPaid_2012 nServ)
drop _merge

* merge with CERT result
merge 1:m hcpcs using "$rootdir/input/CERT/FlagPropenHcpcs_CERT.dta", keep(master match)
drop _m

* compare FP vs CERT_FP
*... correlation coefficients
local cmsFP fpCode_`ixFlag'_2012
corr `cmsFP' fpCERT if fpCERT>0 & fpCERT<100 & `cmsFP'>0 & `cmsFP'<100
mat tabstatR  = r(C)
local rho_unwt = tabstatR[2,1]
local rho_unwt = substr("`rho_unwt'",1,5)

twoway (scatter `cmsFP' fpCERT [aweight = totPaid] if `cmsFP'>0 & fpCERT>0 & `cmsFP'<100 & fpCERT<100, m(Oh) xline(50) yline(50) ///
	text(98 75 "Correlation coefficient = `rho_unwt'", place(c) just(left))) ///
	(function y = x, range(0 100) ), ///
	legend(off) xtitle("CERT Disapproval Index") ytitle("Code Flag Index") ///
	graphregion(color(white)) plotregion(color(white)) scheme(s2mono)

graph export "$tabdir/fig6_vs_CERT_`ixFlag'.eps", replace
