************************************
**
** Table 5.8: upcoding factor
**
************************************
local ixFlag = substr("$flag_2012",1,8)

use "$datadir/phy_time.dta", clear

* FOCUS ON THE SUBSAMPLE WITH 20+ HOURS/WEEK
keep if totHrsPerWk_2012>20 | totHrsPerWk_2013>20

* prep to merge with hrr data
tostring phyZip, replace
gen str5 zip5 = substr(phyZip,1,5) if length(phyZip)==9
replace zip5 = "0"+substr(phyZip,1,4) if length(phyZip)==8
replace zip5 = "00"+substr(phyZip,1,3) if length(phyZip)==7
replace zip5 = phyZip if length(phyZip)==5
replace zip5 = "0"+phyZip if length(phyZip)==4
replace zip5 = "00"+phyZip if length(phyZip)==3

* link to hrr
merge m:1 zip5 using "$datadir/zip_hrr.dta", keep(master matched)

* create vars of interest
keep npi male isMD experYear phyType hrr totHrsPerWk_* totPaid_* $flag_2012 $flag_2013
reshape long totHrsPerWk_ totPaid_ `ixFlag'_, i(npi male isMD experYear phyType hrr) j(year)
ren totHrsPerWk_ totHrsPerWk
ren totPaid_ totPaid
ren `ixFlag'_ `ixFlag'

* STEP 1: "FAIR HOURLY RATE" (using unflagged only)
gen hrate = totPaid*1000/totHrsPerWk/52

xi: reg hrate male isMD experYear i.year i.phyType i.hrr if `ixFlag'==0
predict hrate_hat

* STEP 2: UPCODING FACTOR 1
gen upfact1 = totPaid*1000/hrate_hat/8/365

* STEP 2': UPCODING FACTOR 2
gen upfact2 = hrate_hat/hrate 

* DISTRIBUTION PLOTS

* ... UPCODING FACTOR 1
histogram upfact1 if `ixFlag' == 1 & upfact1<=20,  width(0.2) ///
	xtitle("Flagged physicians") ///
	density ///
	graphregion(color(white)) plotregion(color(white)) scheme(s2mono) ///
	saving("$tempdir/fig5a_flagged", replace)

histogram upfact1 if `ixFlag' == 0 & upfact1<=20,  width(0.2) ///
	xtitle("Unflagged physicians")  ///
	density ///
	graphregion(color(white)) plotregion(color(white)) scheme(s2mono) ///
	saving("$tempdir/fig5a_unflagged", replace)
	
graph combine "$tempdir/fig5a_flagged" "$tempdir/fig5a_unflagged", ///
	 title("(a) Distribution of Overbilling Potential Factor 1")  ///
	 ycommon cols(2) xsize(14) ysize(7) ///
	graphregion(color(white)) plotregion(color(white)) scheme(s2mono) ///
	saving("$tempdir/fig5a_upfact1", replace)

* ... UPCODING FACTOR 2 (actual vs predicted wages)
twoway (scatter hrate_hat hrate if `ixFlag'==1 & hrate<500, ///
	m(p) jitter(1)) ///
	(function y = x, range(0 400) lwidth(0.5) lstyle(solid)), ///
	legend(off) xsize(15) ysize(10) ///
	xtitle("Reported hourly revenue ($)" "flagged physicians") ///
	ytitle("Predicted hourly revenue ($)") ///
	graphregion(color(white)) plotregion(color(white)) scheme(s2mono) ///
	saving("$tempdir/fig5b_flagged", replace)
	
twoway (scatter hrate_hat hrate if `ixFlag'==0 & hrate<500, ///
	m(p)) ///
	(function y = x, range(0 400) lwidth(0.5) lstyle(solid)), ///
	legend(off) xsize(15) ysize(10) ///
	xtitle("Reported hourly revenue ($)" "unflagged physicians") ///
	ytitle("Predicted hourly revenue ($)") ///
	graphregion(color(white)) plotregion(color(white)) scheme(s2mono) ///
	saving("$tempdir/fig5b_unflagged", replace)	

graph combine "$tempdir/fig5b_flagged" "$tempdir/fig5b_unflagged", ///
	 title("(b) Reported and predicted hourly revenues")  ///
	 ycommon cols(2) xsize(14) ysize(7) ///
	graphregion(color(white)) plotregion(color(white)) scheme(s2mono) ///
	saving("$tempdir/fig5b_upfact2", replace)	
	
* ... UPCODING FACTOR 2
histogram upfact2 if `ixFlag' == 1 & upfact2<=20, width(0.2) ///
	xtitle("Flagged physicians") ///
	density ///
	graphregion(color(white)) plotregion(color(white)) scheme(s2mono) ///
	saving("$tempdir/fig5c_flagged", replace)

histogram upfact2 if `ixFlag' == 0 & upfact2<=20, width(0.2)  ///
	xtitle("Unflagged physicians")  ///
	density ///
	graphregion(color(white)) plotregion(color(white)) scheme(s2mono) ///
	saving("$tempdir/fig5c_unflagged", replace)
	
graph combine "$tempdir/fig5c_flagged" "$tempdir/fig5c_unflagged", ///
	 title("(c) Distribution of Overbilling Potential Factor 2")  ///
	 ycommon cols(2) xsize(14) ysize(7) ///
	graphregion(color(white)) plotregion(color(white)) scheme(s2mono) ///
	saving("$tempdir/fig5c_upfact2", replace)
	
graph combine "$tempdir/fig5a_upfact1" "$tempdir/fig5b_upfact2" "$tempdir/fig5c_upfact2", ///
	ycommon cols(1) xsize(14) ysize(20) ///
	graphregion(color(white)) plotregion(color(white)) scheme(s2mono)
graph export "$tabdir/fig5_OPF_`ixFlag'.eps", replace
