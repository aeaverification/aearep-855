************************************
**
** Figure: HCPCS flag Index vs flag probability
**
************************************

local ixFlag = substr("$flag_2012",1,8)

* prepare hcpcs-level total payment data
* ... list of physicians who billed over 20 hours in any year
use "$datadir/phy_time.dta", clear
keep if totHrsPerWk_2012>20 | totHrsPerWk_2013>20
duplicates drop npi, force
save "$tempdir/subSample_above20hrs.dta", replace

* ... npi-hcpcs-level payment sums
use "$datadir/phy_serv_merged.dta", clear
merge m:1 npi using "$tempdir/subSample_above20hrs.dta", keep(matched)
drop _merge
collapse (sum) nServ totPaid_2012, by(hcpcs)
save "$tempdir/totPaid_2012_hcpcs.dta", replace

* MASTER file: npi-hcpcs level data with flag index
use "$datadir/phy_serv_time_FI.dta", clear

* reduce npi-hcpcs level to hcpcs level [ONLY 2012 - CERT 2013 decisions not final yet]
keep hcpcs prFlag_`ixFlag'_2012 fpCode_`ixFlag'_2012
duplicates drop hcpcs, force
replace prFlag_`ixFlag'_2012  = prFlag_`ixFlag'_2012*100

merge 1:1 hcpcs using "$tempdir/totPaid_2012_hcpcs.dta", keep(match master) keepusing(totPaid_2012 nServ)
drop _merge

* compare FP vs Pr(flag)
twoway (scatter fpCode_`ixFlag'_2012 prFlag_`ixFlag'_2012 [aweight = totPaid], m(Oh)) ///
	(function y = x, range(0 100) ), ///
	legend(off) xtitle("Probability Being Filed by Flagged Physicians") ytitle("Code Flag Index") ///
	graphregion(color(white)) plotregion(color(white)) scheme(s2mono)
	
graph export "$tabdir/fig2_fp_pr_`ixFlag'.eps", replace
