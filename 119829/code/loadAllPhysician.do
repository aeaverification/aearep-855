************************************
****							****
****    loadAllPhysician.do		****
****							****
************************************

************************************
*		housekeeping
************************************
clear all
local loadraw  = 1

version 11.2
include "config.do"


local home : env HOME
if "`home'"=="" {
	local home : env HOMEPATH
}
// global rootdir "`home'/Documents/Workplace/Medicare_payment"
global datadir "$rootdir/input"
global tempdir "/tmp"
global savedir "$rootdir/output"
global tabdir "$rootdir/tex"

cap log close
log using "$rootdir/code/loadAllPhysician.txt", text replace

set more off

************************************
* load data (too big, load in batches)
************************************
if `loadraw'==1 {
	foreach batch in A-G H-N O-Z {
		insheet using "$datadir/National_physician_`batch'.csv", clear
		gen zipcode5 = .
			replace zipcode5 = zipcode if zipcode<=99999
			replace zipcode5 = int(zipcode/10000) if zipcode>99999
		duplicates drop npi grouppracticepacid zipcode5, force

		* destring variables, drop irrelevant variables to reduce file size
		drop middlename suffix line2streetaddress markerofaddressline2suppression organizationlegalname claimsbasedhospitalaffiliationlb v26 v28 v30 v32
				 
		destring grouppracticepacid, replace ignore(" ") force
		
		drop if gender == ""
		gen byte male = (gender=="M")
		drop gender
		
		replace professionalacceptsmedicareassig = "0" if professionalacceptsmedicareassig=="M"
		replace professionalacceptsmedicareassig = "1" if professionalacceptsmedicareassig=="Y"
		destring professionalacceptsmedicareassig, replace
		
		replace participatinginerx = "0" if participatinginerx=="N"
		replace participatinginerx = "1" if participatinginerx=="Y"
		destring participatinginerx, replace
		
		replace participatinginpqrs = "0" if participatinginpqrs=="N"
		replace participatinginpqrs = "1" if participatinginpqrs=="Y"
		destring participatinginpqrs, replace
		
		replace participatinginehr = "0" if participatinginehr=="N"
		replace participatinginehr = "1" if participatinginehr=="Y"
		destring participatinginehr, replace

		save "$tempdir/temp`batch'.dta", replace
	}	
}
	
************************************
* combine and clean
************************************
use "$tempdir/tempA-G.dta", clear
append using "$tempdir/tempH-N.dta"
append using "$tempdir/tempO-Z.dta"

rename credential phyCred
rename professionalenrollmentid peid
rename graduationyear gradYear
rename primaryspecialty phySpec1
rename allsecondaryspecialties phySpec2
rename grouppracticepacid groupPAC

rename numberofgrouppracticemembers nPhyInGroup
rename line1streetaddress addStreet
rename city addCity
rename state addState
rename zipcode phyZip
rename zipcode5 phyZip5
rename claimsbasedhospitalaffiliationcc hosCCN1
rename v25 hosCCN2
rename v27 hosCCN3
rename v29 hosCCN4
rename v31 hosCCN5
rename professionalacceptsmedicareassig inMedicare
rename participatinginerx inERX
rename participatinginpqrs inPQRS
rename participatinginehr inEHR

* numeric medical school
egen medSchool = group(medicalschoolname), label

* suspect graduation year
replace gradYear = gradYear+100 if int(gradYear/100)==18
replace gradYear = gradYear+200 if int(gradYear/100)==17

order npi pacid peid lastname firstname male phyCred medSchool gradYear ///
	phySpec1 phySpec2 groupPAC nPhyInGroup addStreet addCity addState phyZip phyZip5

desc
sum, sep(0)
codebook npi
sort npi phyZip

save "$savedir/physician_info.dta", replace

************************************
*		housekeeping
************************************

log close
