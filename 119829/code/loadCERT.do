*********************************
**	Housekeeping
*********************************

version 11.2
include "config.do"
clear all


local home : env HOME
if "`home'"=="" {
	local home : env HOMEPATH
}
// global rootdir "`home'/Documents/Workplace/Medicare_payment"
global datadir "$rootdir/input"
global tempdir "/tmp"
global savedir "$rootdir/output"
global tabdir "$rootdir/tex"

set more off


************************************
*	load data & select sample
************************************
forval y=2013/2014 {
	insheet using "$datadir/CERT_FY`y'.csv", clear names

	* sample selection - time
	gen cyear = substr(servicefromdate,7,4)
	keep if cyear=="2012" | cyear=="2013"

	* clean
	ren hcpcsprocedurecode hcpcs
	ren servicefromdate date_start
	gen byte isMiscoded = reviewdecision=="Disagree"
	egen byte errType = group(errorcode), label
	recode errType(1=0)

	* save
	keep hcpcs part date_start providertype isMiscoded errType
	ren providertype phyTypeCERT
	save "$tempdir/cert_fy`y'.dta", replace
}

************************************
*	combine and calculate
************************************
use "$tempdir/cert_fy2013.dta", clear
append using "$tempdir/cert_fy2014.dta", force

gen year = substr(date_start,7,4)
destring year, replace
keep if year==2012

tabstat isMiscoded 
tabstat isMiscoded if part=="1. Part B" 

* sample selection - medicare part b only
keep if part == "1. Part B"

save "$datadir/CERT_claims.dta", replace

* hcpcs-level stats
collapse (count) nClaim=errType, by(hcpcs isMiscoded)

egen totMiscode = total(nClaim), by(isMiscoded)
gen prCond = nClaim/totMiscode // Pr(hcpcs|miscoded)
drop totMiscode
reshape wide prCond nClaim, i(hcpcs) j(isMiscoded)
foreach v of varlist prCond0 prCond1 nClaim0 nClaim1 {
	recode `v'(.=0) 
	}

* Measure 1: simple % miscoded
gen nTotClaim = nClaim0+nClaim1
gen prMiscode = nClaim1/nTotClaim*100

* Measure 2: HCPCS code propensity to get disapproval
gen fpCERT = prCond1*100/(prCond1+prCond0)

* clean up and describe
keep hcpcs prMiscode fpCERT nTotClaim 
sum fpCERT if fpCERT==0
sum fpCERT if fpCERT==100
sum fpCERT if fpCERT>0 & fpCERT<100
twoway scatter fpCERT prMiscode [aweight = nTotClaim], ///
	m(Oh) xtitle("Pr(disapproved) (%)") ytitle("Flag propensity of code") ///
	graphregion(color(white)) plotregion(color(white)) scheme(s2mono)
*Note: each HCPCS code sampled in CERT is represented by a circle, with radius propotional to sample size.
graph export "$tabdir/fig_FP_pr.eps", replace
	
* save data
save "$savedir/FlagPropenHcpcs_CERT.dta", replace
