**************************************
*
*	NAMCS data 2012
*	- physician reported time; and
*	- fraction of Medicare
*
**************************************

**************************************
* housekeeping
**************************************
clear all

local home : env HOME
if "`home'"=="" {
	local home : env HOMEPATH
}
// global rootdir "`home'/Documents/Workplace/Medicare_payment"
global datadir "$rootdir/input"
global tempdir "/tmp"
global savedir "$rootdir/output"
global tabdir "$rootdir/tex"

set more off

**************************************
* prep NAMCS data
************************************** 
use "$datadir/namcs2012-stata.dta", clear
 
* all varnames to lowercase
foreach v of varlist VMONTH - PHYSWT {
	local v_l = lower("`v'")
	ren `v' `v_l'
	}
		
* verify: sampling weight vars do not vary w/i phy: 
foreach v of varlist cstratm cpsum settype patwt patwtst {
	egen minv = min(`v'), by(phycode)
	egen maxv = max(`v'), by(phycode)
	assert minv == maxv
	drop minv maxv
	}

* fill in physician weights
sort phycode patcode 
replace physwt = physwt[_n-1] if phycode==phycode[_n-1]

* recode variables
recode mddo(2=0) 
recode solo(2=0) (-8 -6=.)
recode muinc(2 3 4=0) (-8 -6=.)
recode emedrec(2 3=0) (-8 -6=.)
recode msa(2=0)
recode prmcarer(-9=.)
recode revffsr(-9=.)

* clean "prmcarer" ("Percent of patient care revenue from Medicare")
* ... assuming all categories (0-25, 26-50, 51-75, 76-100) take the lower-bound value
gen shrevmcare_lb = .
lab var shrevmcare_lb "% revenue from Medicare (lower bound)"
	replace shrevmcare_lb = 0   if prmcarer == 1
	replace shrevmcare_lb = .26 if prmcarer == 2
	replace shrevmcare_lb = .51 if prmcarer == 3
	replace shrevmcare_lb = .76 if prmcarer == 4

* ... assuming all categories take the category mean
gen shrevmcare_avg = .
lab var shrevmcare_avg "% revenue from Medicare (average)"
	replace shrevmcare_avg = .125 if prmcarer == 1
	replace shrevmcare_avg = .38 if prmcarer == 2
	replace shrevmcare_avg = .63 if prmcarer == 3
	replace shrevmcare_avg = .88 if prmcarer == 4
	
* ... assuming all categories take the upper-bound value
gen shrevmcare_ub = .
lab var shrevmcare_ub "% revenue from Medicare (upper bound)"
	replace shrevmcare_ub = .25 if prmcarer == 1
	replace shrevmcare_ub = .50 if prmcarer == 2
	replace shrevmcare_ub = .75 if prmcarer == 3
	replace shrevmcare_ub = 1   if prmcarer == 4

* select variables
local phyvar phycode ///
	specr_14 specr_17 mddo solo muinc emedrec ///
	fipsstoff divisionoff msa ///
	prmcarer shrevmcare_lb shrevmcare_avg shrevmcare_ub revffsr ///
	cstratm cpsum settype patwt patwtst physwt
	
local visitvar patcode paypriv paymcare paymcaid paywkcmp payself paynochg payoth paydk paytyper ///
	servcnt timemd timemdfl
	
keep `phyvar' `visitvar'
order `phyvar' `visitvar'

* extract phy-level data
* ... phy-invariant vars
preserve

keep `phyvar'
duplicates drop phycode, force
saveold "$tempdir/namcs2012-phy.dta", replace

restore

* ... aggregate visit-specific var
gen int nservmcare = paymcare*servcnt
gen timemcare = paymcare*timemd

collapse (count) npat = patcode ///
		 (mean) shpatmcare = paymcare ///
		 (sum)  nserv = servcnt ///
			npatmcare = paymcare ///
			nservmcare = nservmcare ///
			totminutes = timemd ///
			timemcare, by(phycode)
gen shservmcare = nservmcare/nserv
gen shtimemcare = timemcare/totminutes
gen tothours = totminutes/60
gen tothoursmcare = timemcare/60

drop totminutes timemcare
order phycode npat* shpat* nserv* shserv* tothours* shtime*

lab var npat "num patients"
lab var npatmcare "num Medicare patients"
lab var shpatmcare "fraction of Medicare patients"

lab var nserv "num of services provided"
lab var nservmcare "num of services for Medicare patients"
lab var shservmcare "fraction of services on Medicare patients"

lab var tothours "total number of hours with patients" 
lab var tothoursmcare "total number of hours with Medicare patients"
lab var shtimemcare "fraction of time with Medicare patients"

* ... merge with preserved physician-invariant variables
merge 1:1 phycode using "$tempdir/namcs2012-phy.dta"
drop _m

* define survey data structure (for standard error calculation)
svyset cpsum [pweight=physwt], strata(cstratm)
svydescribe
desc
su sh* [aweight=physwt], sep(0)
saveold "$savedir/namcs2012-phy.dta", replace

************************************
*  NAMCS physician characteristics (for balancing test)
* ... and share of Medicare services
************************************
* set up postfile
local postTabS1f "NAMCS"
di "`postTabS1f'"

cap postclose tabS1
postfile tabS1 str40 r double(`postTabS1f') using "$tempdir/tableS1_p1.dta", replace

* set up row titles
local row1 "1(MD)"
local row3 "1(solo practice)"
local row5 "1(in EHR)"
local row7 "1(in IT incentive program)"
local row9 "% Medicare patients"
local row11 "% Medicare services"
local row13 "% time spent with Medicare patients"
local row15 "% revenue from Medicare"

* fill in rows
local postTabS1
foreach pval in `postTabS1f' {
    local postTabS1 "`postTabS1' (`pval')"
    }

local ixRow = 1
local compvars mddo solo emedrec muinc ///
	shpatmcare shservmcare shtimemcare shrevmcare_avg
foreach v of varlist `compvars' {
	
	* ... column 1 (NAMCS)
	svy: mean `v'	
	mat tabMean = e(b)
	mat tabSD = e(V)
	local meanNAMCS = tabMean[1,1]
	local seNAMCS  = sqrt(tabSD[1,1])
	
	* post means (odd-num. rows)
	scalar NAMCS   	= `meanNAMCS'
	post tabS1 ("`row`ixRow''") `postTabS1'
	local ixRow = `ixRow'+1

	* post se of means (even-num. rows)
	scalar NAMCS   	= `seNAMCS'
	post tabS1 ("`row`ixRow''") `postTabS1'
	local ixRow = `ixRow'+1
	
	}

* the last row: sample sizes
qui su phycode
scalar NAMCS = r(N)
post tabS1 ("Sample size (before weighting)") `postTabS1'
	
* close post file
postclose tabS1

************************************
* table: balancing test (part II: CMS phy characteristics)
************************************
use "$savedir/phy_time.dta", clear

* restrict sample to DO and MD
gen isDO = (strpos(phyCred,"DO,")>0 | strpos(phyCred,"DO")==length(phyCred)-1)
keep if isDO+isMD > 0

* generate vars correspond to those in NAMCS
gen byte mddo = 1-isDO
gen byte solo = nPhyInGroup<=5
ren inEHR emedrec
gen byte muinc = inERX+inPQRS>0

* set up postfile
local postTabS1f "CMS"
di "`postTabS1f'"

cap postclose tabS1
postfile tabS1 str30 r double(`postTabS1f') using "$tempdir/tableS1_p2.dta", replace

* set up row titles
local row1 "1(MD)"
local row3 "1(solo practice)"
local row5 "1(in EHR)"
local row7 "1(in IT incentive program)"

* fill in rows
local postTabS1
foreach pval in `postTabS1f' {
    local postTabS1 "`postTabS1' (`pval')"
    }

local ixRow = 1
local compvars mddo solo emedrec muinc
foreach v of varlist `compvars' {
	
	* ... column 1 (CMS)
	qui tabstat `v', stats(mean sem) save
	mat tabstatR  = r(StatTotal)
	local meanCMS  = tabstatR[1,1]
	local seCMS   = tabstatR[2,1]
	
	* post means and p-values (odd-num. rows)
	scalar CMS   	= `meanCMS'	
	post tabS1 ("`row`ixRow''") `postTabS1'
	local ixRow = `ixRow'+1

	* post se of means (even-num. rows)
	scalar CMS   	= `seCMS'
	post tabS1 ("`row`ixRow''") `postTabS1'
	local ixRow = `ixRow'+1	
	}

* the last row: sample sizes
qui su npi
local nobsCMS = r(N)
	
* close post file
postclose tabS1

* combine
use "$tempdir/tableS1_p1.dta",clear
merge 1:1 _n using "$tempdir/tableS1_p2.dta"
drop _merge

drop in 5/6 // "1(in EHR)" definition not really consistent between samples

local totN = _N
replace CMS = `nobsCMS' in `totN' // fill in CMS sample size

* clean up and export to .tex
qui compress
foreach v of varlist NAMCS CMS {
	replace `v' = (int(`v'*1000)+( mod(int(`v'*10000),10)>=5) )/1000
	tostring `v', replace
	replace `v' = "0"+`v' if strpos(`v',".")==1
	replace `v' = `v'+"00" if length(`v')-strpos(`v',".")==1
	replace `v' = `v'+"0" if length(`v')-strpos(`v',".")==2
	replace `v' = "("+`v'+")" if mod(_n,2)==0
	}	
replace CMS = "" if CMS=="0." | CMS=="(0.)"

* save 1st part (balancing test) => Table A26 of Online Appendix
preserve

drop if CMS==""
save "$savedir/tableS1_balancing.dta", replace
dataout, save("$tabdir/tableS1_balancing.tex") tex replace

restore

* save 2nd part (share of Medicare services) => Table 10 of manuscript
preserve

drop in 1/6
drop CMS
save "$savedir/tableS2_shMedicare.dta", replace
dataout, save("$tabdir/tableS2_shMedicare.tex") tex replace

restore

