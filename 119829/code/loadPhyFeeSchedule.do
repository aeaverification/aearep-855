************************************
****							****
****    loadPhyFeeSchedule.do	****
****							****
************************************

************************************
*		housekeeping
************************************
version 11.2
include "config.do"
clear all

local home : env HOME
if "`home'"=="" {
	local home : env HOMEPATH
}
// global rootdir "`home'/Documents/Workplace/Medicare_payment"
global datadir "$rootdir/input"
global tempdir "/tmp"
global savedir "$rootdir/output"
global tabdir "$rootdir/tex"

cap log close
log using "$rootdir/code/loadPhyFeeSchedule.txt", text replace

set more off


************************************
*	hcpcs-level PFS info
************************************
* GPCI's
insheet using "$datadir/GPCI2012.csv", clear
rename gpciregion GPCIregion 
sort GPCIregion
save "$tempdir/tempGPCI.dta", replace

* PFS RVU's: merge with GPCI
insheet using "$datadir/PPRRVU12.csv", clear
sort hcpcs
keep hcpcs status workrvu pervu_officet pervu_officef pervu_fact pervu_facf mprvu cf calcuflag

duplicates drop hcpcs, force
expand 90
sort hcpcs
egen GPCIregion = fill(1(1)90)
	replace GPCIregion = mod(GPCIregion, 90)
	replace GPCIregion = 90 if GPCIregion == 0
sort GPCIregion
merge m:1 GPCIregion using "$tempdir/tempGPCI.dta"	
drop _merge

* calculate PFS $ by hcpcs-office-GPCI
rename pervu_officet pervuT1
rename pervu_officef pervuF1
rename pervu_fact pervuT0
rename pervu_facf pervuF0
reshape long pervuT pervuF, i(hcpcs status workrvu mprvu cf calcuflag GPCIregion workgpci pegpci mpgpci) j(office)

gen inPFS2012 = (status=="A"|status=="R"|status=="T")
gen pfsTran = (workrvu*workgpci + pervuT*pegpci + mprvu*mpgpci)*cf
gen pfsFull = (workrvu*workgpci + pervuF*pegpci + mprvu*mpgpci)*cf
foreach v of varlist pfsTran pfsFull {
	replace `v' = `v'*1.03 if calcuflag == 1
	replace `v' = `v'*1.05 if calcuflag == 2
	replace `v' = `v'*0.98 if calcuflag == 3
}

label variable inPFS2012 "RVUs used for Medicare payment"
label variable pfsTran "PFS baseline payment for transitioned services"
label variable pfsFull  "PFS baseline payment for fully-implemented services"

keep hcpcs office GPCIregion inPFS2012 workrvu pfsTran pfsFull
sort hcpcs GPCIregion office
save "$savedir/hcpcs_physician_fee.dta", replace

************************************
*		housekeeping
************************************

log close
