************************************
****							****
****    	loadPhyServ.do		****
****							****
************************************

************************************
*		housekeeping
************************************
version 13
include "config.do"
clear all

local home : env HOME
if "`home'"=="" {
	local home : env HOMEPATH
}
// global rootdir "`home'/Documents/Workplace/Medicare_payment"
global datadir "$rootdir/input"
global tempdir "/tmp"
global savedir "$rootdir/output"
global tabdir "$rootdir/tex"

cap log close
log using "$rootdir/code/loadPhyServ.txt", text replace

set more off

************************************
* load data (2012 and 2013)
************************************
/* loading tab-delimited data in Stata 13 is much easier */
import delimited "$datadir/Medicare_Provider_Util_Payment_PUF_CY2012.txt", delim(tab) clear
replace hcpcs_description = substr(hcpcs_description,1,240)
compress
saveold "$datadir/Medicare_Provider_Util_Payment_PUF_CY2012.dta", replace

import delimited "$datadir/Medicare_Provider_Util_Payment_PUF_CY2013.txt", delim(tab) clear
replace hcpcs_description = substr(hcpcs_description,1,240)
compress
saveold "$datadir/Medicare_Provider_Util_Payment_PUF_CY2013.dta", replace

	
	* after that, open .dta in Stata 12
	use "$datadir/Medicare_Provider_Util_Payment_PUF_CY2012.dta", clear

	* only keep individuals (about 95%; organizations about 5%)
	drop if nppes_entity_code != "I"
	drop nppes_entity_code
	
	* only keep physicians in continental US (>99.99%)
	drop if nppes_provider_country != "US"
	drop nppes_provider_country
	
	foreach st in XX AA AE AP AS GU MP PR VI ZZ {
		drop if nppes_provider_state == "`st'"
	}
	
	* generate numerical gender
	drop if nppes_provider_gender == ""
	gen male = (nppes_provider_gender=="M")
	
	* drop medicare participation
	drop medicare_participation_indicator
	
	* generate numerical place-of-service
	drop if place_of_service==""
	gen inOffice = (place_of_service=="O")
	
	* generate numerical provider type
	drop if provider_type ==""
	
	* generate numerical drug indicator
	gen isDrug = hcpcs_drug_indicator=="Y"

		
	************************************
	* digression 1: list of distinct physicians with physician info
	************************************
	preserve
	
	keep npi nppes_provider_last_org_name nppes_provider_first_name nppes_provider_zip
	rename nppes_provider_last_org_name phyLastName
	rename nppes_provider_zip phyZipList
	destring phyZipList, replace ignore(" " "-" "/" "," "." "$") force

	* 5-digit zipcode
	gen phyZip5List = .
		replace phyZip5List = phyZipList if phyZipList<=99999
		replace phyZip5List = int(phyZipList/10000) if phyZipList>99999

	* sort and save
	rename phyLastName phyLastNameList
	rename nppes_provider_first_name phyFirstNameList
	duplicates drop npi, force

	desc
	sort npi
	save "$savedir/physician_list.dta", replace

	restore
	
	************************************
	* digression 2: list of distinct hcpcs codes
	************************************
	preserve
	
	keep hcpcs_code hcpcs_description isDrug
	duplicates drop hcpcs_code, force
	
	* rename
	rename hcpcs_code hcpcs

	* identify HPPCS Level I (procedures, aka CPT) and Level II (products) codes
	destring hcpcs, generate(hcpcsGroup) force
	replace hcpcsGroup = 1 if hcpcsGroup != .
	replace hcpcsGroup = 2 if strpos(hcpcs, "F") == 5
	replace hcpcsGroup = 3 if strpos(hcpcs, "T") == 5
	replace hcpcsGroup = 0 if hcpcsGroup == .

	label variable hcpcsGroup "indicator: hcpcs level and categrry"
	label define hcpcs1lab 0 "level II" 1 "level I, category I" 2 "level I, category II" 3 "level I, category III"
	label values hcpcsGroup hcpcs1lab
	tab hcpcsGroup
	tab hcpcsGroup isDrug	// >50% of Level II are drugs (makes sense); few Level I, Category I codes are drugs (only a handful)

	sort hcpcs
	save "$tempdir/hcpcs_list2012.dta", replace

	restore
	**************** end of digressinn ********************
	
	* drop irrelevant variables
	drop nppes_provider_last_org_name nppes_provider_first_name nppes_provider_m  ///
		nppes_provider_gender nppes_provider_street1 nppes_provider_street2  ///
		hcpcs_description place_of_service


	* dsstring variables
	foreach v of varlist nppes_provider_zip line_srvc_cnt - stdev_medicare_payment_amt {
		destring `v', replace ignore("$" " " "-" ",")
	}
	
	save "$tempdir/physician_service_2012.dta", replace	
	
	
	************************************
	* 2013: only get NPI, hcpcs and utilization & payment
	************************************
	use "$datadir/Medicare_Provider_Util_Payment_PUF_CY2013.dta", clear
	
	* only keep individuals (about 95%; organizations about 5%)
	drop if nppes_entity_code != "I"
	drop nppes_entity_code
	
	* only keep physicians in continental US (>99.99%)
	drop if nppes_provider_country != "US"
	drop nppes_provider_country
	
	foreach st in XX AA AE AP AS GU MP PR VI ZZ {
		drop if nppes_provider_state == "`st'"
	}
	
	* generate numerical gender
	drop if nppes_provider_gender == ""
	gen male = (nppes_provider_gender=="M")
	
	* drop medicare participation (coarser info than "whether accepts medicare assignment"
	drop medicare_participation_indicator
	
	* generate numerical place-of-service
	drop if place_of_service==""
	gen inOffice = (place_of_service=="O")
	
	* generate numerical provider type
	drop if provider_type ==""
	
	* generate numerical drug indicator
	gen isDrug = hcpcs_drug_indicator=="Y"
	
	************************************
	* digression: list of distinct hcpcs codes
	************************************
	preserve
	
	keep hcpcs_code hcpcs_description isDrug
	duplicates drop hcpcs_code, force
	
	* rename
	rename hcpcs_code hcpcs

	* identify HCPCS Level I (procedures, aka CPT) and Level II (products) codes
	destring hcpcs, generate(hcpcsGroup) force
	replace hcpcsGroup = 1 if hcpcsGroup != .
	replace hcpcsGroup = 2 if strpos(hcpcs, "F") == 5
	replace hcpcsGroup = 3 if strpos(hcpcs, "T") == 5
	replace hcpcsGroup = 0 if hcpcsGroup == .

	label variable hcpcsGroup "indicator: hcpcs level and category"
	label define hcpcs1lab 0 "level II" 1 "level I, category I" 2 "level I, category II" 3 "level I, category III"
	label values hcpcsGroup hcpcs1lab
	tab hcpcsGroup
	tab hcpcsGroup isDrug

	sort hcpcs
	save "$tempdir/hcpcs_list2013.dta", replace

	restore
	**************** end of digression ********************

	* drop irrelevant variables
	keep npi hcpcs_code inOffice isDrug line_srvc_cnt bene_unique_cnt bene_day_srvc_cnt average_medicare_payment_amt
	ren hcpcs_code hcpcs
	ren isDrug isDrug2013
	ren line_srvc_cnt nServ2013
	ren bene_unique_cnt nBene2013
	ren bene_day_srvc_cnt nServUniq2013
	ren average_medicare_payment_amt meanPaid2013
		
	save "$tempdir/physician_service_2013.dta", replace	
	



************************************
* combine and clean: physician-service-level info
************************************
use "$tempdir/physician_service_2012.dta", clear

* rename variables
rename nppes_credentials phyCred
rename nppes_provider_zip phyZip
rename nppes_provider_city phyCity
rename nppes_provider_state phyState
rename hcpcs_code hcpcs
rename line_srvc_cnt nServ
rename bene_unique_cnt nBene
rename bene_day_srvc_cnt nServUniq
rename average_medicare_allowed_amt meanAllow
rename stdev_medicare_allowed_amt sdAllow
rename average_submitted_chrg_amt meanCharge
rename stdev_submitted_chrg_amt sdCharge
rename average_medicare_payment_amt meanPaid
rename stdev_medicare_payment_amt sdPaid

* code physician specialty
egen phyType = group(provider_type), label
drop provider_type

* clean physician credentials
* (1) remove all " " and "." to avoid unnecessary variation
replace phyCred = subinstr(phyCred, " ", "", .)
replace phyCred = subinstr(phyCred, ".", "", .)
replace phyCred = subinstr(phyCred, ",", "", .) if strpos(phyCred,",")==length(phyCred)	//remove commas at the end

* (2) unify coding for common credentials
replace phyCred = "MD" if phyCred == "M,D," | phyCred == "M,D" | phyCred == "M>D>" | ///
		phyCred == "MD>" | phyCred =="MD," | phyCred == "MEDICALDOCTOR"
replace phyCred = subinstr(phyCred, "PA-C", "PA", .)
replace phyCred = subinstr(phyCred, "PAC", "PA", .)
replace phyCred = "PA" if phyCred=="PHYSICIANASSISTANT" | phyCred=="PHYSICIANSASSISTANT"
replace phyCred = "PT" if phyCred=="PHYSICALTHERAPIST" | phyCred=="PHYSICALTHERAPY"
replace phyCred = "NP" if phyCred=="NURSEPRACTITIONER"
replace phyCred = "DC" if phyCred=="CHIROPRACTOR" | phyCred=="D.C." | phyCred=="D,C," | phyCred=="DC,"
replace phyCred = "MD,PHD" if phyCred=="MDPHD" | phyCred=="MD-PHD" | phyCred=="MD/PHD"
replace phyCred = "DDS,MD" if phyCred=="MD,DDS" | phyCred=="MDDDS" | phyCred=="DDSMD"
replace phyCred = "MD,MS" if phyCred=="MS,MD" | phyCred=="MDMS" | phyCred=="MSMD"
replace phyCred = "MD,MPH" if phyCred=="MDMPH"
replace phyCred = "MD,FACS" if phyCred=="MDFACS"
replace phyCred = "MD,FACP" if phyCred=="MDFACP"

gen multiCred = (strpos(phyCred,",")>2 & strpos(phyCred,",")!=length(phyCred) )
gen isMD = ( strpos(phyCred,"MD")>0 )


* code for GPCI regions (mostly entire states) -- GPCI isn't really used in the paper
gen GPCIregion = .
gen GPCIregion_vague = 0
	replace GPCIregion=1 if phyState=="AL"
	replace GPCIregion=2 if phyState=="AK"
	replace GPCIregion=3 if phyState=="AZ"
	replace GPCIregion=4 if phyState=="AR"
	replace GPCIregion=5 if (phyCity=="ANAHEIM"|phyCity=="SANTA ANA") & phyState=="CA"
	replace GPCIregion=6 if (phyCity=="LA"|phyCity=="LOS ANGELE"|phyCity=="LOS ANGELES") & phyState=="CA"
	replace GPCIregion=7 if (phyCity=="MARINA"|phyCity=="NAPA"|phyCity=="SOLANO BEACH") & phyState=="CA"
	replace GPCIregion=8 if (phyCity=="OAKLAND"|phyCity=="BERKELEY") & phyState=="CA"
	replace GPCIregion=11 if (phyCity=="SAN FRANCISCO"|phyCity=="SAN FRANCSICO"|phyCity=="SAN FRAQNCISOCO") & phyState=="CA"
	replace GPCIregion=12 if phyCity=="SAN MATEO" & phyState=="CA"
	replace GPCIregion=13 if phyCity=="SANTA CLARA" & phyState=="CA"
	replace GPCIregion=14 if (phyCity=="VENTURA"|phyCity=="VERTURA") & phyState=="CA"
	replace GPCIregion=9 if phyState=="CA" & GPCIregion==.
	replace GPCIregion=15 if phyState=="CO"
	replace GPCIregion=16 if phyState=="CT"
	replace GPCIregion=17 if phyState=="DC"
		replace GPCIregion_vague=1 if phyState=="VA"|phyState=="MD"	//"MD/VA suburbs" also in region 17
	replace GPCIregion=18 if phyState=="DE"
	replace GPCIregion=19 if (phyCity=="FORT LAUDERDALE"|phyCity=="FT LAUDERDALE") & phyState=="FL"
	replace GPCIregion=20 if (phyCity=="MIAMI"|phyCity=="MIA"|phyCity=="MIAMI BEACH"|phyCity=="MIAMI GARDENS"|phyCity=="MIAMI LAKES"|phyCity=="MIAMI SHORES"|phyCity=="MIAMI SPRINGS") & phyState=="FL"
	replace GPCIregion=21 if phyState=="FL" & GPCIregion==.
	replace GPCIregion=22 if (phyCity=="ALTANTA"|phyCity=="ATLANTA"|phyCity=="ATLANTA,") & phyState=="GA"
	replace GPCIregion=23 if phyState=="GA" & GPCIregion==.
	replace GPCIregion=24 if phyState=="HI"
	replace GPCIregion=25 if phyState=="ID"
	replace GPCIregion=26 if (phyCity=="CHGO"|phyCity=="CHICAGE"|phyCity=="CHICAGO"|phyCity=="CHICAGO HEIGHTS"|phyCity=="CHICAGO HTS"|phyCity=="CHICAGO RIDGE") & phyState=="IL"
		replace GPCIregion_vague=1 if GPCIregion==26
	replace GPCIregion=27 if (phyCity=="EAST SAINT LOUIS"|phyCity=="EAST ST LOUIS") & phyState=="IL"
	replace GPCIregion=28 if phyState=="IL" & GPCIregion==.
	replace GPCIregion=30 if phyState=="IN"
	replace GPCIregion=31 if phyState=="IA"
	replace GPCIregion=32 if phyState=="KS"
	replace GPCIregion=33 if phyState=="KY"
	replace GPCIregion=34 if phyCity=="NEW ORLEANS" & phyState=="LA"
	replace GPCIregion=35 if phyState=="LA" & GPCIregion==.
	replace GPCIregion=36 if phyState=="ME"
		replace GPCIregion_vague=1 if phyState=="ME"
	replace GPCIregion=38 if strpos(phyCity,"BAL")>0 & phyState=="MD"
	replace GPCIregion=39 if phyState=="MD" & GPCIregion==.
	replace GPCIregion=40 if (phyCity=="BOSTOM"|phyCity=="BOSTON") & phyState=="MA"
	replace GPCIregion=41 if phyState=="MA" & GPCIregion==.
	replace GPCIregion=42 if (phyCity=="DETOIT"|phyCity=="DETRIOT"|phyCity=="DETROIT"|phyCity=="DETROIT RD."|phyCity=="DETRTOIT") & phyState=="MI"
	replace GPCIregion=43 if phyState=="MI" & GPCIregion==.
	replace GPCIregion=44 if phyState=="MN"
	replace GPCIregion=45 if phyState=="MS"
	replace GPCIregion=46 if (phyCity=="KANSAS CITY"|phyCity=="KC") & phyState=="MO"
	replace GPCIregion=47 if (phyCity=="SAINT LOUIS"|phyCity=="SAIT LOUIS"|phyCity=="ST .LOUIS") & phyState=="MO"
	replace GPCIregion=48 if phyState=="MO" & GPCIregion==.
	replace GPCIregion=49 if phyState=="MT"
	replace GPCIregion=50 if phyState=="NE"
	replace GPCIregion=51 if phyState=="NV"
	replace GPCIregion=52 if phyState=="NH"
	replace GPCIregion=54 if phyState=="NJ"
		replace GPCIregion_vague=1 if phyState=="NJ"
	replace GPCIregion=55 if phyState=="NM"
	replace GPCIregion=56 if phyCity=="MANHATTAN" & phyState=="NY"
	replace GPCIregion=60 if phyState=="NY" & GPCIregion==.
		replace GPCIregion_vague=1 if GPCIregion==60 // NYC suburbs
	replace GPCIregion=61 if phyState=="NC"
	replace GPCIregion=62 if phyState=="ND"
	replace GPCIregion=63 if phyState=="OH"
	replace GPCIregion=64 if phyState=="OK"
	replace GPCIregion=65 if (phyCity=="PORTAND"|phyCity=="PORTLAND") & phyState=="OR"
	replace GPCIregion=66 if phyState=="OR" & GPCIregion==.
	replace GPCIregion=67 if (phyCity=="PGH"|phyCity=="PHIALDELPHIA"|strpos(phyCity,"PHILA")>0|phyCity=="PHILDELPHIA"|phyCity=="PHILLADELPHIA") & phyState=="PA"
	replace GPCIregion=68 if phyState=="PA" & GPCIregion==.
	replace GPCIregion=70 if phyState=="RI"
	replace GPCIregion=71 if phyState=="SC"
	replace GPCIregion=72 if phyState=="SD"
	replace GPCIregion=73 if phyState=="TN"
	replace GPCIregion=74 if (phyCity=="AUSTEN"|phyCity=="AUSTIN") & phyState=="TX"
	replace GPCIregion=75 if phyCity=="BEAUMONT" & phyState=="TX"
	replace GPCIregion=76 if phyCity=="BRAZORIA" & phyState=="TX"
	replace GPCIregion=77 if strpos(phyCity,"DALLAS")>0 & phyState=="TX"
	replace GPCIregion=78 if (phyCity=="FORT WORTH"|phyCity=="FORTWORTH"|phyCity=="FT WORTH"|phyCity=="FT. WORTH") & phyState=="TX"
	replace GPCIregion=79 if phyCity=="GALVESTON" & phyState=="TX"
	replace GPCIregion=80 if (phyCity=="HOUTON"|strpos(phyCity,"HOUST")>0) & phyState=="TX"
	replace GPCIregion=81 if phyState=="TX" & GPCIregion==.
	replace GPCIregion=82 if phyState=="UT"
	replace GPCIregion=83 if phyState=="VT"
	replace GPCIregion=85 if phyState=="VA"
	replace GPCIregion=87 if (phyCity=="SEATLE"|phyCity=="SEATTLE") & phyState=="WA"
	replace GPCIregion=86 if phyState=="WA" & GPCIregion==.
	replace GPCIregion=88 if phyState=="WV"
	replace GPCIregion=89 if phyState=="WI"
	replace GPCIregion=90 if phyState=="WY"
drop phyCity phyState


* merge with 2013 utilization data
merge 1:1 npi hcpcs inOffice using "$tempdir/physician_service_2013.dta"

* keep those with data from both years
egen npiMergeStatus = max(_merge), by(npi)
keep if npiMergeStatus == 3
drop npiMergeStatus _merge

* verify that isDrug==isDrug2013, and keep only one indicator
tab isDrug isDrug2013	//only 1 disagreement (J1457), ignore
replace isDrug=isDrug2013 if isDrug==. & isDrug2013!=.
replace isDrug=. if (isDrug==1 & isDrug2013==0) | (isDrug==0 & isDrug2013==1)
drop isDrug2013

* label variables
label variable isDrug "indicator: hcpcs is a drug"
label variable npi "physician npi"
label variable phyCred "physician credential"
label variable phyZip "5/9-digit zipcode"
label variable hcpcs "hcpcs code of service"
label variable nServ "number of services provided in CY 2012"
label variable nBene "number of distinct beneficiaries served in CY 2012"
label variable nServUniq "number of services provided in CY 2013, excluding double counting of same bene. in a single day"
label variable nServ2013 "number of services provided in CY 2013"
label variable nBene2013 "number of distinct beneficiaries served in CY 2013"
label variable nServUniq2013 "number of services provided in CY 2012, excluding double counting of same bene. in a single day"
label variable meanAllow "average Medicare allowed amt ($)"
label variable sdAllow "s.d. of Medicare allowed amt"
label variable meanCharge "average submitted charge ($)"
label variable sdCharge "s.d. of submitted charge"
label variable meanPaid "average Medicare payment amount ($)"
label variable sdPaid "s.d. of Medicare payment amount"
label variable male "indicator: physician is male"
label variable inOffice "indicatrr: place-of-seriice is inOffice (as opposed to facility)"
label variable phyType "provider type code"
label variable multiCred "indicator: multiple credentials"
label variable isMD "indicator: has a MD degree"
label variable GPCIregion "code for GPCI region in PFS 2012"
label variable GPCIregion_vague "indicator: vaguely defined GPCIregion"

order npi hcpcs male inOffice phyCred multiCred isMD phyType phyZip GPCIregion GPCIregion_vague isDrug

desc
sum, sep(0)
codebook npi
sort npi phyZip hcpcs inOffice
save "$savedir/physician_service.dta", replace

************************************
*		complete hcpcs list
************************************
use "$tempdir/hcpcs_list2012.dta", clear
gen year = 2012

append using "$tempdir/hcpcs_list2013.dta"
replace year = 2013 if year==.

sort hcpcs year
duplicates drop hcpcs, force
tab year
save "$savedir/hcpcs_list.dta", replace


************************************
*		housekeeping
************************************

log close


