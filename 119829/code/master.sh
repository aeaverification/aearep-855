#!/bin/bash

stata-mp -b do loadPhyServ_1.do
stata-mp -b do loadAllPhysician.do
stata-mp -b do loadPhyFeeSchedule.do
stata-mp -b do mergePhyServ_AllPhysician.do
stata-mp -b do time_define_old.do
stata-mp -b do time_define.do
stata-mp -b do time_flag_old.do
stata-mp -b do time_flag.do
stata-mp -b do time_flag_new.do
stata-mp -b do table_a10.do
stata-mp -b do table_a11.do
stata-mp -b do table_a12.do


