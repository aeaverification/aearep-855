************************************
****							****
*** mergePhyServ_AllPhysician.do ***
****							****
************************************

************************************
*		housekeeping
************************************
version 11.2
include "config.do"
clear all

local home : env HOME
if "`home'"=="" {
	local home : env HOMEPATH
}
// global rootdir "`home'/Documents/Workplace/Medicare_payment"
global datadir "$rootdir/output"
global tempdir "/tmp"
global savedir "$rootdir/output"
global tabdir "$rootdir/tex"

cap log close
log using "$rootdir/code/mergePhyServ_AllPhysician.txt", text replace

set more off

************************************
* extract physician info from National directory
************************************
* Note: due to data size, only extract relevant info

use "$datadir/physician_info.dta", clear

* first best: merge by npi
sort npi
merge m:1 npi using "$datadir/physician_list.dta"
tab _merge

* clean _merge==3 observations
preserve

keep if _merge == 3
gen zipDiff = abs( phyZip5 - phyZip5List)
sort npi zipDiff

* ...if one npi from the list is matched with multiple lines, keep one with least diff in zipcode
duplicates drop npi, force
drop phyZip phyZip5 phyZipList phyZip5List _merge zipDiff
save "$tempdir/phy_merged.dta",replace

restore

* second best: those not merged - by lastname
* ... get the list of unmerged distinct physicians
preserve

keep if _merge == 2
keep npi phyLastNameList phyFirstNameList phyZipList phyZip5List
duplicates drop npi, force

gen FLmerge = 1
label variable FLmerge "FLAG: not merged by npi in 1st try"
rename phyLastNameList lastname
rename phyFirstNameList firstname
sort lastname firstname
save "$tempdir/phy_unmerged.dta", replace

restore

* ... re-merge by lastname-firstname-zipcode
drop if _merge == 2
drop _merge npi phyLastNameList phyFirstNameList phyZipList phyZip5List
sort lastname firstname
merge m:m lastname firstname using "$tempdir/phy_unmerged.dta"

keep if _merge == 3
gen zipDiff = abs( phyZip5 - phyZip5List)
sort npi zipDiff
duplicates drop npi, force

* Finally: combine with those merged in 1st try
drop phyZip phyZip5 phyZipList phyZip5List _merge zipDiff
rename lastname phyLastNameList
rename firstname phyFirstNameList
append using "$tempdir/phy_merged.dta"

duplicates list npi	// check uniqueness of npi

* save merged physician list + info from national file
* ... only keep necessary info (note: npi uniquely identifies an obs.)
* (e.g. detailed address barely useful given we're done with merging)
drop pacid peid phyLastNameList phyFirstNameList lastname firstname ///
	male phyCred addStreet addCity phySpec2

* ... suppress affiliated hospitals' ccn to reduce size
forvalues i = 1/5 {
	destring hosCCN`i', replace
}
gen hosAff = (hosCCN1!=.)+(hosCCN2!=.)+(hosCCN3!=.)+(hosCCN4!=.)+(hosCCN5!=.)
label define hosAffLab 5 "5 or more"
label values hosAff hosAffLab 
label variable hosAff "number of hospitals affiliated"
drop hosCCN*

sort npi
save "$tempdir/physician_info_merged.dta", replace


************************************
* add physician info to util&payment data
************************************
use "$datadir/physician_service.dta", clear

* merge physician background info
sort npi
merge m:1 npi using "$tempdir/physician_info_merged.dta"
egen phySpec = group(phySpec1), label
drop phySpec1

* drop oss. with missing physician info
drop if FLmerge == 1 | _merge != 3	// only use those with physician info
drop FLmerge _merge


* drop those graduated before WWII ended or after 2012
drop if gradYear <= 1945
drop if gradYear >= 2012

* generate expereence
gen experYear = 2012 - gradYear + 1
label variable experYear "experience in years"

save "$savedir/phy_serv_merged.dta", replace

codebook npi hcpcs, compact

************************************
*		housekeeping
************************************
log close

