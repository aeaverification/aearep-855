




*make a file of all the time estimates

* start with the utilization data
use "$savedir/phy_time_old.dta",clear

keep totHrsPerWk_2013 totAllow_2013 npi 
rename totHrsPerWk_2013 totHrsPerWk_util_old
rename totAllow_2013 totAllow_util

merge 1:1 npi using "$savedir/phy_time_1.dta",keepusing(totHrsPerWk_2013) nogen

rename totHrsPerWk_2013 totHrsPerWk_util_newish

merge 1:1 npi using "$savedir/phy_time_2.dta",keepusing(totHrsPerWk_2013) nogen

rename totHrsPerWk_2013 totHrsPerWk_util_new

merge 1:1 npi using "$savedir/carr_times_old.dta", keepusing( totHrsPerWk_noadj totHrsPerWk1)nogen

rename totHrsPerWk_noadj totHrsPerWk_claim_old_noadj
rename totHrsPerWk1 totHrsPerWk_claim_old_adj

merge 1:1 npi using "$savedir/carr_times2.dta",keepusing(totHrsPerWk_noadj_all totHrsPerWk_all) keep(match) nogen

rename totHrsPerWk_noadj_all totHrsPerWk_claim_new_noadj
rename totHrsPerWk_all totHrsPerWk_claim_new_adj

merge 1:1 npi using "$savedir/cataract_dr.dta"
gen cataract=0
replace cataract=1 if _merge==3
drop _merge


save "$savedir/phy_time_all.dta",replace
su totHrsPerWk_util_old totHrsPerWk_util_newish totHrsPerWk_util_new totHrsPerWk_claim_old_noadj totHrsPerWk_claim_old_adj /*
*/ totHrsPerWk_claim_new_noadj totHrsPerWk_claim_new_adj,detail

corr totHrsPerWk_util_old totHrsPerWk_util_new totHrsPerWk_claim_old_noadj totHrsPerWk_claim_old_adj /*
*/ totHrsPerWk_claim_new_noadj totHrsPerWk_claim_new_adj

gen flag=totHrsPerWk_util_old>100
gen flag20=totHrsPerWk_util_old>20

su totHrsPerWk_util_old totHrsPerWk_claim_old_noadj if cataract==1,d
corr totHrsPerWk_util_old totHrsPerWk_claim_old_noadj if cataract==1
su totHrsPerWk_util_old totHrsPerWk_claim_old_noadj if cataract==0,d
corr totHrsPerWk_util_old totHrsPerWk_claim_old_noadj if cataract==0

su totHrsPerWk_util_old totHrsPerWk_claim_old_noadj if flag==0 & flag20==1,d
corr totHrsPerWk_util_old totHrsPerWk_claim_old_noadj if flag==0 & flag20==1

su totHrsPerWk_util_old totHrsPerWk_claim_old_noadj if flag==1,d
corr totHrsPerWk_util_old totHrsPerWk_claim_old_noadj if flag==1
su totHrsPerWk_util_old totHrsPerWk_claim_old_noadj if flag==0 & flag20==1 & cataract==0, d
corr totHrsPerWk_util_old totHrsPerWk_claim_old_noadj if flag==0 & flag20==1 & cataract==0
su totHrsPerWk_util_old totHrsPerWk_claim_old_noadj if flag==0 & flag20==1 & cataract==1, d
corr totHrsPerWk_util_old totHrsPerWk_claim_old_noadj if flag==0 & flag20==1 & cataract==1
su totHrsPerWk_util_old totHrsPerWk_claim_old_noadj if flag==1 & cataract==0, d
corr totHrsPerWk_util_old totHrsPerWk_claim_old_noadj if flag==1 & cataract==0
su totHrsPerWk_util_old totHrsPerWk_claim_old_noadj if flag==1 & cataract==1,d
corr totHrsPerWk_util_old totHrsPerWk_claim_old_noadj if flag==1 & cataract==1


*looks at the total allowed to time ratio in the Utilization and the claims data

use "$savedir/phy_time_old.dta",clear
gen temp=totAllow_2013/totHrsPerWk_2013
keep temp npi totHrsPerWk_2013
save "$tempdir/junk.dta",replace



use "$savedir/cardata.dta",clear

drop lbenpmt lprvpmt ldedamt lprpdamt coinamt

merge m:1 npi using "$savedir/flaggedlist_old1.dta"
keep if _merge==3 // drop all docs that don't appear in the utilization file
drop _merge

egen tot_serv=sum(srvc_cnt),by(npi hcpcs) // sum service count by procedure and doc
egen tot_allow=sum(lalowchg),by(npi)
egen temp=tag(npi hcpcs) // keep uniqu doc/procedure


keep if temp==1

keep npi hcpcs tot_serv tot_allow


merge m:1 hcpcs using "$savedir/servTimeListold.dta" // merge with serve time data
keep if _merge == 3	// _m=2: no such hcpcs filed, _m=1: the hcpcs is not Level I, Category 1
drop _merge

gen intHrs   = tot_serv*tIntra/60 // convert to hours
gen totHrs   = tot_serv*tTotal/60

egen intHrs_yr=sum(intHrs),by(npi) // total by physician for the year
egen totHrs_yr=sum(totHrs),by(npi)
egen temp=tag(npi)

keep if temp==1
drop intHrs totHrs 

gen intHrsPerWk   = intHrs_yr/51 // weekly hours
gen totHrsPerWk   = totHrs_yr/51


keep npi totHrsPerWk tot_allow
rename totHrsPerWk totHrsPerWk_noadj

replace tot_allow=tot_allow/1000
gen temp1=tot_allow/totHrsPerWk_noadj
keep npi temp1 totHrsPerWk_noadj

save "$tempdir/junk1.dta",replace



*constructs summary statistics from the 5% claims sample
use "$savedir/cardata.dta",clear


egen temp=sum(linepmt),by(npi)  // total medicare payment by provider
egen temp1=sum(lalowchg),by(npi)  // total allowed charges by provider
egen temp2=sum(srvc_cnt),by(npi) // total service counts by provider

egen temp3=tag(claimno npi)
egen nclaims=sum(temp3),by(npi)
egen nlines=count(temp3),by(npi)

egen temp4=tag(dsysrtky npi)
egen nbene=sum(temp4),by(npi)

merge m:1 hcpcs using "$savedir/hcpcslist_wrvu2013.dta",nogen keep(match master)


replace wRVU2013=0 if wRVU2013==.

gen twrvu=srvc_cnt*wRVU2013
egen tot_wrvu1=sum(twrvu),by(npi)

egen mwrvu=mean(wRVU2013),by(npi)
gen tot_wrvu=nlines*mwrvu

egen doc=tag(npi) // tag and keep unique docs
keep if doc


merge 1:1 npi using "$savedir/flaggedlist_old1.dta"
keep if _merge==3 // drop all docs that don't appear in the utilization file
drop _merge


merge 1:1 npi using "/medicare/physician times/phys_totals.dta"
keep if _merge==3
drop _merge

gen medallowratio=temp1/totalmedicareallowedamount

merge 1:1 npi using "$savedir/carr_times_noadj_old.dta"
keep if _merge==3
drop _merge


save "$savedir/misc_stuff.dta",replace

gen mtot_wrvu=tot_wrvu/nlines
gen mtot_wrvu1=tot_wrvu1/temp2

su temp temp1 temp2 nclaims nlines nbene mtot_wrvu mtot_wrvu1 medallowratio, detail 
su temp temp1 temp2 nclaims nlines nbene mtot_wrvu mtot_wrvu1 medallowratio  if flagged==0,detail
su temp temp1 temp2 nclaims nlines nbene mtot_wrvu mtot_wrvu1 medallowratio  if flagged==1,detail
su temp temp1 temp2 nclaims nlines nbene mtot_wrvu mtot_wrvu1 medallowratio  if flagged==0 & flag20==1,detail



*

use "$savedir/phy_time_old.dta",clear





