clear all

// global rootdir "/medicare/physician times"
global datadir "$rootdir/input"
global tempdir "$rootdir/temp"
global savedir "$rootdir/output"



foreach i in 1 3 {

local a1 "old" // set i=1, original times
local a2 "1" // set i=2, corrected methodology, original data
local a3 "2" // set i=3, corrected methodology, new data

use "$savedir/servTimeList`a`i''.dta", clear
keep hcpcs tTotal tIntra wRVU2013
save "$savedir/timelist`a`i''.dta",replace


import delimited "$datadir/partial_service.csv",clear
replace hcpcs=ltrim(rtrim(hcpcs))  // trim leading and tailing blanks
gen temp=length(hcpcs)
drop if temp==1
replace hcpcs="00"+hcpcs if temp==3  // the spreadsheet drops leading zeros in the hcpcs codes
replace hcpcs="0"+hcpcs if temp==4
drop temp


merge m:1 hcpcs using "$savedir/timelist`a`i''.dta"
keep if _merge==3
recode wRVU2013 (.=0)


gen partial=0
replace partial=1 if mod=="26" // each of these partial services has a separate line in the spreadsheet
replace partial=2 if mod=="TC"
replace partial=3 if mod=="53"

gen tTotal1=tTotal*work_rvu/wRVU2013 if partial>0
replace tTotal1=tTotal if partial==0
replace tTotal1=0 if wRVU2013==0 & partial>0  // the division above makes this missing, change to zero

gen tIntra1=tTotal*work_rvu/wRVU2013 if partial>0
replace tIntra1=tIntra if partial==0
replace tIntra1=0 if wRVU2013==0 & partial>0


keep hcpcs partial mult_proc bilat tTotal1 tIntra1 pre_op intra_op post_op glob_days
rename tTotal1 tTotal
rename tIntra1 tIntra

gen mult_adj=0
gen bilat_adj=0
replace mult_adj=1 if mult_proc==2 //other codes are reduction of TC or facility charges for mult procedures, or no adjustment
replace bilat_adj=1 if bilat==0 | bilat==2 // can only be billed as single procedure
replace bilat_adj=2 if bilat==1 // 150% bilat adjustment
replace bilat_adj=3 if bilat==3 // gets billed as 2 separate procedures for each side with no mp adjustment

drop mult_proc bilat

save "$savedir/timelist`a`i''_partial.dta",replace
}
*
