
clear all

// global rootdir "/medicare/physician times"
global datadir "$rootdir/input"
global tempdir "$rootdir/temp"
global savedir "$rootdir/output"


use "$savedir/cardata.dta",clear
merge m:1 npi using "$savedir/flaggedlist_old1.dta",keep(match) nogen // drop all docs that don't appear in the utilization file

egen totalmedicarepaymentamount=sum(linepmt),by(npi)  // total medicare payment by provider
egen totalmedicareallowedamount=sum(lalowchg),by(npi)  // total allowed charges by provider
egen numberofservices=sum(srvc_cnt),by(npi) // total service counts by provider
egen doc_person=tag(npi dsysrtky) // tag and keep unique docs
keep if doc_person==1

egen numberofpatients=sum(doc_person),by(npi)
drop doc_person
egen doc=tag(npi)
keep if doc==1
drop doc

merge 1:1 npi using "$savedir/carr_times_noadj_noconv_old.dta",keepusing(totHrsPerWk_noadj_old) keep(match) nogen

merge 1:1 npi using "$savedir/carr_times_adj_noconv_old.dta", keepusing(totHrsPerWk_adj_noconv_old) keep(match) nogen

rename totHrsPerWk_adj_noconv_old full_totHrsPerWk
rename totHrsPerWk_noadj_old full_totHrsPerWk_noadj

keep full_totHrsPerWk_noadj full_totHrsPerWk numberofservices numberofpatients totalmedicareallowedamount totalmedicarepaymentamount npi

keep if full_totHrsPerWk_noadj>0

save "$savedir/fulldata.dta",replace

*aggregate to the level of patient, doc, procedure

*in the 5% sample 
use "$savedir/cardata.dta",clear

keep npi dsysrtky lalowchg


egen temp1=sum(lalowchg),by(npi dsysrtky)  // total allowed charges by provider


egen temp=tag(npi dsysrtky) // tag and keep unique docs
keep if temp
drop temp lalowchg
rename temp1 lalowchg

save "$savedir/fullmult.dta",replace


use "$savedir/cardata.dta",clear
drop lbenpmt lprvpmt ldedamt lprpdamt coinamt

egen tot_serv=sum(srvc_cnt),by(npi dsysrtky hcpcs) // sum service count by procedure and doc

egen temp=tag(npi dsysrtky hcpcs) // keep uniqu doc/procedure

keep if temp==1

keep npi dsysrtky hcpcs tot_serv 


merge m:1 hcpcs using "$savedir/servTimeListold.dta" // merge with serve time data
keep if _merge == 3	// _m=2: no such hcpcs filed, _m=1: the hcpcs is not Level I, Category 1
drop _merge

// convert to hours
gen totHrs   = tot_serv*tTotal/60

// total by physician for the year
egen totHrs_yr=sum(totHrs),by(npi dsysrtky)
egen temp=tag(npi dsysrtky)

keep if temp==1
keep npi dsysrtky totHrs_yr
save "$savedir/unadj_byind.dta",replace


use "$savedir/claims_adjold.dta",clear

*now aggregate up to the npi/hcpcs/partial,partial1 flag level

egen tot_serv=sum(srvc_cnt),by(npi dsysrtky hcpcs partial) // calculate total service counts


egen temp=tag(npi dsysrtky hcpcs partial)

keep if temp==1

keep npi dsysrtky hcpcs tot_serv tTotal

gen totHrs   = tot_serv*tTotal/60

egen totHrs_yr=sum(totHrs),by(npi dsysrtky)
egen temp=tag(npi dsysrtky)
keep if temp==1
keep npi dsysrtky totHrs_yr

save "$savedir/adj_byind.dta",replace


*predraw the sample
use "$savedir/cardata.dta",clear
merge m:1 npi using "$savedir/fulldata.dta",keepusing(npi) keep(match) nogen

keep dsysrtky
egen temp=tag(dsysrtky)
keep if temp==1
drop temp
save "$tempdir/sampleall.dta",replace

set seed 2026916597 // this is my office phone number
set more off
forvalues i=1(1)500 {

use "$tempdir/sampleall.dta",clear
gen sample=runiform()

keep if sample<=.10
drop sample
save "$tempdir/sample`i'.dta",replace
}
*

log using "/medicare/physician times/log.smcl",replace


set more off
forvalues i=1(1)500 {

*in the 5% sample 
use "$savedir/fullmult.dta",clear

merge m:1 dsysrtky using "$tempdir/sample`i'.dta",keep(match) nogen

egen temp1=sum(lalowchg),by(npi)  // total allowed charges by provider

egen doc=tag(npi) // tag and keep unique docs
keep if doc

merge 1:1 npi using "$savedir/fulldata.dta",keepusing(totalmedicareallowedamount)
keep if _merge==3
drop _merge

gen medallowratio=temp1/totalmedicareallowedamount

gen medallowmult=1/medallowratio

rename temp1 claims_totallow

keep npi claims_totallow  medallowmult medallowratio
save "$tempdir/convert1.dta",replace


use "$savedir/unadj_byind.dta",clear

merge m:1 dsysrtky using "$tempdir/sample`i'.dta",keep(match) nogen
 
egen totHrs_yr1=sum(totHrs_yr),by(npi) // total by physician for the year
egen temp=tag(npi)

keep if temp==1
drop totHrs_yr 

gen totHrsPerWk   = totHrs_yr1/51 // weekly hours

merge 1:1 npi using "$tempdir/convert1.dta" // merge with conversion data
keep if _merge==3
drop _merge

gen totHrsPerWk_noadj_allow=totHrsPerWk*medallowmult

keep npi totHrsPerWk_noadj_allow

save "$tempdir/temp.dta",replace


use "$savedir/adj_byind.dta",clear

merge m:1 dsysrtky using "$tempdir/sample`i'.dta",keep(match) nogen

*now aggregate up to the npi/hcpcs/partial,partial1 flag level

egen totHrs_yr1=sum(totHrs_yr),by(npi)
egen temp=tag(npi)

keep if temp==1
drop totHrs_yr 

gen totHrsPerWk   = totHrs_yr1/51


merge 1:1 npi using "$tempdir/convert1.dta"
keep if _merge==3
drop _merge

gen totHrsPerWk_allow=totHrsPerWk*medallowmult


merge 1:1 npi using "$tempdir/temp.dta" // merge with the unadjusted times
keep if _merge==3
drop _merge

keep npi totHrsPerWk_allow totHrsPerWk_noadj_allow claims_totallow medallowratio
rename totHrsPerWk_allow totHrsPerWk_allow`i'
rename totHrsPerWk_noadj_allow totHrsPerWk_noadj_allow`i'
rename claims_totallow claims_totallow`i'
rename medallowratio medallowratio`i'
save "$tempdir/sim`i'.dta",replace

}
*

log close



set more off
use "$tempdir/sim1.dta",clear

forvalues i=2(1)500 {
merge 1:1 npi using "$tempdir/sim`i'.dta",nogen
}
*

merge 1:1 npi using "$savedir/fulldata.dta"

save "$savedir/sims_500_10.dta",replace




set matsize 10000
matrix A=J(1,500,0)
matrix A1=J(1,500,0)
matrix B=J(1,500,0)
matrix B1=J(1,500,0)
matrix C=J(1,500,0)
matrix C1=J(1,500,0)
matrix D=J(1,500,0)
matrix E=J(1,500,0)
matrix F=J(1,500,0)

set more off
forvalues i=1(1)500 {

su totHrsPerWk_noadj_allow`i'
matrix A[1,`i']=r(mean)
matrix A1[1,`i']=r(sd)

matrix D[1,`i']=r(N)

su full_totHrsPerWk_noadj if totHrsPerWk_noadj_allow`i'!=.
matrix B[1,`i']=r(mean)
matrix B1[1,`i']=r(sd)


su medallowratio`i' if totHrsPerWk_noadj_allow`i'!=.
matrix C[1,`i']=r(mean)
matrix C1[1,`i']=r(sd)

/*
count if totHrsPerWk_noadj_allow`i'>5.766
matrix D[1,`i']=r(N)

count if totHrsPerWk_allow`i'>5.766
matrix E[1,`i']=r(N)

matrix F[1,`i']=(D[1,`i']-E[1,`i'])
*/
}
*

matrix Z=J(500,1,1)

matrix temp=A*Z/500
matrix temp_0=A-temp[1,1]*J(1,500,1)
matrix sd_temp=(temp_0*temp_0')/500
matrix sd_temp[1,1]=sqrt(sd_temp[1,1])
matrix list temp 
matrix list sd_temp


matrix temp1=A1*Z/500
matrix temp_1=A1-temp1[1,1]*J(1,500,1)
matrix sd_temp1=(temp_1*temp_1')/500
matrix sd_temp1[1,1]=sqrt(sd_temp1[1,1])
matrix list temp1
matrix list sd_temp1

matrix temp2=B*Z/500
matrix temp_2=B-temp2[1,1]*J(1,500,1)
matrix sd_temp2=(temp_2*temp_2')/500
matrix sd_temp2[1,1]=sqrt(sd_temp2[1,1])
matrix list temp2 
matrix list sd_temp2

matrix temp3=B1*Z/500
matrix temp_3=B1-temp3[1,1]*J(1,500,1)
matrix sd_temp3=(temp_3*temp_3')/500
matrix sd_temp3[1,1]=sqrt(sd_temp3[1,1])
matrix list temp3 
matrix list sd_temp3

matrix temp4=C*Z/500
matrix temp_4=C-temp4[1,1]*J(1,500,1)
matrix sd_temp4=(temp_4*temp_4')/500
matrix sd_temp4[1,1]=sqrt(sd_temp4[1,1])
matrix list temp4 
matrix list sd_temp4

matrix temp5=C1*Z/500
matrix temp_5=C1-temp5[1,1]*J(1,500,1)
matrix sd_temp5=(temp_5*temp_5')/500
matrix sd_temp5[1,1]=sqrt(sd_temp5[1,1])
matrix list temp5
matrix list sd_temp5

matrix temp6=D*Z/500
matrix temp_6=D-temp6[1,1]*J(1,500,1)
matrix sd_temp6=(temp_6*temp_6')/500
matrix sd_temp6[1,1]=sqrt(sd_temp6[1,1])
matrix list temp6
matrix list sd_temp6



matrix A=J(1,500,0)
matrix A1=J(1,500,0)
matrix B=J(1,500,0)
matrix B1=J(1,500,0)

set more off
forvalues i=1(1)500 {

su totHrsPerWk_allow`i'
matrix A[1,`i']=r(mean)
matrix A1[1,`i']=r(sd)


su full_totHrsPerWk if totHrsPerWk_noadj_allow`i'!=.
matrix B[1,`i']=r(mean)
matrix B1[1,`i']=r(sd)

}
*

matrix temp=A*Z/500
matrix temp_0=A-temp[1,1]*J(1,500,1)
matrix sd_temp=(temp_0*temp_0')/500
matrix sd_temp[1,1]=sqrt(sd_temp[1,1])
matrix list temp 
matrix list sd_temp


matrix temp1=A1*Z/500
matrix temp_1=A1-temp1[1,1]*J(1,500,1)
matrix sd_temp1=(temp_1*temp_1')/500
matrix sd_temp1[1,1]=sqrt(sd_temp1[1,1])
matrix list temp1
matrix list sd_temp1

matrix temp2=B*Z/500
matrix temp_2=B-temp2[1,1]*J(1,500,1)
matrix sd_temp2=(temp_2*temp_2')/500
matrix sd_temp2[1,1]=sqrt(sd_temp2[1,1])
matrix list temp2 
matrix list sd_temp2

matrix temp3=B1*Z/500
matrix temp_3=B1-temp3[1,1]*J(1,500,1)
matrix sd_temp3=(temp_3*temp_3')/500
matrix sd_temp3[1,1]=sqrt(sd_temp3[1,1])
matrix list temp3 
matrix list sd_temp3



set more off
mvencode totHrsPerWk_noadj_allow* totHrsPerWk_allow* medallowratio*,mv(0) o



matrix A=J(1,500,0)
matrix A1=J(1,500,0)
matrix B=J(1,500,0)
matrix B1=J(1,500,0)
matrix C=J(1,500,0)
matrix C1=J(1,500,0)
matrix D=J(1,500,0)
matrix E=J(1,500,0)
matrix F=J(1,500,0)

set more off
forvalues i=1(1)500 {

su totHrsPerWk_noadj_allow`i'
matrix A[1,`i']=r(mean)
matrix A1[1,`i']=r(sd)

su totHrsPerWk_allow`i'
matrix B[1,`i']=r(mean)
matrix B1[1,`i']=r(sd)


su medallowratio`i'
matrix C[1,`i']=r(mean)
matrix C1[1,`i']=r(sd)


count if totHrsPerWk_noadj_allow`i'>5.766
matrix D[1,`i']=r(N)

count if totHrsPerWk_allow`i'>5.766
matrix E[1,`i']=r(N)

matrix F[1,`i']=(D[1,`i']-E[1,`i'])

}
*

matrix Z=J(500,1,1)

matrix temp=A*Z/500
matrix temp_0=A-temp[1,1]*J(1,500,1)
matrix sd_temp=(temp_0*temp_0')/500
matrix sd_temp[1,1]=sqrt(sd_temp[1,1])
matrix list temp 
matrix list sd_temp


matrix temp1=A1*Z/500
matrix temp_1=A1-temp1[1,1]*J(1,500,1)
matrix sd_temp1=(temp_1*temp_1')/500
matrix sd_temp1[1,1]=sqrt(sd_temp1[1,1])
matrix list temp1
matrix list sd_temp1

matrix temp2=B*Z/500
matrix temp_2=B-temp2[1,1]*J(1,500,1)
matrix sd_temp2=(temp_2*temp_2')/500
matrix sd_temp2[1,1]=sqrt(sd_temp2[1,1])
matrix list temp2 
matrix list sd_temp2

matrix temp3=B1*Z/500
matrix temp_3=B1-temp3[1,1]*J(1,500,1)
matrix sd_temp3=(temp_3*temp_3')/500
matrix sd_temp3[1,1]=sqrt(sd_temp3[1,1])
matrix list temp3 
matrix list sd_temp3

matrix temp4=C*Z/500
matrix temp_4=C-temp4[1,1]*J(1,500,1)
matrix sd_temp4=(temp_4*temp_4')/500
matrix sd_temp4[1,1]=sqrt(sd_temp4[1,1])
matrix list temp4 
matrix list sd_temp4

matrix temp5=C1*Z/500
matrix temp_5=C1-temp5[1,1]*J(1,500,1)
matrix sd_temp5=(temp_5*temp_5')/500
matrix sd_temp5[1,1]=sqrt(sd_temp5[1,1])
matrix list temp5
matrix list sd_temp5

matrix temp6=D*Z/500
matrix temp_6=D-temp6[1,1]*J(1,500,1)
matrix sd_temp6=(temp_6*temp_6')/500
matrix sd_temp6[1,1]=sqrt(sd_temp6[1,1])
matrix list temp6
matrix list sd_temp6

matrix temp7=E*Z/500
matrix temp_7=E-temp7[1,1]*J(1,500,1)
matrix sd_temp7=(temp_7*temp_7')/500
matrix sd_temp7[1,1]=sqrt(sd_temp7[1,1])
matrix list temp7
matrix list sd_temp7

matrix temp8=F*Z/500
matrix temp_8=F-temp8[1,1]*J(1,500,1)
matrix sd_temp8=(temp_8*temp_8')/500
matrix sd_temp8[1,1]=sqrt(sd_temp8[1,1])
matrix list temp8
matrix list sd_temp8

su full_totHrsPerWk_noadj full_totHrsPerWk

count if full_totHrsPerWk_noadj>5.766
count if full_totHrsPerWk>5.766


/*
egen hrs_noadj_allow=rowmean(totHrsPerWk_noadj_allow*)
egen hrs_allow=rowmean(totHrsPerWk_allow*)
egen allow_ratio=rowmean(medallowratio*)
*egen hrs_noadj_all_tc=rowmean(totHrsPerWk_noadj_all_tc*)

su hrs_noadj_allow hrs_allow full_totHrsPerWk_noadj full_totHrsPerWk



gen bias_noadj=full_totHrsPerWk_noadj-hrs_noadj_allow
gen bias_adj=full_totHrsPerWk-hrs_allow


*full data, 99.66 percentile is 5.766

