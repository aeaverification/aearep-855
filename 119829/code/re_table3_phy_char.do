************************************
**
** comparison of physician characteristics under alternative time flags
**
************************************

************************************
* phy time data
************************************
use "$datadir/phy_time.dta", clear
merge m:1 npi using "$savedir/re_flag_$ixV.dta"
drop _merge

* FOCUS ON THE SUBSAMPLE WITH 20+ HOURS/WEEK
keep if totHrsPerWk_2012>20 | totHrsPerWk_2013>20

* prep to merge with hrr data
tostring phyZip, replace
gen str5 zip5 = substr(phyZip,1,5) if length(phyZip)==9
replace zip5 = "0"+substr(phyZip,1,4) if length(phyZip)==8
replace zip5 = "00"+substr(phyZip,1,3) if length(phyZip)==7
replace zip5 = phyZip if length(phyZip)==5
replace zip5 = "0"+phyZip if length(phyZip)==4
replace zip5 = "00"+phyZip if length(phyZip)==3

* link to hrr
merge m:1 zip5 using "$datadir/zip_hrr.dta", keep(master matched)

************************************
* see if characteristics differ across groups conditional on HRR
************************************
* prep: group indicators
gen byte ever  = $flag_2012 + $flag_2013>0
	
* set up postfile
local postTab3_5f "Ever"
di "`postTab3_5f'"

cap postclose tab3_5
postfile tab3_5 str50 r double(`postTab3_5f') using "$savedir/table3_$flag_2012.dta", replace

* set up row titles
local row1 "1(male)"
local row3 "1(MD)"
local row5 "Experience (years)"
local row7 "# providers in group service"
local row9 "# hospital affiliations"
local row11 "1(in Medicare)"
local row13 "1(in ERX)"
local row15 "1(in PQRS)"
local row17 "1(in EHR)"
local row19 "Types of codes 2012"
local row21 "Types of codes 2013"
local row23 "Types of E/M codes 2012"
local row25 "Types of E/M codes 2013"

* fill in rows
local postTab3_5
foreach pval in `postTab3_5f' {
    local postTab3_5 "`postTab3_5' (`pval')"
    }

local ixRow = 1
local compvars male isMD experYear nPhyInGroup hosAff inMedicare inERX inPQRS inEHR nCode_2012 nCode_2013 nCodeEM_2012 nCodeEM_2013
foreach v of varlist `compvars' {

	* ... column 3 (ever)
	qui xi: reg `v' ever i.hrr, vce(cluster hrr)
	mat regRbeta = e(b)
	mat regRse   = e(V)
	local meanEv = regRbeta[1,1]
	local seEv   = sqrt(regRse[1,1])
		
	* post means and p-values (odd-num. rows)
	scalar Ever	 	= `meanEv'
	
	post tab3_5 ("`row`ixRow''") `postTab3_5'
	local ixRow = `ixRow'+1

	* post se of means (even-num. rows)
	scalar Ever	 	= `seEv'

	post tab3_5 ("`row`ixRow''") `postTab3_5'
	local ixRow = `ixRow'+1
	
	}

* the last row: sample sizes
qui su npi if $flag_2012+$flag_2013>0
scalar Ever = r(N)


post tab3_5 ("N") `postTab3_5'
	
* close post file
postclose tab3_5

