************************************
**
** physician specialty (flagged vs other)
**
************************************
use "$datadir/phy_time.dta", clear
merge m:1 npi using "$savedir/re_flag_$ixV.dta"
drop _merge

local ixFlag2012 $flag_2012
local ixFlag2013 $flag_2013

* FOCUS ON THE SUBSAMPLE WITH 20+ HOURS/WEEK
keep if totHrsPerWk_2012>20 | totHrsPerWk_2013>20

* COUNT TOTAL NUM. OF FLAGGED/UNFLAGGED BY YEAR (USE LAST)
qui su $flag_2012 if $flag_2012==1 
local nFlag_2012 = r(N)
qui su $flag_2012 if $flag_2012==0 
local nUnflag_2012 = r(N)

qui su $flag_2013 if $flag_2013==1 
local nFlag_2013 = r(N)
qui su $flag_2013 if $flag_2013==0 
local nUnflag_2013 = r(N)

* 2012 
preserve
local totNumPhy = _N
collapse (count) nPhy=npi, by($flag_2012 phyType)
egen totByFlagStatus = total(nPhy), by($flag_2012)
gen nPhyShare = nPhy/totByFlagStatus

egen totBySpec = total(nPhy), by(phyType)
gen nPhyShareAll = totBySpec/`totNumPhy'

drop totByFlagStatus totBySpec
reshape wide nPhy nPhyShare, i(phyType nPhyShareAll) j($flag_2012)
recode nPhy1(.=0)
recode nPhyShare1(.=0)

gen fpSpec_`ixFlag2012' = nPhyShare1/ (nPhyShare0 + nPhyShare1)

save "$tempdir/re_table4supp.dta", replace
restore

* 2013 
local totNumPhy = _N
collapse (count) nPhy=npi, by($flag_2013 phyType)
egen totByFlagStatus = total(nPhy), by($flag_2013)
gen nPhyShare = nPhy/totByFlagStatus

egen totBySpec = total(nPhy), by(phyType)
gen nPhyShareAll = totBySpec/`totNumPhy'

drop totByFlagStatus totBySpec
reshape wide nPhy nPhyShare, i(phyType nPhyShareAll) j($flag_2013)
recode nPhy1(.=0)
recode nPhyShare1(.=0)

gen fpSpec_`ixFlag2013' = nPhyShare1/ (nPhyShare0 + nPhyShare1)

* combine and save
foreach v of varlist nPhy0 - nPhyShare1 {
	ren `v' `v'_2013
	}
	
merge 1:1 phyType using "$tempdir/re_table4supp.dta"

local keepvar phyType nPhyShareAll nPhy0 nPhy0_2013 nPhy1 nPhy1_2013 fpSpec_`ixFlag2012' fpSpec_`ixFlag2013'
keep `keepvar'
order `keepvar'
ren nPhy0 Num_unflagged_2012
ren nPhy0_2013 Num_unflagged_2013
ren nPhy1 Num_flagged_2012
ren nPhy1_2013 Num_flagged_2013
gsort - fpSpec_`ixFlag2012'

* add line showing year
expand 2 in 1
gsort - fpSpec_`ixFlag2012'
qui replace phyType = . in 1
qui replace nPhyShare = . in 1
foreach v in Num_unflagged Num_flagged {
	qui replace `v'_2012 = 2012 in 1 
	qui replace `v'_2013 = 2013 in 1
	}

decode phyType, gen(Specialty)
keep if phyType==11 | phyType==19 | phyType==36 | phyType==44 | phyType==52 | phyType==53 | phyType==59

* add final line showing total # flagged/unflagged by year
expand 2 in 1
local last = _N
replace Specialty = "Total" in `last'
forval year = 2012/2013 {
	replace nPhyShareAll = . in `last'
	replace fpSpec_`ixFlag`year'' = . in `last'
	replace Num_unflagged_`year' = `nUnflag_`year'' in `last'
	replace Num_flagged_`year'   = `nFlag_`year'' in `last'
}

replace phyType = 0 if Specialty == "Total"

save "$savedir/re_table6_$flag_2012.dta", replace
