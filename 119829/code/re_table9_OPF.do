************************************
**
** Response to R2: Table 9 Overbilling Potential Factors
**
************************************
*local ixFlag = substr("$flag_2012",1,8)

use "$datadir/phy_time.dta", clear
merge m:1 npi using "$savedir/re_flag_$ixV.dta"
drop _merge

ren $flag_2012 flag_2012
ren $flag_2013 flag_2013

* FOCUS ON THE SUBSAMPLE WITH 20+ HOURS/WEEK
keep if totHrsPerWk_2012>20 | totHrsPerWk_2013>20

* prep to merge with hrr data
tostring phyZip, replace
gen str5 zip5 = substr(phyZip,1,5) if length(phyZip)==9
replace zip5 = "0"+substr(phyZip,1,4) if length(phyZip)==8
replace zip5 = "00"+substr(phyZip,1,3) if length(phyZip)==7
replace zip5 = phyZip if length(phyZip)==5
replace zip5 = "0"+phyZip if length(phyZip)==4
replace zip5 = "00"+phyZip if length(phyZip)==3

* link to hrr
merge m:1 zip5 using "$datadir/zip_hrr.dta", keep(master matched)

* create vars of interest
keep npi male isMD experYear phyType hrr totHrsPerWk_* totPaid_* flag_2012 flag_2013
reshape long totHrsPerWk_ totPaid_ flag_, i(npi male isMD experYear phyType hrr) j(year)
ren totHrsPerWk_ totHrsPerWk
ren totPaid_ totPaid
ren flag_ flag

local ixFlag = "flag"

* STEP 1: "FAIR HOURLY RATE" (using unflagged only)
gen hrate = totPaid*1000/totHrsPerWk/52

xi: reg hrate male isMD experYear i.phyType i.hrr i.year if `ixFlag'==0 
predict hrate_hat

* STEP 2: UPCODING FACTOR 1
gen upfact1 = totPaid*1000/hrate_hat/8/365

* STEP 3: UPCODING FACTOR 2
gen upfact2 = hrate_hat/hrate 

* summarize: table
* set up postfile
local postTab5_8f "Flagged Not_flagged"
di "`postTab5_8f'"

cap postclose tab5_8
postfile tab5_8 str40 r double(`postTab5_8f') using "$savedir/table9_OPF_$flag_2012.dta", replace

* set up row titles
local row1 "Reported hourly revenue (\$)"
local row3 "Predicted hourly revenue (\$)"
local row5 "Overbilling Potential Factor 1"
local row7 "Overbilling Potential Factor 2"

* fill in rows
local postTab5_8
foreach pval in `postTab5_8f' {
    local postTab5_8 "`postTab5_8' (`pval')"
    }
	
local ixRow = 1
local compvars hrate hrate_hat upfact1 upfact2
foreach v in `compvars' {
	* ... column 1
	qui tabstat `v' if `ixFlag'==1, stats(mean sem) save
	mat tabstatR  = r(StatTotal)
	local meanF = tabstatR[1,1]
	local sdF   = tabstatR[2,1]

	
	* ... column 2
	qui tabstat `v' if `ixFlag'==0, stats(mean sem) save
	mat tabstatR  = r(StatTotal)
	local meanNF = tabstatR[1,1]
	local sdNF   = tabstatR[2,1]
		
	* post means and p-values (odd-num. rows)
	scalar Flagged = `meanF'
	scalar Not_flagged = `meanNF'

		
	post tab5_8 ("`row`ixRow''") `postTab5_8'
	local ixRow = `ixRow'+1

	* post se of means (even-num. rows)
	scalar Flagged = `sdF'
	scalar Not_flagged = `sdNF'


	post tab5_8 ("`row`ixRow''") `postTab5_8'
	local ixRow = `ixRow'+1
	
	}

* the last row: sample sizes
qui su npi if `ixFlag'==1
scalar Flagged = r(N)
	
qui su npi if `ixFlag'==0
scalar Not_flagged = r(N)

post tab5_8 ("N") `postTab5_8'
	
* close post file
postclose tab5_8

