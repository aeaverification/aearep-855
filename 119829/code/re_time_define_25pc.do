************************************
****							****
****	Alternative time estimation
****							****
************************************

************************************
*       housekeeping
************************************
* need -dataout- package
ssc install dataout

version 11.2
include "config.do"
clear all

local home : env HOME
if "`home'"=="" {
	local home : env HOMEPATH
}
// global rootdir "`home'/Documents/Workplace/Medicare_payment"
global datadir "$rootdir/input"
global tempdir "/tmp"
global savedir "$rootdir/output"
global tabdir "$rootdir/tex"

cap log close
log using "$rootdir/code/time_define.txt", text replace

set more off


***** choose which time to use*****
foreach timeS in minimum pc25 timedOnly timedMin {

	************************************
	*	HCPCS list: time per service
	************************************
	* list of E/M codes
	insheet using "$datadir/hcpcslist_timed_with_min.csv", clear
	drop hcpcs1
	rename typical timeIntraTyp
	recode timeIntraTyp(0=.)
	rename expected timeIntra
	recode timeIntra(0=.)
	rename complexity codeComplx
	rename numcomplex nComplx
	rename hcpcsgr EMgroup
	rename isem isEM

	gen pc25 = (minimum + timeIntra)/2
	
	label variable pc25 "25th percentile time"
	label variable minimum "minimum time required"
	label variable timeIntraTyp "AMA suggested time in minutes"
	label variable timeIntra "expected time (lower bound) in minutes"
	label variable codeComplx "level of complexity (1=lowest)"
	label variable nComplx "number of complexity levels in total (3/4/5)"
	label variable EMgroup "group no. of hcpcs"
	label variable isEM "indicator: E/M codes"
	cap label drop isEMlab
	label define isEMlab 0 "non-E/M" 1 "E/M"
	label values isEM isEMlab

	gen timeFlag = 1

	tostring hcpcs, replace
	sort hcpcs
	save "$tempdir/hcpcslist_timed.dta", replace

	* list of CMS (2014) codes with objectively measured time
	insheet using "$datadir/hcpcslist_untimed_survey.csv", clear
	rename intra timeIntra
	rename total timeTotal
	gen timeFlag = 2
	label variable timeFlag "Flag: how time is measured"

	gen minimum = timeIntra/2
	gen pc25 = (minimum + timeIntra)/2

	append using "$tempdir/hcpcslist_timed.dta"
	sort hcpcs
	save "$tempdir/hcpcslist_timed_untimed.dta", replace

	* work RVU data
	insheet using "$datadir/PPRRVU12.csv", clear
	keep hcpcs workrvu
	rename workrvu wRVU2012
	gsort hcpcs -wRVU2012
	duplicates drop hcpcs, force
	save "$tempdir/hcpcslist_wrvu2012.dta", replace

	insheet using "$datadir/PPRRVU13.csv", clear
	keep hcpcs workrvu
	rename workrvu wRVU2013
	gsort hcpcs -wRVU2013
	duplicates drop hcpcs, force
	save "$tempdir/hcpcslist_wrvu2013.dta", replace

	merge 1:1 hcpcs using "$tempdir/hcpcslist_wrvu2012.dta"
	drop _merge

	save "$tempdir/hcpcslist_wrvu.dta", replace

	* all merge to master data: list of hcpcs in our sample
	use "$savedir/hcpcs_list.dta", clear
	rename hcpcsGroup hcpcsCateg

	merge 1:1 hcpcs using "$tempdir/hcpcslist_timed_untimed.dta"
	replace timeFlag = 0 if _merge == 1
	drop if _merge == 2
	drop _merge hcpcsdesc
	rename hcpcs_description hcpcsDesc

	merge 1:1 hcpcs using "$tempdir/hcpcslist_wrvu.dta"
	drop if _merge == 2
	drop _merge

	* only keep Level I, Category I codes
	* ...(level II = products; level I, category III no rvu info or can't be reliably imputed)
	keep if hcpcsCateg == 1
	replace isEM = 0 if isEM == .

	* AND only keep hcpcs that is not a drug
	keep if isDrug==0

	* grouping of codes
	gen codeGroup = .
	label variable codeGroup "15 groups of level I, category I codes"
		replace codeGroup = 1 if hcpcs>="00100" & hcpcs<="01999"
		replace codeGroup = 2 if hcpcs>="10021" & hcpcs<="19499"
		replace codeGroup = 3 if hcpcs>="20005" & hcpcs<="29999"
		replace codeGroup = 4 if hcpcs>="30000" & hcpcs<="39999"
		replace codeGroup = 5 if hcpcs>="33010" & hcpcs<="39599"
		replace codeGroup = 6 if hcpcs>="40490" & hcpcs<="49999"
		replace codeGroup = 7 if hcpcs>="50010" & hcpcs<="53899"
		replace codeGroup = 8 if hcpcs>="54000" & hcpcs<="59899"
		replace codeGroup = 9 if hcpcs>="60000" & hcpcs<="60699"
		replace codeGroup = 10 if hcpcs>="61000" & hcpcs<="64999"
		replace codeGroup = 11 if hcpcs>="65091" & hcpcs<="69990"
		replace codeGroup = 12 if hcpcs>="70010" & hcpcs<="79999"
		replace codeGroup = 13 if hcpcs>="80047" & hcpcs<="89398"
		replace codeGroup = 14 if hcpcs>="90281" & isEM == 0
		replace codeGroup = 15 if isEM == 1
	# delimit ;
	cap label drop codeGrouplab
	label define codeGrouplab  1 "anesthesia" 
							2 "integumentary sys"
							3 "musculoskeletal sys"
							4 "respiratory sys"
							5 "cardiovascular/hemic/lymphatic/mediastinum"
							6 "digestive"
							7 "urinary sys"
							8 "genital sys"
							9 "endocrine sys"
							10 "nervous sys"
							11 "eye/ocular adnexa/auditory sys"
							12 "radiology"
							13 "pathology and lab"
							14 "medicine"
							15 "e/m";
	# delimit cr
	label values codeGroup codeGrouplab
	tab codeGroup
	
	* only EM services (typical time) OR only EM services (min. time)
	gen timedOnly = timeIntra
	gen timedMin = minimum

	* impute time I: average within group
	preserve
	gen tIntPerRVU = `timeS'/wRVU2012
	replace tIntPerRVU = `timeS'/wRVU2013 if tIntPerRVU==.

	gen tTotPerRVU = timeTotal/wRVU2012
	replace tTotPerRVU = timeTotal/wRVU2013 if tTotPerRVU==.

	collapse (mean) tIntPerRVU tTotPerRVU, by(codeGroup)
	sort codeGroup
	save "$tempdir/timePerRVU.dta", replace
	restore

	sort codeGroup
	merge m:1 codeGroup using "$tempdir/timePerRVU.dta"
	assert _merge==3
	drop _merge

	gen timeIntra1 = wRVU2012*tIntPerRVU if `timeS'==.
	replace timeIntra1 = wRVU2013*tIntPerRVU if `timeS'==. & timeIntra1==.
	gen timeTotal1 = wRVU2012*tTotPerRVU if timeTotal==.
	replace timeTotal1 = wRVU2013*tTotPerRVU if timeTotal==. & timeTotal1==.

	drop tIntPerRVU tTotPerRVU

	* impute time II: regression within group
	preserve
	rename `timeS' time
	xi: reg wRVU2012 i.codeGroup*time, noconst
	est store impute2012a

	xi: reg wRVU2013 i.codeGroup*time, noconst
	est store impute2013a
	drop time

	rename timeTotal time
	xi: reg wRVU2012 i.codeGroup*time, noconst
	est store impute2012b

	xi: reg wRVU2013 i.codeGroup*time, noconst
	est store impute2013b

	esttab impute20* using "$tempdir/timeImputeReg.csv", ///
		not nostar noobs nogaps nolines nomtitle nodep nonum nonote replace

	insheet using "$tempdir/timeImputeReg.csv", clear
	rename v1 codeGroup
	rename v2 betaIn
	rename v3 betaIn2013
	rename v4 betaTot
	rename v5 betaTot2013

	foreach v of varlist betaIn betaIn2013 betaTot betaTot2013 {
		replace `v'=. if strpos(codeGroup,"o._Icod")>0 | `v'==0
	}
	replace betaIn  = betaIn2013 if betaIn==.
	replace betaTot = betaTot2013 if betaTot==.
	drop betaIn2013 betaTot2013
	drop if betaIn==. & betaTot==.

	replace codeGroup="_IcodXtime_1" if codeGroup=="time"
	destring codeGroup, replace ignore("_IcodeGroup_" "o._IcodeGroup_" "_IcodXtime_" "o._IcodXtime_")

	egen separator = max( _n*(codeGroup>codeGroup[_n+1]) )	//detect the last _n of beta0's
	gen isInter = (_n>separator)
	drop separator
	reshape wide betaIn betaTot, i(codeGroup) j(isInter)
	replace betaIn0=0 if codeGroup==1
	replace betaTot0=0 if codeGroup==1
	replace betaIn1=0 if betaIn1==. & betaIn0!=.
	replace betaTot1=0 if betaTot1==. & betaTot0!=.
	replace betaIn1=betaIn1+betaIn1[1] if _n>1
	replace betaTot1=betaTot1+betaTot1[1] if _n>1

	order codeGroup betaIn0 betaIn1 betaTot0 betaTot1
	sort codeGroup
	save "$tempdir/timeImputeReg.dta", replace
	restore	

	* combine the two imputed time measures
	sort codeGroup
	merge m:1 codeGroup using "$tempdir/timeImputeReg.dta"
	drop _merge

	gen timeIntra2 = (wRVU2012-betaIn0)/betaIn1 if wRVU2012!=0 & `timeS'!=.
	replace timeIntra2 = (wRVU2013-betaIn0)/betaIn1 if timeIntra2==. & wRVU2013!=0 & `timeS'!=.
	gen timeTotal2 = (wRVU2012-betaTot0)/betaTot1 if wRVU2012!=0 & timeTotal!=.
	replace timeTotal2 = (wRVU2013-betaTot0)/betaTot1 if timeTotal2==. & wRVU2013!=0 & timeTotal!=.

	gen tIntra = `timeS' 
		replace tIntra = max(0,timeIntra1) if timeIntra1!=. & timeIntra2==.
		replace tIntra = max(0,timeIntra2) if timeIntra1==. & timeIntra2!=.
		replace tIntra = min(max(0,timeIntra1),max(0,timeIntra2)) if tIntra==.
		

	gen tTotal = timeTotal
		replace tTotal = max(0,timeTotal1) if timeTotal1!=. & timeTotal2==.
		replace tTotal = max(0,timeTotal2) if timeTotal1==. & timeTotal2!=.
		replace tTotal = min(max(0,timeTotal1),max(0,timeTotal2)) if tTotal==.
		replace tTotal = tIntra if tTotal<tIntra

		
	if "`timeS'"=="timedOnly" | "`timeS'"=="timedMin" {
		replace tIntra = . if timeFlag==0
		replace tTotal = . if timeFlag==0
	}
		
		
	************************************
	*		merge, flag, and compare
	************************************
	merge 1:m hcpcs using "$savedir/phy_serv_merged.dta"
	keep if _merge == 3
	drop _merge

	* calculate total time needed
	gen intHrs_2012   = nServ*tIntra/60
	gen totHrs_2012   = nServ*tTotal/60

	gen intHrs_2013   = nServ2013*tIntra/60
	gen totHrs_2013   = nServ2013*tTotal/60

	* NEW: only keep physicians who appear in both years
	recode nServ(.=0)
	recode nServ2013(.=0)

	egen phyIn2012 = max(nServ>0), by(npi)
	egen phyIn2013 = max(nServ2013>0), by(npi)
	tab phyIn2012 phyIn2013
	keep if phyIn2012==1 & phyIn2013==1
	drop phyIn2012 phyIn2013

	* collapse to physician level
	sort npi hcpcs inOffice
	gen totPaid_2012 = nServ*meanPaid/1000
	gen totPaid_2013 = nServ2013*meanPaid2013/1000

	collapse (sum) intHrs_2012 intHrs_2013 ///
				   totHrs_2012 totHrs_2013 ///
				   totPaid_2012 totPaid_2013 ///
				   , by(npi)

	forval yr = 2012/2013 {
		gen intHrsPerWk_`yr'   = intHrs_`yr'/51
		gen totHrsPerWk_`yr'   = totHrs_`yr'/51
	}


	* flag implausible cases
	forval yr = 2012/2013 {
		gen impFlag0_`yr' = ( totHrsPerWk_`yr'> 80 )
		gen impFlag1_`yr' = ( totHrsPerWk_`yr'>100 )
		gen impFlag2_`yr' = ( totHrsPerWk_`yr'>112 )
		gen impFlag3_`yr' = ( totHrsPerWk_`yr'>168 )

		label variable impFlag0_`yr' "FLAG: > 80 hours/week"
		label variable impFlag1_`yr' "FLAG: >100 hours/week"
		label variable impFlag2_`yr' "FLAG: >112 hours/week (16hr/day)"
		label variable impFlag3_`yr' "FLAG: >168 hours/week (24/7)"
	}


	************************************
	*		post results in table
	************************************
	local postTab1f
	forval i=1/8 {
		local postTab1f "`postTab1f' var`i'"
		}
	di "`postTab1f'"


	cap postclose tab1
	postfile tab1 str60 r double(`postTab1f') using "$tempdir/re_table1_`timeS'.dta", replace

	* set up column titles
	post tab1 ("weekly hours above") (80) (80) (100) (100) (112) (112) (168) (168)
	post tab1 ("") (2012) (2013) (2012) (2013) (2012) (2013) (2012) (2013)

	* set up row titles
	local row1 "Number of physicians flagged (`timeS')"
	local row2 "% in physicians working 20+ hr/week (`timeS')"

	* fill in rows
	local postTab1
	foreach pval in `postTab1f' {
		local postTab1 "`postTab1' (`pval')"
		}

	* ... row 1
	local ixVar = 1
	foreach v of varlist impFlag0_2012 impFlag0_2013 impFlag1_2012 impFlag1_2013 impFlag2_2012 impFlag2_2013 impFlag3_2012 impFlag3_2013 {
		qui su `v'
		scalar var`ixVar' = r(sum)
		local ixVar = `ixVar'+1
		}

	post tab1 ("`row1'") `postTab1'

	* ... row 2
	qui su npi if totHrsPerWk_2012 > 20
	local nOver20_2012 = r(N)
	di "`nOver20_2012'"

	qui su npi if totHrsPerWk_2013 > 20
	local nOver20_2013 = r(N)
	di "`nOver20_2013'"

	local ixVar = 1
	foreach v of varlist impFlag0_2012 impFlag1_2012 impFlag2_2012 impFlag3_2012 {
		qui su `v'
		local nFlagged = r(sum)	
		scalar var`ixVar' = `nFlagged'/`nOver20_2012'
		local ixVar = `ixVar'+2
		}
		
	local ixVar = 2
	foreach v of varlist impFlag0_2013 impFlag1_2013 impFlag2_2013 impFlag3_2013 {
		qui su `v'
		local nFlagged = r(sum)	
		scalar var`ixVar' = `nFlagged'/`nOver20_2013'
		local ixVar = `ixVar'+2
		}	

	post tab1 ("`row2'") `postTab1'


	* close post file
	postclose tab1
	
	************************************
	*		save phy-level flag status
	************************************
	foreach v of varlist impFlag0_2012 impFlag1_2012 impFlag2_2012 impFlag3_2012 impFlag0_2013 impFlag1_2013 impFlag2_2013 impFlag3_2013 {
		ren `v' `v'_`timeS'
	}
	
	duplicates drop npi, force
	keep npi impFlag*
	saveold "$savedir/re_flag_`timeS'.dta", replace

}


************************************
*		format tables
************************************
use "$tempdir/re_table1_minimum.dta", clear
append using "$tempdir/re_table1_pc25.dta"
append using "$tempdir/re_table1_timedOnly.dta"
append using "$tempdir/re_table1_timedMin.dta"

duplicates drop r - var8, force // drop duplicating table headers

replace r = "Weekly hours" in 1
replace r = "Year" in 2
foreach v of varlist var1-var8 {
	qui replace `v' = `v'*100  if `v'<1  // unit: %
	qui replace `v' = (int(`v'*1000)+( mod(int(`v'*10000),10)>=5) )/1000 if `v'<100 // digit: 3-digits after .
	
	tostring `v', replace force
	replace `v' = "" if `v'=="."
	replace `v' = "0"+`v' if strpos(`v',".")==1
	replace `v' = `v'+"00" if length(`v')-strpos(`v',".")==1
	replace `v' = `v'+"0" if length(`v')-strpos(`v',".")==2
	}
	
* ... insert separator columns
gen blank1 = ""
gen blank2 = ""
gen blank3 = ""
order r var1 var2 blank1 var3 var4 blank2 var5 var6 blank3 	
dataout, save("$tabdir/re_table2_nflagged_minTime.tex") tex replace


************************************
*		housekeeping
************************************

log close



