************************************
**
** Response to Referees: codes that are major time takers
**
************************************

************************************
*       housekeeping
************************************
* need -dataout- package
ssc install dataout

version 11.2
include "config.do"
clear all

local home : env HOME
if "`home'"=="" {
	local home : env HOMEPATH
}
// global rootdir "`home'/Documents/Workplace/Medicare_payment"
global datadir "$rootdir/output"
global tempdir "/tmp"
global savedir "$rootdir/output"
global tabdir "$rootdir/tex"

cap log close
log using "$savedir/re_time_takers.txt", text replace

set more off


************************************
*	get the relevant sample of physicians
************************************ 
use "$savedir/phy_time.dta", clear
keep if totHrsPerWk_2012>20 | totHrsPerWk_2013>20
keep npi impFlag1_2012 impFlag1_2013
save "$tempdir/phy_list_relevant.dta", replace


************************************
*	list of time takers
************************************
use "$savedir/phy_serv_time.dta", clear

* to phy-hcpcs-level (i.e. aggregate up diff places)
collapse (sum) nServ2012=nServ nServ2013 intHrs_2012 intHrs_2013, ///
	 by(npi phyType hcpcs hcpcsDesc wRVU2012 wRVU2013 tIntra)
	 
merge m:1 npi using "$tempdir/phy_list_relevant.dta", keep(matched)
drop _merge

* calculate the % of hcpcs in total revenue of each phy
egen totPhy2012 = total(intHrs_2012), by(npi)
egen totPhy2013 = total(intHrs_2013), by(npi)

gen  sh2012 = intHrs_2012/totPhy2012
gen  sh2013 = intHrs_2013/totPhy2013

gen  sh2012F = sh2012 if impFlag1_2012==1
gen  sh2013F = sh2013 if impFlag1_2013==1

* to hcpcs-level
collapse (sum)  nServ2012 nServ2013 intHrs_2012 intHrs_2013 ///
				nFlag2012=impFlag1_2012 nFlag2013=impFlag1_2013 ///
		 (count) nPhy = npi  ///
		 (mean) shFlag2012=impFlag1_2012 shFlag2013=impFlag1_2013 ///
		 (min) minSh2012=sh2012 minSh2013=sh2013 ///
			   minSh2012F=sh2012F minSh2013F=sh2013F ///
		 (p25) p25Sh2012=sh2012 p25Sh2013=sh2013 ///
			   p25Sh2012F=sh2012F p25Sh2013F=sh2013F ///
		 (p50) medSh2012=sh2012 medSh2013=sh2013 ///
			   medSh2012F=sh2012F medSh2013F=sh2013F ///
		 (p75) p75Sh2012=sh2012 p75Sh2013=sh2013 ///
			   p75Sh2012F=sh2012F p75Sh2013F=sh2013F ///
		 (p90) p90Sh2012=sh2012 p90Sh2013=sh2013 ///
			   p90Sh2012F=sh2012F p90Sh2013F=sh2013F ///
		 (max) maxSh2012=sh2012 maxSh2013=sh2013 ///
			   maxSh2012F=sh2012F maxSh2013F=sh2013F ///
		 , by(hcpcs hcpcsDesc wRVU2012 wRVU2013 tIntra)

format hcpcsDesc %-50s

keep if tIntra>120 & shFlag2012>0 & shFlag2012>0 & ///
		nPhy>20 & nFlag2012>1 & nFlag2013>1 & ///
		nServ2012>100 & nServ2013>100
		
save "$savedir/re_time_takers.dta", replace

************************************
*	get the UNION of physicians for whom
*	the time-takers make up a significant share
************************************

* the list of time-taker HCPCSs
use "$savedir/re_time_takers.dta", clear
keep hcpcs
save "$tempdir/time_taker_hcpcs.dta", replace


* at the physician level, find the share of time-taking hcpcs's
use "$savedir/phy_serv_time.dta", clear
keep npi hcpcs intHrs_2012

merge m:1 npi using "$tempdir/phy_list_relevant.dta", keep(matched) keepusing(impFlag1_2012)
drop _merge

merge m:1 hcpcs using "$tempdir/time_taker_hcpcs.dta"
gen tIntraTaker = intHrs_2012*(_merge==3)
drop _merge

collapse (sum) tTot=intHrs_2012 tTaker=tIntraTaker, by(npi impFlag1_2012)
gen shTaker = tTaker/tTot*100
save "$savedir/re_phy_taker_share.dta", replace


* get statistics on the union of flagged physicians associated with "time takers"
use "$savedir/re_phy_taker_share.dta", clear

* ... # FLAGGED physicians EVER filed a time-taking code
qui su npi if impFlag1_2012==1 & shTaker>0 & !missing(shTaker)
local uPhyF = r(N)

* ... # UNFLAGGED physicians EVER filed a time-taking code
qui su npi if impFlag1_2012==0 & shTaker>0 & !missing(shTaker)
local uPhyNF = r(N)

* ... percentiles of %time-taker among FLAGGED physicians conditional on EVER filing
qui su shTaker if impFlag1_2012==1 & shTaker>0 & !missing(shTaker), d
local uPhyP75 = r(p75)
local uPhyP90 = r(p90)
local uPhyMax = r(max)

* add to the last line of table
use "$savedir/re_time_takers.dta", clear
gsort -tIntra

keep hcpcs hcpcsDesc wRVU2012 tIntra nServ2012 nFlag2012 nPhy ///
	p75Sh2012F p90Sh2012F maxSh2012F
ren hcpcs HCPCS
ren hcpcsDesc Description
ren wRVU2012 wRVU
ren tIntra Minutes
ren nServ2012 Num
ren nFlag2012 Flagged
ren nPhy Num_phy

replace Num = round(Num/1000,0.01)

foreach v of varlist Minutes {
	replace `v' = round(`v',0.01)
}

foreach v of varlist p75Sh2012F p90Sh2012F maxSh2012F {
	replace `v' = round(`v'*100,0.01)
}

qui su
local N = r(N)
expand 2 in `N'
local N = `N'+1

replace HCPCS = "Union" in `N'
replace Description = "" in `N'
foreach v of varlist wRVU Minutes Num {
	replace `v' = . in `N'
	}
replace Flagged = `uPhyF' in `N'
replace Num_phy = `uPhyF'+`uPhyNF' in `N'
replace p75Sh2012F = `uPhyP75' in `N'
replace p90Sh2012F = `uPhyP90' in `N'
replace maxSh2012F = `uPhyMax' in `N'

foreach v of varlist p75Sh2012F p90Sh2012F maxSh2012F {
	replace `v' = round(`v',0.01) in `N'
}

dataout, save("$tabdir/re_time_takers.tex") tex replace



************************************
*	Housekeeping
************************************
log close
