************************************
****							****
**** Table A1: representativeness of the specialties in timed codes		****
****							****
************************************

************************************
*       housekeeping
************************************
* need -dataout- package
ssc install dataout

version 11.2
include "config.do"
clear all

local home : env HOME
if "`home'"=="" {
	local home : env HOMEPATH
}
// global rootdir "`home'/Documents/Workplace/Medicare_payment"
global datadir "$rootdir/input"
global tempdir "/tmp"
global savedir "$rootdir/output"
global tabdir "$rootdir/tex"
 
cap log close
log using "$rootdir/code/re_timed_codes_specialty.txt", text replace

set more off


************************************
*		tabulate the specialty of timed codes
************************************

* list of CMS (2014) codes with objectively measured time
insheet using "$datadir/hcpcslist_untimed_survey.csv", clear
rename intra timeIntra
rename total timeTotal
gen timeFlag = 2
label variable timeFlag "Flag: how time is measured"

* drop G codes
drop if strpos(hcpcs,"G")==1

* grouping of codes
gen codeGroup = .
label variable codeGroup "15 groups of level I, category I codes"
	replace codeGroup = 1 if hcpcs>="00100" & hcpcs<="01999"
	replace codeGroup = 2 if hcpcs>="10021" & hcpcs<="19499"
	replace codeGroup = 3 if hcpcs>="20005" & hcpcs<="29999"
	replace codeGroup = 4 if hcpcs>="30000" & hcpcs<="39999"
	replace codeGroup = 5 if hcpcs>="33010" & hcpcs<="39599"
	replace codeGroup = 6 if hcpcs>="40490" & hcpcs<="49999"
	replace codeGroup = 7 if hcpcs>="50010" & hcpcs<="53899"
	replace codeGroup = 8 if hcpcs>="54000" & hcpcs<="59899"
	replace codeGroup = 9 if hcpcs>="60000" & hcpcs<="60699"
	replace codeGroup = 10 if hcpcs>="61000" & hcpcs<="64999"
	replace codeGroup = 11 if hcpcs>="65091" & hcpcs<="69990"
	replace codeGroup = 12 if hcpcs>="70010" & hcpcs<="79999"
	replace codeGroup = 13 if hcpcs>="80047" & hcpcs<="89398"
	replace codeGroup = 14 if hcpcs>="90281"
	
# delimit ;
label define codeGrouplab  1 "Anesthesia" 
						2 "Integumentary system"
						3 "Musculoskeletal system"
						4 "Respiratory system"
						5 "Cardiovascular/hemic/lymphatic/mediastinum"
						6 "Digestive system"
						7 "Urinary system"
						8 "Genital system"
						9 "Endocrine system"
						10 "Nervous system"
						11 "Eye/ocular adnexa/auditory system"
						12 "Radiology"
						13 "Pathology and lab"
						14 "Medicine";
# delimit cr
label values codeGroup codeGrouplab

* make a table
destring hcpcs, replace
gen percent = 1/_N
collapse (count) nCodes=hcpcs (sum) percent, by(codeGroup)
replace percent = round(percent*100,0.01)
decode codeGroup, gen(Specialty)

keep Specialty nCodes percent
order Specialty nCodes percent

dataout, save("$tabdir/re_timed_codes_specialty.tex") tex replace


************************************
*		housekeeping
************************************

log close



