************************************
**
** Revision: does wRVU/totRVU vary much?
**
************************************

/* If wRVU/totRVU *increases* with code intensity
(i.e. only wRVU increases with intensity, but other
components of RVU remain relatively unchanged),
then the flagged physicians' low hourly revenue
would be mechanical because:

HourlyRev = TotRev / TotHours
		  = (wRVU + peRVU + mpRVU)*CF / (wRVU * hours/wRVU)
		  = Const * (totRVU / wRVU),
which *decreases* with code intensity (i.e. flag status).

This do-file tests whether this is the case by
looking at the variation of (wRVU/totRVU) across
codes in general and code intensities w/i the same
cluster in particular.
*/


************************************
*       housekeeping
************************************
* need -dataout- package
ssc install dataout

version 11.2
include "config.do"
clear all

local home : env HOME
if "`home'"=="" {
	local home : env HOMEPATH
}
// global rootdir "`home'/Documents/Workplace/Medicare_payment"
global datadir "$rootdir/output"
global tempdir "/tmp"
global savedir "$rootdir/output"
global tabdir "$rootdir/tex"

cap log close
log using "$savedir/re_wRVU_to_totRVU.txt", text replace

set more off



************************************
* list of HCPCS codes with RVUs, time, and cluster info
************************************ 

* gen total RVUs for all codes (Physician Fee Schedule 2012)
insheet using "$rootdir/input/PPRRVU12.csv", clear
sort hcpcs

qui su cf
local CF = string(r(mean), "%7.3f")
di "**** Conversion Factor = $`CF' per RVU ****"


keep hcpcs workrvu pervu_officef mprvu
duplicates drop hcpcs, force

ren workrvu wRVU
ren pervu_officef peRVU
ren mprvu mpRVU
gen totRVU = wRVU + peRVU + mpRVU
lab var totRVU "total RVU (wRVU+peRVU+mpRVU)"

gen w2tRVU = wRVU/totRVU*100
lab var w2tRVU "wRVU/totRVU (%)"

* keep codes that showed up in Medicare B FFS 2012
merge 1:1 hcpcs using "$savedir/servTimeList.dta", keep(matched)
drop _merge hcpcsCateg

saveold "$savedir/hcpcs_wRVU_totRVU.dta", replace

************************************
* variation in (wRVU/totRVU) in general
************************************ 

use "$savedir/hcpcs_wRVU_totRVU.dta", clear

su w2tRVU totRVU if totRVU!=0, d
su w2tRVU totRVU if totRVU!=0 & EMgroup!=., d

qui reg w2tRVU totRVU, r
est store w2tRVU_1

qui xi: reg w2tRVU totRVU i.codeGroup, r
est store w2tRVU_2

qui xi: reg w2tRVU codeComplx i.EMgroup, r
est store w2tRVU_3

qui xi: reg w2tRVU i.codeComplx i.EMgroup, r
est store w2tRVU_4

local tabOption "se obslast brackets star(* 0.10 ** 0.05 *** 0.01) nogaps ar2 nolines compress depvars label"
esttab w2tRVU_* using "$tabdir/re_wRVU_to_totRVU.tex", ///
	keep(totRVU codeComplx _IcodeCompl_2 _IcodeCompl_3 _IcodeCompl_4 _IcodeCompl_5) ///
	replace `tabOption'


************************************
*	Housekeeping
************************************
log close
