*Performs the authors' analysis by reconstructing service counts based on the claims data
*This version uses the original methodology and data for the time estimates (and list of flagged physicians)


clear all

// global rootdir "/medicare/physician times"
global datadir "$rootdir/input"
global tempdir "$rootdir/temp"
global savedir "$rootdir/output"

*change npi to string in the output data from the time analysis for merging with carrier claims data
use "$savedir/flaggedlist1.dta",clear
tostring npi,replace
save "$savedir/flaggedlist1.dta",replace
use "$savedir/flaggedlist_old.dta",clear
tostring npi,replace
save "$savedir/flaggedlist_old.dta",replace

use "$savedir/flaggedlist_npi1.dta",clear
tostring npi,replace
save "$savedir/flaggedlist_npi1.dta",replace
use "$savedir/flaggedlist_npi_old.dta",clear
tostring npi,replace
save "$savedir/flaggedlist_npi_old.dta",replace

use "$savedir/phy_serv_time_1.dta",clear
tostring npi,replace
save "$savedir/phy_serv_time_1.dta",replace
use "$savedir/phy_serv_time_old.dta",clear
tostring npi,replace
save "$savedir/phy_serv_time_old.dta",replace


use "$savedir/phy_time_1.dta",clear
tostring npi,replace
save "$savedir/phy_time_1.dta",replace
use "$savedir/phy_time_old.dta",clear
tostring npi,replace
save "$savedir/phy_time_old.dta",replace

use "$savedir/phy_time_1.dta",clear
rename impFlag1_2013 flagged
keep npi flagged totHrsPerWk_2013
save "$savedir/flaggedlist_all1.dta",replace // list of all physicians from the Utilization data analysis

use "$savedir/phy_time_old.dta",clear
rename impFlag1_2013 flagged
keep npi flagged totHrsPerWk_2013
save "$savedir/flaggedlist_all_old.dta",replace



*compares docs in claims and docs in utilization data
use "/medicare/Extracted Files/car/car_2013/car_line_2013.dta",clear


drop prf_upin clm_type betos hcthgbrs hcthgbtp carr_line_clia_lab_num carr_line_ansthsa_unit_cnt //drop some unneccessaries
drop prf_prfl prgrpnpi prv_type hcfaspcl prtcptg astnt_cd typsrvcb plcsrvc lclty_cd mtus_cnt mtus_ind line_icd_dgns_cd lnndccd
drop lprpaycd lsbmtchg pmtindsw ded_sw


*limit to accepted claims
gen accept=0
replace accept=1 if prcngind=="A" | prcngind=="S" | prcngind=="R"
keep if accept==1

rename prf_npi npi
egen temp=tag(npi)
keep if temp==1

merge m:1 npi using "$savedir/flaggedlist_all_old.dta"

tab _merge if flagged==0 // unflagged in 2017 paper in claims data if _merge==3 
tab _merge if flagged==1 // flagged in 2017 paper in claims data if _merge==3
count if flagged==. // # physicians in claims but not in utilization data



********************
insheet using "$datadir/PPRRVU13.csv", clear
keep hcpcs workrvu
rename workrvu wRVU2013
gsort hcpcs -wRVU
duplicates drop hcpcs, force
save "$savedir/hcpcslist_wrvu2013.dta", replace



use "/medicare/Extracted Files/car/car_2013/car_line_2013.dta",clear

drop prf_upin clm_type betos hcthgbrs hcthgbtp carr_line_clia_lab_num carr_line_ansthsa_unit_cnt //drop some unneccessaries
drop prf_prfl prgrpnpi prv_type prvstate hcfaspcl prtcptg astnt_cd typsrvcb plcsrvc lclty_cd mtus_cnt mtus_ind line_icd_dgns_cd lnndccd
drop lbenpmt lprvpmt ldedamt lprpaycd lprpdamt coinamt lsbmtchg  pmtindsw ded_sw


gen accept=0
replace accept=1 if prcngind=="A" | prcngind=="S" | prcngind=="R"

keep if accept==1
drop accept

rename prf_npi npi
rename hcpcs_cd hcpcs

egen temp=sum(linepmt),by(npi)  // total medicare payment by provider
egen temp1=sum(lalowchg),by(npi)  // total allowed charges by provider
egen temp2=sum(srvc_cnt),by(npi) // total service counts by provider

egen temp3=tag(claimno npi)
egen nclaims=sum(temp3),by(npi)
egen nlines=count(temp3),by(npi)

egen temp4=tag(dsysrtky npi)
egen nbene=sum(temp4),by(npi)

merge m:1 hcpcs using "$savedir/hcpcslist_wrvu2013.dta",nogen keep(match master)


replace wRVU2013=0 if wRVU2013==.

gen twrvu=srvc_cnt*wRVU2013
egen tot_wrvu1=sum(twrvu),by(npi)

egen mwrvu=mean(wRVU2013),by(npi)
gen tot_wrvu=nlines*mwrvu

egen doc=tag(npi) // tag and keep unique docs
keep if doc


merge 1:1 npi using "$savedir/flaggedlist_all_old.dta"
keep if _merge==3 // drop all docs that don't appear in the utilization file
drop _merge



/* This creates ratios based on the provider utilization summary table and includes 100% of services */

merge 1:1 npi using "/medicare/physician times/phys_totals.dta"
keep if _merge==3
drop _merge

gen medpayratio=temp/totalmedicarepaymentamount


tab flagged if medpayratio<.001
tab flagged if medpayratio<.01

gen medpayratio1=medpayratio
replace medpayratio1=.01 if medpayratio<.01

su temp temp2 nclaims nlines nbene tot_wrvu tot_wrvu1 medpayratio medpayratio1
su temp temp2 nclaims nlines nbene tot_wrvu tot_wrvu1 medpayratio medpayratio1 if flagged==0
su temp temp2 nclaims nlines nbene tot_wrvu tot_wrvu1 medpayratio medpayratio1 if flagged==1
su temp temp2 nclaims nlines nbene tot_wrvu tot_wrvu1 medpayratio medpayratio1 if flagged==0 & totHrsPerWk_2013>20 & totHrsPerWk_2013!=.
