clear all

// global rootdir "/medicare/physician times"
global datadir "$rootdir/input"
global tempdir "$rootdir/temp"
global savedir "$rootdir/output"

*look at the docs flagged in the the utilization data but not in the claims data using the original time values

*create a list of docs who do any of the 3 common post-service care only codes (call them the cataract docs because 2/3 codes are cataract surgery)
use "$savedir/phy_serv_time_2.dta",clear
keep if hcpcs=="66984" | hcpcs=="66821" | hcpcs=="66982"
drop if nServ2013==0

keep npi
egen temp=tag(npi)
keep if temp==1
drop temp

save "$savedir/cataract_dr.dta",replace


*create a list of mismatched docs
use "$savedir/carr_times_old.dta",clear

merge 1:1 npi using "$savedir/cataract_dr.dta"
gen cataract=0
replace cataract=1 if _merge==3
drop _merge

gen mismatch_docs= (totHrsPerWk_noadj<=100 & flag_old==1 & totHrsPerWk_noadj!=.)
gen flag_both=(totHrsPerWk_noadj>100 & totHrsPerWk_noadj!=. & flag_old==1)
gen flag_new=(totHrsPerWk_noadj>100 & totHrsPerWk_noadj!=. )

tab mismatch_docs cataract if flag_old==1 // a lot of the mismatch is due to the cataract docs
count if totHrsPerWk_noadj>90 & mismatch_doc==1 & cataract==0 // not cataract mismatched flagged at a slightly lower threshold
count if totHrsPerWk_noadj>80 & mismatch_doc==1 & cataract==0

keep if mismatch_docs==1 | flag_both==1 | flag_new
keep npi mismatch_docs flag_both flag_new

save "$savedir/mismatch_docs.dta",replace


use "$savedir/phy_serv_time_old.dta",clear
keep if hcpcs=="66984" | hcpcs=="66821" | hcpcs=="66982"
drop if nServ2013==0

*merge m:1 npi using "$savedir/mismatch_docs.dta",keep(match) nogen

gen totHrs_1=nServUniq2013*tTotal/60 // total time assuming a srvc_cnt of 1

gen Paid=meanPaid2013*nServ2013
gen allow=meanAllow2013*nServ2013
egen tot_srvc=sum(nServ2013),by(npi)
egen tot_srvc_uniq=sum(nServUniq2013),by(npi)
egen tot_bene=sum(nBene2013),by(npi)
egen tot_pay=sum(Paid),by(npi)
egen tot_allow=sum(allow),by(npi)
egen tot_time=sum(totHrs_2013),by(npi)
egen tot_time_1=sum(totHrs_1),by(npi)


gen wage=tot_allow/tot_time

egen doc=tag(npi)
keep if doc

merge 1:1 npi using "$savedir/flaggedlist_old1.dta",keep(match) nogen

merge 1:1 npi using "$savedir/mismatch_docs.dta"
keep if _merge==1 | _merge==3
drop _merge

mvencode mismatch_docs flag_both flag_new,mv(0) o
gen hrs_per_wk=tot_time/51
gen hrs_per_wk1=tot_time_1/51

su tot_srvc tot_srvc_uniq tot_bene tot_pay tot_allow hrs_per_wk hrs_per_wk1 wage,d
su tot_srvc tot_srvc_uniq tot_bene tot_pay tot_allow hrs_per_wk hrs_per_wk1 wage if flag_both==1,d
su tot_srvc tot_srvc_uniq tot_bene tot_pay tot_allow hrs_per_wk hrs_per_wk1 wage if mismatch_docs==1,d
su tot_srvc tot_srvc_uniq tot_bene tot_pay tot_allow hrs_per_wk hrs_per_wk1 wage if flag_new==1 & flagged==0,d
su tot_srvc tot_srvc_uniq tot_bene tot_pay tot_allow hrs_per_wk hrs_per_wk1 wage if flag_new==0 & flagged==0 & flag20==1,d




keep if mismatch_docs==1
keep npi tot_time tot_time_1

*recalculate time in the utilization data for the cataract mismatched assuming a service count of 1 for the 3 procedures
merge 1:1 npi using "$savedir/phy_time_old.dta",keep(match)



gen new_time=(totHrs_2013-tot_time+tot_time_1)/51  // assumes a service count of 1
gen new_time1=(totHrs_2013-tot_time+2*tot_time_1)/51 // assumes a service count of 2 (i.e. both eyes)
count if new_time>100
count if new_time1>100


