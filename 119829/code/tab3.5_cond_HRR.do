************************************
**
** Table 3.5: physician characteristics condtinal on HRR
**
************************************
************************************
* clean zip-hrr crosswalk
************************************
insheet using "$rootdir/input/hrr_zip12.csv", clear names
keep zipcode12 hrrnum
ren zipcode12 zip5
ren hrrnum hrr

tostring zip5, replace
replace zip5="0"+zip5 if length(zip5)==4
replace zip5="00"+zip5 if length(zip5)==3

duplicates drop zip5, force //none
save "$datadir/zip_hrr.dta", replace

************************************
* phy time data
************************************
use "$datadir/phy_time.dta", clear

* FOCUS ON THE SUBSAMPLE WITH 20+ HOURS/WEEK
keep if totHrsPerWk_2012>20 | totHrsPerWk_2013>20

* prep to merge with hrr data
tostring phyZip, replace
gen str5 zip5 = substr(phyZip,1,5) if length(phyZip)==9
replace zip5 = "0"+substr(phyZip,1,4) if length(phyZip)==8
replace zip5 = "00"+substr(phyZip,1,3) if length(phyZip)==7
replace zip5 = phyZip if length(phyZip)==5
replace zip5 = "0"+phyZip if length(phyZip)==4
replace zip5 = "00"+phyZip if length(phyZip)==3

* link to hrr
merge m:1 zip5 using "$datadir/zip_hrr.dta", keep(master matched)

************************************
* see if characteristics differ across groups conditional on HRR
************************************
* prep: group indicators
gen byte never = impFlag1_2012+impFlag1_2013==0
gen byte ever  = impFlag1_2012+impFlag1_2013>0
gen byte both  = impFlag1_2012+impFlag1_2013==2
gen byte f2012o = impFlag1_2012==1 & impFlag1_2013==0
gen byte f2013o = impFlag1_2013==1 & impFlag1_2012==0
	
* set up postfile
local postTab3_5f "Ever F2012 F2013 F2012only Fboth F2013only"
di "`postTab3_5f'"

cap postclose tab3_5
postfile tab3_5 str50 r double(`postTab3_5f') using "$savedir/table3_5.dta", replace

* set up row titles
local row1 "1(male)"
local row3 "1(MD)"
local row5 "Experience (years)"
local row7 "# providers in group service"
local row9 "# hospital affiliations"
local row11 "1(in Medicare)"
local row13 "1(in ERX)"
local row15 "1(in PQRS)"
local row17 "1(in EHR)"
local row19 "Types of codes 2012"
local row21 "Types of codes 2013"
local row23 "Types of E/M codes 2012"
local row25 "Types of E/M codes 2013"

* fill in rows
local postTab3_5
foreach pval in `postTab3_5f' {
    local postTab3_5 "`postTab3_5' (`pval')"
    }

local ixRow = 1
local compvars male isMD experYear nPhyInGroup hosAff inMedicare inERX inPQRS inEHR nCode_2012 nCode_2013 nCodeEM_2012 nCodeEM_2013
foreach v of varlist `compvars' {

	* ... column 1 (all)
	* (adapted from Tab 3, n/a here)
	
	* ... column 2 (never) 
	* (adapted from Tab 3, n/a here)

	* ... column 3 (ever)
	qui xi: reg `v' ever i.hrr, vce(cluster hrr)
	mat regRbeta = e(b)
	mat regRse   = e(V)
	local meanEv = regRbeta[1,1]
	local seEv   = sqrt(regRse[1,1])
	
	* ... column 4 (2012)
	qui xi: reg `v' impFlag1_2012 i.hrr if impFlag1_2012==1 | never==1, vce(cluster hrr)
	mat regRbeta = e(b)
	mat regRse   = e(V)
	local mean12 = regRbeta[1,1]
	local se12   = sqrt(regRse[1,1])
	
	* ... column 5 (2013)
	qui xi: reg `v' impFlag1_2013 i.hrr if impFlag1_2013==1 | never==1, vce(cluster hrr)
	mat regRbeta = e(b)
	mat regRse   = e(V)
	local mean13 = regRbeta[1,1]
	local se13   = sqrt(regRse[1,1])
	
	* ... column 6 (F2012only)
	qui xi: reg `v' f2012o i.hrr if f2012o==1 | never==1, vce(cluster hrr)
	mat regRbeta = e(b)
	mat regRse   = e(V)
	local mean12o = regRbeta[1,1]
	local se12o  = sqrt(regRse[1,1])
	
	* ... column 7 (both)
	qui xi: reg `v' both i.hrr if both==1 | never==1, vce(cluster hrr)
	mat regRbeta = e(b)
	mat regRse   = e(V)
	local meanbo = regRbeta[1,1]
	local sebo   = sqrt(regRse[1,1])

	* ... column 8 (F2013only)
	qui xi: reg `v' f2013o i.hrr if f2013o==1 | never==1, vce(cluster hrr)
	mat regRbeta = e(b)
	mat regRse   = e(V)
	local mean13o = regRbeta[1,1]
	local se13o  = sqrt(regRse[1,1])
	
	* post means and p-values (odd-num. rows)
	scalar Ever	 	= `meanEv'
	scalar F2012 	= `mean12'
	scalar F2013 	= `mean13'
	scalar F2012only= `mean12o'
	scalar Fboth 	= `meanbo'
	scalar F2013only= `mean13o'
	
	post tab3_5 ("`row`ixRow''") `postTab3_5'
	local ixRow = `ixRow'+1

	* post se of means (even-num. rows)
	scalar Ever	 	= `seEv'
	scalar F2012 	= `se12'
	scalar F2013 	= `se13'
	scalar F2012only= `se12o'
	scalar Fboth 	= `sebo'
	scalar F2013only= `se13o'

	post tab3_5 ("`row`ixRow''") `postTab3_5'
	local ixRow = `ixRow'+1
	
	}

* the last row: sample sizes
qui su npi if impFlag1_2012+impFlag1_2013>0
scalar Ever = r(N)

qui su npi if impFlag1_2012==1
scalar F2012 = r(N)

qui su npi if impFlag1_2013==1
scalar F2013 = r(N)

qui su npi if impFlag1_2012==1 & impFlag1_2013==0
scalar F2012only = r(N)

qui su npi if impFlag1_2012==1 & impFlag1_2013==1
scalar Fboth = r(N)

qui su npi if impFlag1_2012==0 & impFlag1_2013==1
scalar F2013only = r(N) 

post tab3_5 ("N") `postTab3_5'
	
* close post file
postclose tab3_5

