************************************
**
** Table 1: Robustness - possible residents
**
************************************

use "$datadir/phy_time.dta", clear

* find potential residents 
* ... crude search (maximum 7 years of residency, of which the final year is usually for research)
decode phyType, gen(phyTypeStr)
tab phyTypeStr if impFlag1_2012==1 & gradYear>=2005

* ... link with residency lengths
gen isResident = 0
	replace isResident=1 if gradYear>=2008 & /// 3-year residencies (+1 Yr research)
		( phyTypeStr=="Cardiology" | phyTypeStr=="Dermatology" | ///
		  phyTypeStr=="Family Practice" | phyTypeStr=="Internal Medicine" | ///
		  phyTypeStr=="Pathology" | phyTypeStr=="Physical Therapist" | ///
		  phyTypeStr=="Pulmonary Disease" )
		  
	replace isResident=1 if gradYear>=2009 & /// 2-year residencies (+1 Yr)
		 ( phyTypeStr=="Ophthalmology" )
		 
	replace isResident=1 if gradYear>=2010 & /// 1-year residencies (+1 Yr)
		 ( phyTypeStr=="Nurse Practitioner" | phyTypeStr=="Optometry")

* set up postfile
local postTab1Sf
forval i=1/8 {
	local postTab1Sf "`postTab1Sf' var`i'"
	}
di "`postTabSf'"

cap postclose tab1S
postfile tab1S str40 r double(`postTab1Sf') using "$savedir/table1S_residency.dta", replace

* set up column titles
post tab1S ("weekly hours above") (80) (80) (100) (100) (112) (112) (168) (168)
post tab1S ("") (2012) (2013) (2012) (2013) (2012) (2013) (2012) (2013)

* set up row titles
local row1 "Number of physicians flagged"
local row2 "Number of possible residents"
local row3 "Possible residents/flagged (%)"

* fill in rows
local postTab1S
foreach pval in `postTab1Sf' {
    local postTab1S "`postTab1S' (`pval')"
    }

* ... row 1
local ixVar = 1
foreach v of varlist impFlag0_2012 impFlag0_2013 impFlag1_2012 impFlag1_2013 impFlag2_2012 impFlag2_2013 impFlag3_2012 impFlag3_2013 {
	qui su `v'
	scalar var`ixVar' = r(sum)
	local ixVar = `ixVar'+1
	}

post tab1S ("`row1'") `postTab1S'

* ... row 2
local ixVar = 1
foreach v of varlist impFlag0_2012 impFlag0_2013 impFlag1_2012 impFlag1_2013 impFlag2_2012 impFlag2_2013 impFlag3_2012 impFlag3_2013 {
	qui su `v' if isResident==1
	scalar var`ixVar' = r(sum)
	local ixVar = `ixVar'+1
	}

post tab1S ("`row2'") `postTab1S'

* ... row 3
local ixVar = 1
foreach v of varlist impFlag0_2012 impFlag0_2013 impFlag1_2012 impFlag1_2013 impFlag2_2012 impFlag2_2013 impFlag3_2012 impFlag3_2013 {
	qui su `v' if isResident==1
	local resident = r(sum)
	qui su `v'
	local all = r(sum)
	
	scalar var`ixVar' = `resident'/`all'*100
	local ixVar = `ixVar'+1
	}

post tab1S ("`row3'") `postTab1S'


* close post file
postclose tab1S
