************************************
**
** Table 2: Overlapping flags across years
**
************************************
include "config.do"
global datadir "$savedir"

use "$datadir/phy_time.dta", clear

tab impFlag0*
tab impFlag1*
tab impFlag2*
tab impFlag3*

* set up postfile
local postTab2f
forval i=1/8 {
	local postTab2f "`postTab2f' var`i'"
	}
di "`postTab2f'"

cap postclose tab2
postfile tab2 str100 r double(`postTab2f') using "$savedir/table2.dta", replace

* set up column titles
post tab2 ("weekly hours above") (80) (80) (100) (100) (112) (112) (168) (168)

* set up row titles
local row1 "Flagged in 2012 only"
local row2 "Flagged in 2012 and 2013"
local row3 "Flagged in 2013 only"

* fill in rows
local postTab2
foreach pval in `postTab2f' {
    local postTab2 "`postTab2' (`pval')"
    }

* ... row 1
local ixVar = 1
foreach v in impFlag0 impFlag1 impFlag2 impFlag3 {
	qui su `v'_2012 if `v'_2013 == 0
	local only = r(sum)
	scalar var`ixVar' = `only'
	local ixVar = `ixVar' + 1
	
	qui su `v'_2012 if `v'_2013 == 1
	local both = r(sum)
	scalar var`ixVar' = `only'/(`only'+`both')
	local ixVar = `ixVar' + 1
	}

post tab2 ("`row1'") `postTab2'

* ... row 2
local ixVar = 1
foreach v in impFlag0 impFlag1 impFlag2 impFlag3 {
	qui su `v'_2012 if `v'_2013 == 1
	scalar var`ixVar' = r(sum)
	local ixVar = `ixVar' + 1
	
	scalar var`ixVar' = .
	local ixVar = `ixVar' + 1
	}

post tab2 ("`row2'") `postTab2'

* ... row 3
local ixVar = 1
foreach v in impFlag0 impFlag1 impFlag2 impFlag3 {
	qui su `v'_2013 if `v'_2012 == 0
	local only = r(sum)
	scalar var`ixVar' = `only'
	local ixVar = `ixVar' + 1
	
	qui su `v'_2013 if `v'_2012 == 1
	local both = r(sum)
	scalar var`ixVar' = `only'/(`only'+`both')
	local ixVar = `ixVar' + 1
	}

post tab2 ("`row3'") `postTab2'

* close post file
postclose tab2
