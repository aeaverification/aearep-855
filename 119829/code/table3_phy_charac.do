************************************
**
** Table 3: physician characteristics (flagged vs other)
**
************************************
use "$datadir/phy_time.dta", clear

* FOCUS ON THE SUBSAMPLE WITH 20+ HOURS/WEEK
keep if totHrsPerWk_2012>20 | totHrsPerWk_2013>20

* set up postfile
local postTab3f "All Never Ever F2012 F2013 F2012only Fboth F2013only"
di "`postTab3f'"

cap postclose tab3
postfile tab3 str100 r double(`postTab3f') using "$savedir/table3.dta", replace

* set up row titles
local row1 "1(male)"
local row3 "1(MD)"
local row5 "Experience (years)"
local row7 "# providers in group service"
local row9 "# hospital affiliations"
local row11 "1(in Medicare)"
local row13 "1(in ERX)"
local row15 "1(in PQRS)"
local row17 "1(in EHR)"
local row19 "Types of codes 2012"
local row21 "Types of codes 2013"
local row23 "Types of E/M codes 2012"
local row25 "Types of E/M codes 2013"

* fill in rows
local postTab3
foreach pval in `postTab3f' {
    local postTab3 "`postTab3' (`pval')"
    }

local ixRow = 1
local compvars male isMD experYear nPhyInGroup hosAff inMedicare inERX inPQRS inEHR nCode_2012 nCode_2013 nCodeEM_2012 nCodeEM_2013
foreach v of varlist `compvars' {
	
	* ... column 1 (all)
	qui tabstat `v', stats(mean sem) save
	mat tabstatR  = r(StatTotal)
	local meanAll = tabstatR[1,1]
	local seAll  = tabstatR[2,1]
	
	* ... column 2 (never) 
	qui tabstat `v' if impFlag1_2012+impFlag1_2013==0, stats(mean sem) save
	mat tabstatR  = r(StatTotal)
	local meanNe  = tabstatR[1,1]
	local seNe   = tabstatR[2,1]
	
	* ... column 3 (ever)
	qui tabstat `v' if impFlag1_2012+impFlag1_2013>0, stats(mean sem) save
	mat tabstatR  = r(StatTotal)
	local meanEv  = tabstatR[1,1]
	local seEv   = tabstatR[2,1]
	
	* ... column 4 (2012)
	qui tabstat `v' if impFlag1_2012==1, stats(mean sem) save
	mat tabstatR  = r(StatTotal)
	local mean12  = tabstatR[1,1]
	local se12   = tabstatR[2,1]
	
	* ... column 5 (2013)
	qui tabstat `v' if impFlag1_2013==1, stats(mean sem) save
	mat tabstatR  = r(StatTotal)
	local mean13  = tabstatR[1,1]
	local se13   = tabstatR[2,1]
	
	* ... column 6 (F2012only)
	qui tabstat `v' if impFlag1_2012==1 & impFlag1_2013==0, stats(mean sem) save
	mat tabstatR  = r(StatTotal)
	local mean12o = tabstatR[1,1]
	local se12o   = tabstatR[2,1]
	
	* ... column 7 (both)
	qui tabstat `v' if impFlag1_2012==1 & impFlag1_2013==1, stats(mean sem) save
	mat tabstatR  = r(StatTotal)
	local meanbo = tabstatR[1,1]
	local sebo   = tabstatR[2,1]

	* ... column 8 (F2013only)
	qui tabstat `v' if impFlag1_2012==0 & impFlag1_2013==1, stats(mean sem) save
	mat tabstatR  = r(StatTotal)
	local mean13o = tabstatR[1,1]
	local se13o   = tabstatR[2,1]

	* post means and p-values (odd-num. rows)
	scalar All   	= `meanAll'
	scalar Never 	= `meanNe'
	scalar Ever	 	= `meanEv'
	scalar F2012 	= `mean12'
	scalar F2013 	= `mean13'
	scalar F2012only= `mean12o'
	scalar Fboth 	= `meanbo'
	scalar F2013only= `mean13o'
	
	post tab3 ("`row`ixRow''") `postTab3'
	local ixRow = `ixRow'+1

	* post se of means (even-num. rows)
	scalar All   	= `seAll'
	scalar Never 	= `seNe'
	scalar Ever	 	= `seEv'
	scalar F2012 	= `se12'
	scalar F2013 	= `se13'
	scalar F2012only= `se12o'
	scalar Fboth 	= `sebo'
	scalar F2013only= `se13o'
	*foreach sname in p_ev_ne p_12_ne p_13_ne p_12o_bo p_13o_bo {
	*	scalar `sname' = .
	*	}
	post tab3 ("`row`ixRow''") `postTab3'
	local ixRow = `ixRow'+1
	
	}

* the last row: sample sizes
qui su npi
scalar All = r(N)

qui su npi if impFlag1_2012+impFlag1_2013==0
scalar Never = r(N)

qui su npi if impFlag1_2012+impFlag1_2013>0
scalar Ever = r(N)

qui su npi if impFlag1_2012==1
scalar F2012 = r(N)

qui su npi if impFlag1_2013==1
scalar F2013 = r(N)

qui su npi if impFlag1_2012==1 & impFlag1_2013==0
scalar F2012only = r(N)

qui su npi if impFlag1_2012==1 & impFlag1_2013==1
scalar Fboth = r(N)

qui su npi if impFlag1_2012==0 & impFlag1_2013==1
scalar F2013only = r(N) 

post tab3 ("N") `postTab3'

	
* close post file
postclose tab3


