************************************
**
** Table 5.5: decomposing the hours (cond'l on HRR)
**
************************************
local ixFlag = substr("$flag_2012",1,8)


use "$datadir/phy_time.dta", clear

* FOCUS ON THE SUBSAMPLE WITH 20+ HOURS/WEEK
keep if totHrsPerWk_2012>20 | totHrsPerWk_2013>20

* prep to merge with hrr data
tostring phyZip, replace
gen str5 zip5 = substr(phyZip,1,5) if length(phyZip)==9
replace zip5 = "0"+substr(phyZip,1,4) if length(phyZip)==8
replace zip5 = "00"+substr(phyZip,1,3) if length(phyZip)==7
replace zip5 = phyZip if length(phyZip)==5
replace zip5 = "0"+phyZip if length(phyZip)==4
replace zip5 = "00"+phyZip if length(phyZip)==3

* link to hrr
merge m:1 zip5 using "$datadir/zip_hrr.dta", keep(master matched)

* create vars of interest
gen nPerBene_2012 = nServ_2012/nBene_2012	//...per-patient volume
gen nPerBene_2013 = nServ_2013/nBene_2013
gen nPerHour_2012 = nServ_2012/totHrs_2012	//...per-hour volume
gen nPerHour_2013 = nServ_2013/totHrs_2013
gen nBenePerDay_2012 = nBene_2012/366		//...per-hour patient
gen nBenePerDay_2013 = nBene_2013/365
gen nBenePerHour_2012 = nBene_2012/totHrs_2012		//...per-hour patient
gen nBenePerHour_2013 = nBene_2013/totHrs_2013
gen payPerServ_2012 = totPaid_2012/nServ_2012*1000	// ...pay per service
gen payPerServ_2013 = totPaid_2013/nServ_2013*1000
gen payPerBene_2012 = totPaid_2012/nBene_2012*1000	//...pay per patient
gen payPerBene_2013 = totPaid_2013/nBene_2013*1000
gen payPerHour_2012 = totPaid_2012/totHrs_2012*1000	// ...pay per hour
gen payPerHour_2013 = totPaid_2013/totHrs_2013*1000

* set up postfile
local postTab5_5f "Flagged2012 Flagged2013 meanUF2012 meanUF2013"
di "`postTab5_5f'"

cap postclose tab5_5
postfile tab5_5 str50 r double(`postTab5_5f') using "$savedir/table8_decomp_HRR_`ixFlag'.dta", replace

* set up row titles
local row1 "Num. of services provided"
local row3 "Num. of services per patient"
local row5 "Num. of services provided per hour"
local row7 "Num. of patients"
local row9 "Num. of patients per day"
local row11 "Num. of patients per hour"
local row13 "Medicare payment per service ($)"
local row15 "Medicare payment per patient ($)"
local row17 "Medicare payment per hour ($)"

* fill in rows
local postTab5_5
foreach pval in `postTab5_5f' {
    local postTab5_5 "`postTab5_5' (`pval')"
    }
	
local ixRow = 1
local compvars nServ nPerBene nPerHour nBene nBenePerDay nBenePerHour payPerServ payPerBene payPerHour
foreach v in `compvars' {
	* ... column 1
	qui xi: reg `v'_2012 $flag_2012 i.hrr, vce(cluster hrr)
	mat regRbeta = e(b)
	mat regRse   = e(V)
	local meanF12 = regRbeta[1,1]
	local sdF12  = sqrt(regRse[1,1])
	
	* ... column 2
	qui xi: reg `v'_2013 $flag_2013 i.hrr, vce(cluster hrr)
	mat regRbeta = e(b)
	mat regRse   = e(V)
	local meanF13 = regRbeta[1,1]
	local sdF13  = sqrt(regRse[1,1])
	
	* ... column 3
	qui su `v'_2012 if $flag_2012==0
	local meanUF2012 = r(mean)
	local sdUF2012 = .
	
	* ... column 4
	qui su `v'_2013 if $flag_2013==0
	local meanUF2013 = r(mean)
	local sdUF2013 = .
	
	* post means and p-values (odd-num. rows)
	scalar Flagged2012   = `meanF12'
	scalar Flagged2013	 = `meanF13'
	scalar meanUF2012    = `meanUF2012'
	scalar meanUF2013    = `meanUF2013'
	
	post tab5_5 ("`row`ixRow''") `postTab5_5'
	local ixRow = `ixRow'+1

	* post se of means (even-num. rows)
	scalar Flagged2012   = `sdF12'
	scalar Flagged2013	 = `sdF13'
	scalar meanUF2012    = `sdUF2012'
	scalar meanUF2013    = `sdUF2013'

	post tab5_5 ("`row`ixRow''") `postTab5_5'
	local ixRow = `ixRow'+1
	
	}

* the last row: sample sizes
qui su npi if $flag_2012==1
scalar Flagged2012 = r(N)

qui su npi if $flag_2013==1
scalar Flagged2013 = r(N)

qui su npi if $flag_2012==0
scalar meanUF2012 = r(N)

qui su npi if $flag_2013==0
scalar meanUF2013 = r(N)

post tab5_5 ("N") `postTab5_5'
	
* close post file
postclose tab5_5


