************************************
**
** Table 5.8: upcoding factor
**
************************************
local ixFlag = substr("$flag_2012",1,8)

use "$datadir/phy_time.dta", clear

* FOCUS ON THE SUBSAMPLE WITH 20+ HOURS/WEEK
keep if totHrsPerWk_2012>20 | totHrsPerWk_2013>20

* prep to merge with hrr data
tostring phyZip, replace
gen str5 zip5 = substr(phyZip,1,5) if length(phyZip)==9
replace zip5 = "0"+substr(phyZip,1,4) if length(phyZip)==8
replace zip5 = "00"+substr(phyZip,1,3) if length(phyZip)==7
replace zip5 = phyZip if length(phyZip)==5
replace zip5 = "0"+phyZip if length(phyZip)==4
replace zip5 = "00"+phyZip if length(phyZip)==3

* link to hrr
merge m:1 zip5 using "$datadir/zip_hrr.dta", keep(master matched)

* create vars of interest
keep npi male isMD experYear phyType hrr totHrsPerWk_* totPaid_* $flag_2012 $flag_2013
reshape long totHrsPerWk_ totPaid_ `ixFlag'_, i(npi male isMD experYear phyType hrr) j(year)
ren totHrsPerWk_ totHrsPerWk
ren totPaid_ totPaid
ren `ixFlag'_ `ixFlag'

* STEP 1: "FAIR HOURLY RATE" (using unflagged only)
gen hrate = totPaid*1000/totHrsPerWk/52

forval ixYear = 2012/2013 {
	xi: reg hrate male isMD experYear i.phyType i.hrr if `ixFlag'==0 & year==`ixYear'
	predict hrate_hat_`ixYear'
	}
	gen hrate_hat = hrate_hat_2012*(year==2012) + hrate_hat_2013*(year==2013)

* STEP 2: UPCODING FACTOR 1
gen upfact1 = totPaid*1000/hrate_hat/8/365

* STEP 2': UPCODING FACTOR 2
gen upfact2 = hrate_hat/hrate 

* summarize: table
* set up postfile
local postTab5_8f "Flagged_2012 Flagged_2013 Not_flagged_2012 Not_flagged_2013"
di "`postTab5_8f'"

cap postclose tab5_8
postfile tab5_8 str40 r double(`postTab5_8f') using "$savedir/table9_OPF_`ixFlag'.dta", replace

* set up row titles
local row1 "Reported hourly revenue (\$)"
local row3 "Predicted hourly revenue (\$)"
local row5 "Overbilling Potential Factor 1"
local row7 "Overbilling Potential Factor 2"

* fill in rows
local postTab5_8
foreach pval in `postTab5_8f' {
    local postTab5_8 "`postTab5_8' (`pval')"
    }
	
local ixRow = 1
local compvars hrate hrate_hat upfact1 upfact2
foreach v in `compvars' {
	* ... column 1-2
	forval ixYear = 2012/2013 {
		qui tabstat `v' if `ixFlag'==1 & year==`ixYear', stats(mean sem) save
		mat tabstatR  = r(StatTotal)
		local meanF`ixYear' = tabstatR[1,1]
		local sdF`ixYear'   = tabstatR[2,1]
		}
	
	* ... column 3-4
	forval ixYear = 2012/2013 {
		qui tabstat `v' if `ixFlag'==0 & year ==`ixYear', stats(mean sem) save
		mat tabstatR  = r(StatTotal)
		local meanNF`ixYear' = tabstatR[1,1]
		local sdNF`ixYear'   = tabstatR[2,1]
		}
		
	* post means and p-values (odd-num. rows)
	forval ixYear = 2012/2013 {
		scalar Flagged_`ixYear' = `meanF`ixYear' ='
		scalar Not_flagged_`ixYear' = `meanNF`ixYear' ='
		}
		
	post tab5_8 ("`row`ixRow''") `postTab5_8'
	local ixRow = `ixRow'+1

	* post se of means (even-num. rows)
	forval ixYear = 2012/2013 {
		scalar Flagged_`ixYear' = `sdF`ixYear' ='
		scalar Not_flagged_`ixYear' = `sdNF`ixYear' ='
		}

	post tab5_8 ("`row`ixRow''") `postTab5_8'
	local ixRow = `ixRow'+1
	
	}

* the last row: sample sizes
forval ixYear = 2012/2013 {
	qui su npi if `ixFlag'==1 & year==`ixYear'
	scalar Flagged_`ixYear' = r(N)
	
	qui su npi if `ixFlag'==0 & year==`ixYear'
	scalar Not_flagged_`ixYear' = r(N)
	}


post tab5_8 ("N") `postTab5_8'
	
* close post file
postclose tab5_8

