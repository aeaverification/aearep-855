************************************
**
** Table 5: decomposing the hours (quantity vs intensity)
**
************************************
use "$datadir/phy_time.dta", clear

* FOCUS ON THE SUBSAMPLE WITH 20+ HOURS/WEEK
keep if totHrsPerWk_2012>20 | totHrsPerWk_2013>20

* create vars of interest
gen nPerBene_2012 = nServ_2012/nBene_2012	//...per-patient volume
gen nPerBene_2013 = nServ_2013/nBene_2013
gen nPerHour_2012 = nServ_2012/totHrs_2012	//...per-hour volume
gen nPerHour_2013 = nServ_2013/totHrs_2013
gen nBenePerDay_2012 = nBene_2012/366		//...per-hour patient
gen nBenePerDay_2013 = nBene_2013/365
gen nBenePerHour_2012 = nBene_2012/totHrs_2012		//...per-hour patient
gen nBenePerHour_2013 = nBene_2013/totHrs_2013
gen payPerServ_2012 = totPaid_2012/nServ_2012*1000	// ...pay per service
gen payPerServ_2013 = totPaid_2013/nServ_2013*1000
gen payPerBene_2012 = totPaid_2012/nBene_2012*1000	//...pay per patient
gen payPerBene_2013 = totPaid_2013/nBene_2013*1000
gen payPerHour_2012 = totPaid_2012/totHrs_2012*1000	// ...pay per hour
gen payPerHour_2013 = totPaid_2013/totHrs_2013*1000

* set up postfile
local postTab5f "Flagged2012 Flagged2013 Unflagged2012 Unflagged2013 p2012 p2013"
di "`postTab5f'"

cap postclose tab5
postfile tab5 str100 r double(`postTab5f') using "$savedir/table5.dta", replace

* set up row titles
local row1 "Num. of services provided"
local row3 "Num. of services per patient"
local row5 "Num. of services provided per hour"
local row7 "Num. of patients"
local row9 "Num. of patients per day"
local row11 "Num. of patients per hour"
local row13 "Medicare payment per service ($)"
local row15 "Medicare payment per patient ($)"
local row17 "Medicare payment per hour ($)"

* fill in rows
local postTab5
foreach pval in `postTab5f' {
    local postTab5 "`postTab5' (`pval')"
    }
	
local ixRow = 1
local compvars nServ nPerBene nPerHour nBene nBenePerDay nBenePerHour payPerServ payPerBene payPerHour
foreach v in `compvars' {
	* ... column 1
	qui tabstat `v'_2012 if impFlag1_2012==1, stats(mean sem) save
	mat tabstatR  = r(StatTotal)
	local meanF12 = tabstatR[1,1]
	local sdF12   = tabstatR[2,1]
	
	* ... column 2
	qui tabstat `v'_2013 if impFlag1_2013==1, stats(mean sem) save
	mat tabstatR  = r(StatTotal)
	local meanF13 = tabstatR[1,1]
	local sdF13   = tabstatR[2,1]
	
	* ... column 3
	qui tabstat `v'_2012 if impFlag1_2012==0, stats(mean sem ) save
	mat tabstatR  = r(StatTotal)
	local meanN12 = tabstatR[1,1]
	local sdN12   = tabstatR[2,1]
	
	* ... column 4
	qui tabstat `v'_2013 if impFlag1_2013==0, stats(mean sem) save
	mat tabstatR  = r(StatTotal)
	local meanN13 = tabstatR[1,1]
	local sdN13   = tabstatR[2,1]

	* ... column 5-6 (Flagged vs uNflagged)
	qui: ttest `v'_2012, by(impFlag1_2012) unequal
	scalar p2012 = r(p)
	
	qui: ttest `v'_2013, by(impFlag1_2013) unequal
	scalar p2013 = r(p)
	
	* post means and p-values (odd-num. rows)
	scalar Flagged2012   = `meanF12'
	scalar Flagged2013	 = `meanF13'
	scalar Unflagged2012 = `meanN12'
	scalar Unflagged2013 = `meanN13'
	
	post tab5 ("`row`ixRow''") `postTab5'
	local ixRow = `ixRow'+1

	* post se of means (even-num. rows)
	scalar Flagged2012   = `sdF12'
	scalar Flagged2013	 = `sdF13'
	scalar Unflagged2012 = `sdN12'
	scalar Unflagged2013 = `sdN13'
	foreach sname in p_2012 p_2013 {
		scalar `sname' = .
		}
	post tab5 ("`row`ixRow''") `postTab5'
	local ixRow = `ixRow'+1
	
	}

* the last row: sample sizes
qui su npi if impFlag1_2012==1
scalar Flagged2012 = r(N)

qui su npi if impFlag1_2013==1
scalar Flagged2013 = r(N)

qui su npi if impFlag1_2012==0
scalar Unflagged2012 = r(N)

qui su npi if impFlag1_2013==0
scalar Unflagged2013 = r(N)

foreach sname in p_2012 p_2013 {
	scalar `sname' = .
	}

post tab5 ("N") `postTab5'
	
* close post file
postclose tab5


