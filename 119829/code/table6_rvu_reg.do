************************************
**
** Table 6: level of complexity and upcoding
**
************************************
local ixFlag = substr("$flag_2012",1,8)

use "$datadir/phy_serv_time.dta", clear

keep npi hcpcs phyType phyZip male GPCIregion nServ nBene nServUniq meanPaid nServ2013 nBene2013 nServUniq2013 meanPaid2013

* prep to merge with hrr data
tostring phyZip, replace
gen str5 zip5 = substr(phyZip,1,5) if length(phyZip)==9
replace zip5 = "0"+substr(phyZip,1,4) if length(phyZip)==8
replace zip5 = "00"+substr(phyZip,1,3) if length(phyZip)==7
replace zip5 = phyZip if length(phyZip)==5
replace zip5 = "0"+phyZip if length(phyZip)==4
replace zip5 = "00"+phyZip if length(phyZip)==3

* link to hrr
merge m:1 zip5 using "$datadir/zip_hrr.dta", keep(master matched)

* fill in phyType info
sort npi phyType
replace phyType = phyType[_n-1] if npi==npi[_n-1]

* get phy-hcpcs-place-level total payments
gen totPaid = meanPaid*nServ
gen totPaid2013 = meanPaid2013*nServ2013

* get phy-hcpcs-level info
collapse (sum) nServ2012=nServ nServ2013 ///
	nBene2012=nBene nBene2013 ///
	nServUniq2012=nServUniq nServUniq2013 ///
	totPaid2012=totPaid totPaid2013, by(npi phyType hrr male GPCIregion hcpcs)

* re-generate phy-hcpcs-level average payments
gen meanPaid2012 = totPaid2012/nServ2012
gen meanPaid2013 = totPaid2013/nServ2013

* merge with npi flags
merge m:1 npi using "$datadir/phy_time.dta", keepusing($flag_2012 $flag_2013 totHrsPerWk_2012 totHrsPerWk_2013)
drop _merge

* merge with hcpcs FlagPropensities and specialty FlagPropensities
merge m:1 hcpcs using "$datadir/CodeFI.dta", keepusing(fpCode_`ixFlag'_2012 fpCode_`ixFlag'_2013)
keep if _m==3
drop _merge

merge m:1 phyType using "$datadir/SpecFI.dta", keepusing(fpSpec_`ixFlag'_2012 fpSpec_`ixFlag'_2013)
keep if _m==3
drop _merge

* FOCUS ON THE SUBSAMPLE WITH 20+ HOURS/WEEK
keep if totHrsPerWk_2012>20 | totHrsPerWk_2013>20

* merge with hcpcs description
merge m:1 hcpcs using "$datadir/servTimeList.dta", keep(match) ///
	keepusing(hcpcsCateg hcpcsDesc codeGroup EMgroup codeComplx nComplx wRVU2012 wRVU2013)
drop _merge

* reshape to long
reshape long nServ nBene nServUniq totPaid meanPaid ///
	`ixFlag'_ totHrsPerWk_ fpCode_`ixFlag'_ fpSpec_`ixFlag'_ wRVU, i(npi hcpcs phyType hrr male GPCIregion) j(year)
drop if nServ == 0

foreach v in `ixFlag' totHrsPerWk fpCode_`ixFlag' fpSpec_`ixFlag' {
	ren `v'_ `v'
	}


* only use codes in a cluster (i.e. those with non-missing nComplx)
drop if nComplx == .

* standardized wRVU (in multiples of lowest-level code)
egen wRVUbase = max(wRVU*(codeComplx==1)), by(EMgroup)
gen wRVUnorm  = wRVU/wRVUbase
drop wRVUbase

* low-, mid-, and high-intensity dummies
gen byte inten3 = .
	replace inten3 = 1 if (codeComplx==1 & nComplx==3) | (codeComplx<=2 & nComplx==5)
	replace inten3 = 2 if (codeComplx==2 & nComplx==3) | (codeComplx==3 & nComplx==5)
	replace inten3 = 3 if (codeComplx==3 & nComplx==3) | (codeComplx>=4 & nComplx==5)

* dummy: wRVU increments above or below average
egen meanwRVUnorm = mean(wRVUnorm), by(codeComplx nComplx)
tabstat meanwRVUnorm if nComplx==3, by(codeComplx)
tabstat meanwRVUnorm if nComplx==4, by(codeComplx)
tabstat meanwRVUnorm if nComplx==5, by(codeComplx)

gen isWRVU_lt_avg = wRVUnorm > meanwRVUnorm

* fixed effects to include in reg
egen int abgroup = group(hrr EMgroup year)
	
* generate dummies and labels (to automate reg output)
lab var `ixFlag' "Flagged"
lab var male "Male"

forvalues j = 2/5 {
	gen codeCompl_`j' = codeComplx==`j'
	lab var codeCompl_`j' "Complexity=`j'"
	gen impXcod_`j'   = `ixFlag'==1 & codeComplx==`j'
	lab var impXcod_`j' "Flagged $\times$ (Complexity=`j')"
	}

gen mi_inten = inten3==2
gen hi_inten = inten3==3
gen impXmi   = `ixFlag'==1 & inten3==2
gen impXhi   = `ixFlag'==1 & inten3==3
lab var mi_inten "Mid-intensity"
lab var hi_inten "High-intensity"
lab var impXmi "Flagged $\times$ Mid-intensity"	
lab var impXhi "Flagged $\times$ High-intensity"	
	
* regression	
local controlX "male"
local regOption "absorb(abgroup) vce(cluster npi)"

forvalues i = 3/5 {
	qui xi: areg nServ `ixFlag' codeCompl_* impXcod_* `controlX' if nComplx==`i', `regOption'
	est store lev_`i'_a
}

qui xi: areg nServ `ixFlag' mi_inten hi_inten impXmi impXhi `controlX', `regOption'
est store lev_all_a
qui xi: areg nServ `ixFlag' mi_inten hi_inten impXmi impXhi `controlX' if isWRVU_lt_avg==0, `regOption'
est store lev_all_b
qui xi: areg nServ `ixFlag' mi_inten hi_inten impXmi impXhi `controlX' if isWRVU_lt_avg==1, `regOption'
est store lev_all_c


local tabOption "se obslast brackets star(* 0.10 ** 0.05 *** 0.01) nogaps ar2 nolines compress depvars label"
esttab lev* using "$tabdir/table10_reg_`ixFlag'.tex", replace `tabOption'

