************************************
**
**  table 1 supplement:
**  count # codes without a time (but actually do take time)
************************************

set more off

************************************
*	get phy-hcpcs-level time info
************************************
* load physician-service-place level data
use "$savedir/phy_serv_merged.dta", clear

* merge with hcpcs-level time-needed info
merge m:1 hcpcs using "$savedir/servTimeListold.dta"

* count codes without a time measure
drop if _m==2
keep npi hcpcs hcpcsCateg nServ* meanPaid* wRVU* tTotal
gen totPaid = nServ*meanPaid
gen totPaid2013 = nServ2013*meanPaid2013
collapse (sum) nServ totPaid nServ2013 totPaid2013 ///
		(mean) wRVU2012 wRVU2013 tTotal, by(npi hcpcs)

gen byte isTimed2012 = (nServ~=0)*(tTotal~=0 & tTotal~=.)
gen byte unTimed2012 = (nServ~=0)*(tTotal==0 | tTotal==.)
gen byte isTimed2013 = (nServ2013~=0)*(tTotal~=0 & tTotal~=.)
gen byte unTimed2013 = (nServ2013~=0)*(tTotal==0 | tTotal==.)

egen nTimedCodes2012   = total(isTimed2012), by(npi)
egen wTimedCodes2012   = total(isTimed2012*wRVU2012*nServ), by(npi)
egen vTimedCodes2012   = total(isTimed2012*nServ), by(npi)
egen rTimedCodes2012   = total(isTimed2012*totPaid), by(npi)

egen nUntimedCodes2012 = total(unTimed2012), by(npi)
egen wUntimedCodes2012 = total(unTimed2012*wRVU2012*nServ), by(npi)
egen vUntimedCodes2012 = total(unTimed2012*nServ), by(npi)
egen rUntimedCodes2012 = total(unTimed2012*totPaid), by(npi)

egen nTimedCodes2013   = total(isTimed2013), by(npi)
egen wTimedCodes2013   = total(isTimed2013*wRVU2013*nServ2013), by(npi)
egen vTimedCodes2013   = total(isTimed2013*nServ2013), by(npi)
egen rTimedCodes2013   = total(isTimed2013*totPaid2013), by(npi)

egen nUntimedCodes2013 = total(unTimed2013), by(npi)
egen wUntimedCodes2013 = total(unTimed2013*wRVU2013*nServ2013), by(npi)
egen vUntimedCodes2013 = total(unTimed2013*nServ2013), by(npi)
egen rUntimedCodes2013 = total(unTimed2013*totPaid2013), by(npi)


* clean up and generate summary stats at NPI level
drop hcpcs nServ totPaid nServ2013 totPaid2013 wRVU2012 wRVU2013 tTotal isTimed2012 unTimed2012 isTimed2013 unTimed2013
duplicates drop npi, force

forval y = 2012/2013 {
	gen sh_nUntimed`y' = nUntimedCodes`y'/(nTimedCodes`y'+nUntimedCodes`y')
	gen sh_wUntimed`y' = wUntimedCodes`y'/(wTimedCodes`y'+wUntimedCodes`y')
	gen sh_vUntimed`y' = vUntimedCodes`y'/(vTimedCodes`y'+vUntimedCodes`y')
	gen sh_rUntimed`y' = rUntimedCodes`y'/(rTimedCodes`y'+rUntimedCodes`y')
}

keep npi sh*

* save (to be merged with $datadir/phy_time.dta"
save "$savedir/phy_untimed_sum.dta", replace

