

************************************
*	Tables
************************************

*	Table 2 in Manuscript: Number and fraction of physicians flagged
clear
set more off
include "config.do"
do "$rootdir/code/table_a12_supp_notime.do"  // change version of service time list in this program
do "$rootdir/code/table_a12_count_flag_fixed.do" // change version of input data in this program,
*do "$rootdir/code/table1_count_flag_int.do" // do this one instead for intra service time

use "$savedir/table1.dta", clear
qui compress
replace r = "Weekly hours" in 1
replace r = "Year" in 2
foreach v of varlist var1-var8 {
	qui replace `v' = `v'*100  in 4/13  // unit: %
	qui replace `v' = (int(`v'*1000)+( mod(int(`v'*10000),10)>=5) )/1000 in 4/13 // digit: 3-digits after .
	tostring `v', replace
	replace `v' = "" if `v'=="."
	replace `v' = "0"+`v' if strpos(`v',".")==1 in 4/13
	replace `v' = `v'+"00" if length(`v')-strpos(`v',".")==1 in 4/13
	replace `v' = `v'+"0" if length(`v')-strpos(`v',".")==2 in 4/13
	}
	
* ... insert separator columns
gen blank1 = ""
gen blank2 = ""
gen blank3 = ""
order r var1 var2 blank1 var3 var4 blank2 var5 var6 blank3 	
*dataout, save("$tabdir/table2_nflagged.tex") tex replace
save "$savedir/table_a12.dta",replace

export excel "$savedir/table_a12.xlsx", replace

