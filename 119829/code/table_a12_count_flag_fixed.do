************************************
**
** Table 1: Number and fraction of physicians flagged
**
************************************

use "$savedir/phy_time_2.dta", clear

* augment with physician-level timed vs untimed codes/workload info
merge 1:1 npi using "$savedir/phy_untimed_sum.dta", keep(master matched)

* set up postfile
local postTab1f
forval i=1/8 {
	local postTab1f "`postTab1f' var`i'"
	}
di "`postTab1f'"

cap postclose tab1
postfile tab1 str60 r double(`postTab1f') using "$savedir/table1.dta", replace

* set up column titles
post tab1 ("weekly hours above") (80) (80) (100) (100) (112) (112) (168) (168)
post tab1 ("") (2012) (2013) (2012) (2013) (2012) (2013) (2012) (2013)

* set up row titles
local row1 "Number of physicians flagged"
local row2 "% in physicians working 20+ hr/week"
local row3 "% in all physicians"
local row4 "% zero-time codes (flagged)"
local row5 "% zero-time codes (unflagged, 20+ hr/week)"
local row7 "% wRVU of zero-time codes (flagged)"
local row8 "% wRVU of zero-time codes (unflagged, 20+ hr/week)"
local row10 "% volume of zero-time codes (flagged)"
local row11 "% volume of zero-time codes (unflagged, working 20+ hr/week)"
local row13 "% revenue from zero-time codes (flagged physicians)"
local row14 "% revenue from zero-time codes (unflagged, working 20+ hr/week)"
local row16 "Total number of physicians working 20+ hr/week"
local row17 "Total number of physicians"

* fill in rows
local postTab1
foreach pval in `postTab1f' {
    local postTab1 "`postTab1' (`pval')"
    }

* ... row 1
local ixVar = 1
foreach v of varlist impFlag0_2012 impFlag0_2013 impFlag1_2012 impFlag1_2013 impFlag2_2012 impFlag2_2013 impFlag3_2012 impFlag3_2013 {
	qui su `v'
	scalar var`ixVar' = r(sum)
	local ixVar = `ixVar'+1
	}

post tab1 ("`row1'") `postTab1'

* ... row 2
qui su npi if totHrsPerWk_2012 > 20
local nOver20_2012 = r(N)
di "`nOver20_2012'"

qui su npi if totHrsPerWk_2013 > 20
local nOver20_2013 = r(N)
di "`nOver20_2013'"

local ixVar = 1
foreach v of varlist impFlag0_2012 impFlag1_2012 impFlag2_2012 impFlag3_2012 {
	qui su `v'
	local nFlagged = r(sum)	
	scalar var`ixVar' = `nFlagged'/`nOver20_2012'
	local ixVar = `ixVar'+2
	}
	
local ixVar = 2
foreach v of varlist impFlag0_2013 impFlag1_2013 impFlag2_2013 impFlag3_2013 {
	qui su `v'
	local nFlagged = r(sum)	
	scalar var`ixVar' = `nFlagged'/`nOver20_2013'
	local ixVar = `ixVar'+2
	}	

post tab1 ("`row2'") `postTab1'

* ... row 3
local ixVar = 1
foreach v of varlist impFlag0_2012 impFlag0_2013 impFlag1_2012 impFlag1_2013 impFlag2_2012 impFlag2_2013 impFlag3_2012 impFlag3_2013 {
	qui su `v'
	scalar var`ixVar' = r(mean)
	local ixVar = `ixVar'+1
	}

post tab1 ("`row3'") `postTab1'

* ... row 4 "% untimed codes (flagged)"
local ixVar = 1
foreach v of varlist impFlag0_2012 impFlag0_2013 impFlag1_2012 impFlag1_2013 impFlag2_2012 impFlag2_2013 impFlag3_2012 impFlag3_2013 {
	if mod(`ixVar',2)==1 {
		qui su sh_nUntimed2012 if `v'==1
	} 
	else {
		qui su sh_nUntimed2013 if `v'==1
	}
	scalar var`ixVar' = r(mean)
	local ixVar = `ixVar'+1
	}
post tab1 ("`row4'") `postTab1'

* ... row 5 "% untimed codes (unflagged, 20+)"
local ixVar = 1
foreach v of varlist impFlag0_2012 impFlag0_2013 impFlag1_2012 impFlag1_2013 impFlag2_2012 impFlag2_2013 impFlag3_2012 impFlag3_2013 {
	if mod(`ixVar',2)==1 {
		qui su sh_nUntimed2012 if `v'==0 & (totHrsPerWk_2012>20 | totHrsPerWk_2013>20)
	} 
	else {
		qui su sh_nUntimed2013 if `v'==0 & (totHrsPerWk_2012>20 | totHrsPerWk_2013>20)
	}
	scalar var`ixVar' = r(mean)
	local ixVar = `ixVar'+1
	}
post tab1 ("`row5'") `postTab1'

* ... row 7 "% wRVU of untimed codes (flagged)"
local ixVar = 1
foreach v of varlist impFlag0_2012 impFlag0_2013 impFlag1_2012 impFlag1_2013 impFlag2_2012 impFlag2_2013 impFlag3_2012 impFlag3_2013 {
	if mod(`ixVar',2)==1 {
		qui su sh_wUntimed2012 if `v'==1
	} 
	else {
		qui su sh_wUntimed2013 if `v'==1
	}
	scalar var`ixVar' = r(mean)
	local ixVar = `ixVar'+1
	}
post tab1 ("`row7'") `postTab1'

* ... row 8 "% wRVU of untimed codes (unflagged, 20+)"
local ixVar = 1
foreach v of varlist impFlag0_2012 impFlag0_2013 impFlag1_2012 impFlag1_2013 impFlag2_2012 impFlag2_2013 impFlag3_2012 impFlag3_2013 {
	if mod(`ixVar',2)==1 {
		qui su sh_wUntimed2012 if `v'==0 & (totHrsPerWk_2012>20 | totHrsPerWk_2013>20)
	} 
	else {
		qui su sh_wUntimed2013 if `v'==0 & (totHrsPerWk_2012>20 | totHrsPerWk_2013>20)
	}
	scalar var`ixVar' = r(mean)
	local ixVar = `ixVar'+1
	}
post tab1 ("`row8'") `postTab1'


* ... row 10 "% volume of untimed codes (flagged)"
local ixVar = 1
foreach v of varlist impFlag0_2012 impFlag0_2013 impFlag1_2012 impFlag1_2013 impFlag2_2012 impFlag2_2013 impFlag3_2012 impFlag3_2013 {
	if mod(`ixVar',2)==1 {
		qui su sh_vUntimed2012 if `v'==1
	} 
	else {
		qui su sh_vUntimed2013 if `v'==1
	}
	scalar var`ixVar' = r(mean)
	local ixVar = `ixVar'+1
	}
post tab1 ("`row10'") `postTab1'

* ... row 11 "% volume of untimed codes (unflagged, 20+)"
local ixVar = 1
foreach v of varlist impFlag0_2012 impFlag0_2013 impFlag1_2012 impFlag1_2013 impFlag2_2012 impFlag2_2013 impFlag3_2012 impFlag3_2013 {
	if mod(`ixVar',2)==1 {
		qui su sh_vUntimed2012 if `v'==0 & (totHrsPerWk_2012>20 | totHrsPerWk_2013>20)
	} 
	else {
		qui su sh_vUntimed2013 if `v'==0 & (totHrsPerWk_2012>20 | totHrsPerWk_2013>20)
	}
	scalar var`ixVar' = r(mean)
	local ixVar = `ixVar'+1
	}
post tab1 ("`row11'") `postTab1'


* ... row 13 "% revenue from untimed codes (flagged)"
local ixVar = 1
foreach v of varlist impFlag0_2012 impFlag0_2013 impFlag1_2012 impFlag1_2013 impFlag2_2012 impFlag2_2013 impFlag3_2012 impFlag3_2013 {
	if mod(`ixVar',2)==1 {
		qui su sh_rUntimed2012 if `v'==1
	} 
	else {
		qui su sh_rUntimed2013 if `v'==1
	}
	scalar var`ixVar' = r(mean)
	local ixVar = `ixVar'+1
	}
post tab1 ("`row13'") `postTab1'

* ... row 14 "% revenue from untimed codes (unflagged, 20+)"
local ixVar = 1
foreach v of varlist impFlag0_2012 impFlag0_2013 impFlag1_2012 impFlag1_2013 impFlag2_2012 impFlag2_2013 impFlag3_2012 impFlag3_2013 {
	if mod(`ixVar',2)==1 {
		qui su sh_rUntimed2012 if `v'==0 & (totHrsPerWk_2012>20 | totHrsPerWk_2013>20)
	} 
	else {
		qui su sh_rUntimed2013 if `v'==0 & (totHrsPerWk_2012>20 | totHrsPerWk_2013>20)
	}
	scalar var`ixVar' = r(mean)
	local ixVar = `ixVar'+1
	}
post tab1 ("`row14'") `postTab1'


* ... row 16
preserve

keep if totHrsPerWk_2012>20 | totHrsPerWk_2013>20
keep npi
duplicates drop npi, force
qui su npi
scalar var1 = r(N)
forval ixVar = 2/8 {
	scalar var`ixVar' = .
	}
	
restore
post tab1 ("`row16'") `postTab1'

* ... row 17
preserve

keep npi
duplicates drop npi, force
qui su npi
scalar var1 = r(N)
forval ixVar = 2/8 {
	scalar var`ixVar' = .
	}
	
restore
post tab1 ("`row17'") `postTab1'

* close post file
postclose tab1
