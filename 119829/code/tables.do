

************************************
*	Tables
************************************
include "config.do"
*	Table 2 in Manuscript: Number and fraction of physicians flagged
clear
set more off

program computetable2 
do "$rootdir/code/sub_table1_supp_notime.do"  // change version of service time list in this program
do "$rootdir/code/sub_table1_count_flag_fixed.do" // change version of input data in this program,
*do "$rootdir/code/table1_count_flag_int.do" // do this one instead for intra service time

use "$tempdir/table1.dta", clear
qui compress
replace r = "Weekly hours" in 1
replace r = "Year" in 2
foreach v of varlist var1-var8 {
	qui replace `v' = `v'*100  in 4/13  // unit: %
	qui replace `v' = (int(`v'*1000)+( mod(int(`v'*10000),10)>=5) )/1000 in 4/13 // digit: 3-digits after .
	tostring `v', replace
	replace `v' = "" if `v'=="."
	replace `v' = "0"+`v' if strpos(`v',".")==1 in 4/13
	replace `v' = `v'+"00" if length(`v')-strpos(`v',".")==1 in 4/13
	replace `v' = `v'+"0" if length(`v')-strpos(`v',".")==2 in 4/13
	}
	
* ... insert separator columns
gen blank1 = ""
gen blank2 = ""
gen blank3 = ""
order r var1 var2 blank1 var3 var4 blank2 var5 var6 blank3 	
*dataout, save("$tabdir/table2_nflagged.tex") tex replace
save "$savedir/table_1_$suffix.dta",replace

end // end of program definition


/* To create corrected versions of Table 2 in Fang and Gong (2017) using the different sets
of flagged physicians (depending on the service time estimates used), run tables.do

- In the file table1_supp_notime.do, change the input file for the service times:
servTimeListold.dta is the service times using the original code and Zuckerman
(2014), servTimeList1.dta is the service times using the corrected code and
Zuckerman (2014), servTimeList2.dta is the service times using corrected code
and Zuckerman (2016) procedure time data.

- In the file table1_count_flag_fixed.do, change the input file: phy_time_old.dta is
the flagged physicians using the original code and service times, phy_time_1.dta
is the flagged physicians using the corrected code and original service times and
phy_time_2.dta is the flagged physicians using the new service times.
*/
global suffix old
computetable2

global suffix 1
computetable2

global suffix 2
computetable2




