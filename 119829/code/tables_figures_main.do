************************************
**
** tables_figures_main.do
**
************************************

************************************
*       housekeeping
************************************
* need -dataout- package
ssc install dataout

version 11.2
include "config.do"
clear all

local home : env HOME
if "`home'"=="" {
	local home : env HOMEPATH
}
// global rootdir "`home'/Documents/Workplace/Medicare_payment"
global datadir "$rootdir/output"
global tempdir "/tmp"
global savedir "$rootdir/output"
global tabdir "$rootdir/tex"
 
cap log close
log using "$savedir/tables_figures_main.txt", text replace

set more off

************************************
*	Tables
************************************

*	Table 2 in Manuscript: Number and fraction of physicians flagged
do "$rootdir/code/table1_supp_notime.do"
do "$rootdir/code/table1_count_flag.do"

use "$savedir/table1.dta", clear
qui compress
replace r = "Weekly hours" in 1
replace r = "Year" in 2
foreach v of varlist var1-var8 {
	qui replace `v' = `v'*100  in 4/13  // unit: %
	qui replace `v' = (int(`v'*1000)+( mod(int(`v'*10000),10)>=5) )/1000 in 4/13 // digit: 3-digits after .
	tostring `v', replace
	replace `v' = "" if `v'=="."
	replace `v' = "0"+`v' if strpos(`v',".")==1 in 4/13
	replace `v' = `v'+"00" if length(`v')-strpos(`v',".")==1 in 4/13
	replace `v' = `v'+"0" if length(`v')-strpos(`v',".")==2 in 4/13
	}
	
* ... insert separator columns
gen blank1 = ""
gen blank2 = ""
gen blank3 = ""
order r var1 var2 blank1 var3 var4 blank2 var5 var6 blank3 	
dataout, save("$tabdir/table2_nflagged.tex") tex replace


*	Table 3 in Manuscript: Overlapping flags across years
do "$rootdir/code/table2_overlapping_flags.do"

use "$savedir/table2.dta", clear
compress
replace r = "Weekly hours" in 1
foreach v of varlist var2 var4 var6 var8 {
	replace `v' = `v'*100  in 2/4  // unit: %
	replace `v' = (int(`v'*100)+( mod(int(`v'*1000),10)>=5) )/100 in 2/4 // digit: 2-digits after .
	tostring `v', replace
	replace `v' = "" if `v'=="."
	}

tostring var1 var3 var5 var7, replace

* ... insert separator columns
gen blank1 = ""
gen blank2 = ""
gen blank3 = ""
order r var1 var2 blank1 var3 var4 blank2 var5 var6 blank3 var7 var8	
dataout, save("$tabdir/table3_overlap_flag.tex") tex replace

*	Table A4 in Online Appendix: physician characteristics (flagged vs other)
do "$rootdir/code/table3_phy_charac.do"

use "$savedir/table3.dta", clear
qui compress
foreach v of varlist All - F2013only {
	replace `v' = (int(`v'*1000)+( mod(int(`v'*10000),10)>=5) )/1000
	tostring `v', replace
	replace `v' = "0"+`v' if strpos(`v',".")==1
	replace `v' = `v'+"00" if length(`v')-strpos(`v',".")==1
	replace `v' = `v'+"0" if length(`v')-strpos(`v',".")==2
	replace `v' = "("+`v'+")" if mod(_n,2)==0
	}	
	
dataout, save("$tabdir/table4_phy_charac.tex") tex replace


* 	Table 4 in Manuscript: physician characteristics cond'l on same HRR
do "$rootdir/code/tab3.5_cond_HRR.do"

use "$savedir/table3_5.dta", clear
qui compress

* ...add the mean for "Never" as reference
preserve
use "$savedir/table3.dta", clear
keep Never
gen row=_n
save "$tempdir/table3.dta", replace
restore

gen row=_n
merge 1:1 row using "$tempdir/table3.dta"
drop row _m

qui foreach v of varlist Ever - Never {
	replace `v' = (int(`v'*1000)+( mod(int(`v'*10000),10)>=5) )/1000
	gen str3 star_`v' = ""
	if "`v'"!="Never" {
		replace star_`v' = "*"   if mod(_n,2)==1 & abs(`v'/`v'[_n+1])>=1.64
		replace star_`v' = "**"  if mod(_n,2)==1 & abs(`v'/`v'[_n+1])>=1.96
		replace star_`v' = "***" if mod(_n,2)==1 & abs(`v'/`v'[_n+1])>=2.576
		replace star_`v' = "" if _n==_N
		}
		
	tostring `v', replace
	replace `v' = `v'+"00" if length(`v')-strpos(`v',".")==1
	replace `v' = `v'+"0" if length(`v')-strpos(`v',".")==2
	replace `v' = "0"+`v' if strpos(`v',".")==1
	replace `v' = "-0"+substr(`v',2,.) if strpos(`v',"-.")==1
	replace `v' = "0.000" if `v'=="000"
	
	replace `v' = "["+`v'+"]" if mod(_n,2)==0
	replace `v' = `v'+star_`v'
	}
	
replace Never="" if mod(_n,2)==0
drop star_*
dataout, save("$tabdir/table5_phy_charac_HRR.tex") tex dec(3) replace


*	Table 5 in Manuscript: physician specialty (flagged vs other)
global flag_2012 impFlag1_2012
global flag_2013 impFlag1_2013
do "$rootdir/code/table4_specialty.do"

use "$savedir/table4_impFlag1_2012.dta", clear
drop phyType
order Specialty

replace nPhyShareAll = nPhyShareAll*100
replace fpSpec_impFlag1_2012 = fpSpec_impFlag1_2012*100
replace fpSpec_impFlag1_2013 = fpSpec_impFlag1_2013*100

qui compress
foreach v of varlist nPhyShareAll fpSpec_impFlag1_2012 fpSpec_impFlag1_2013 {
	qui replace `v' = (int(`v'*1000)+( mod(int(`v'*10000),10)>=5) )/1000 in 1/7 // digit: 3-digits after .
	}

tostring nPhyShareAll - fpSpec_impFlag1_2013 , replace force
	
foreach v of varlist nPhyShareAll fpSpec_impFlag1_2012 fpSpec_impFlag1_2013 {
	tostring `v' , replace force
	replace `v' = substr(`v',1,strpos(`v',".")+3) if strpos(`v',".")>0 & length(`v') - strpos(`v',".") >3
	replace `v' = "" if `v'=="."
	replace `v' = "0"+`v' if strpos(`v',".")==1 in 1/7
	replace `v' = `v'+"00" if length(`v')-strpos(`v',".")==1 in 1/7
	replace `v' = `v'+"0" if length(`v')-strpos(`v',".")==2 in 1/7
	}
	
* ... insert separator columns
gen blank1 = ""
gen blank2 = ""
gen blank3 = ""
order Specialty nPhyShareAll blank1 Num_unflagged_2012 Num_unflagged_2013 blank2 Num_flagged_2012 Num_flagged_2013 blank3 fpSpec_impFlag1_2012 fpSpec_impFlag1_2013
	
dataout, save("$tabdir/table6_specialty.tex") tex replace

*	Table A5 in Online Appendix: decomposing the hours (quantity vs intensity)
do "$rootdir/code/table5_quantity_intensity.do"

use "$savedir/table5.dta", clear
qui compress
drop p2012 p2013 // all VERY significant, drop p-values
qui foreach v of varlist Flagged2012 Flagged2013 Unflagged2012 Unflagged2013 {
	replace `v' = (int(`v'*1000)+( mod(int(`v'*10000),10)>=5) )/1000
	tostring `v', replace
	replace `v' = `v'+"00" if length(`v')-strpos(`v',".")==1
	replace `v' = `v'+"0" if length(`v')-strpos(`v',".")==2
	replace `v' = "0"+`v' if strpos(`v',".")==1
	replace `v' = "-0"+substr(`v',2,.) if strpos(`v',"-.")==1
	replace `v' = "0.000" if `v'=="000"
	
	replace `v' = "("+`v'+")" if mod(_n,2)==0
	}

dataout, save("$tabdir/table5.tex") tex dec(3) replace

* 	Table 6 in Manuscript: decomposing the hours - cond'l on same HRR
global flag_2012 impFlag1_2012
global flag_2013 impFlag1_2013
do "$rootdir/code/table5.5_decomp_HRR.do"

use "$savedir/table8_decomp_HRR_impFlag1.dta", clear
ren meanUF2012 Unflagged2012
ren meanUF2013 Unflagged2013
qui compress

qui foreach v of varlist Flagged2012 - Unflagged2013{
	replace `v' = (int(`v'*1000)+( mod(int(`v'*10000),10)>=5) )/1000
	gen str3 star_`v' = ""
	if "`v'"!="Unflagged2012" & "`v'"!="Unflagged2013" {
		replace star_`v' = "*"   if mod(_n,2)==1 & abs(`v'/`v'[_n+1])>=1.64
		replace star_`v' = "**"  if mod(_n,2)==1 & abs(`v'/`v'[_n+1])>=1.96
		replace star_`v' = "***" if mod(_n,2)==1 & abs(`v'/`v'[_n+1])>=2.576
		replace star_`v' = "" if _n==_N
		}
		
	tostring `v', replace
	replace `v' = `v'+"00" if length(`v')-strpos(`v',".")==1
	replace `v' = `v'+"0" if length(`v')-strpos(`v',".")==2
	replace `v' = "0"+`v' if strpos(`v',".")==1
	replace `v' = "-0"+substr(`v',2,.) if strpos(`v',"-.")==1
	replace `v' = "0.000" if `v'=="000"
	
	replace `v' = "["+`v'+"]" if mod(_n,2)==0
	replace `v' = `v'+star_`v'
	}
	
replace Unflagged2012="" if mod(_n,2)==0
replace Unflagged2013="" if mod(_n,2)==0

drop star_*
dataout, save("$tabdir/table8_decomp_HRR_impFlag1.tex") tex replace

* 	Table 7 in Manuscript: upcoding factors
global flag_2012 impFlag1_2012
global flag_2013 impFlag1_2013
do "$rootdir/code/table5.8_upcoding_factor.do"

use "$savedir/table9_OPF_impFlag1.dta", clear
qui compress

qui foreach v of varlist Flagged_2012 Flagged_2013 Not_flagged_2012 Not_flagged_2013 {
	replace `v' = (int(`v'*1000)+( mod(int(`v'*10000),10)>=5) )/1000
	tostring `v', replace
	replace `v' = `v'+"00" if length(`v')-strpos(`v',".")==1
	replace `v' = `v'+"0" if length(`v')-strpos(`v',".")==2
	replace `v' = "0"+`v' if strpos(`v',".")==1
	replace `v' = "-0"+substr(`v',2,.) if strpos(`v',"-.")==1
	replace `v' = "0.000" if `v'=="000"
	replace `v' = "("+`v'+")" if mod(_n,2)==0
	}

dataout, save("$tabdir/table9_OPF_impFlag1.tex") tex dec(3) replace

*	Table 8 in Manuscript: level of complexity add upcoding
global flag_2012 impFlag1_2012
global flag_2013 impFlag1_2013
do "$rootdir/code/table6_rvu_reg.do"


************************************
*	Figures
************************************

* Figure 1 in Manuscript: distribution of hours-per-week
do "$rootdir/code/fig1_time_dist.do"

* Figure 2 in Manuscript: Code FI vs Pr(flagged)
global flag_2012 impFlag1_2012
global flag_2013 impFlag1_2013
do "$rootdir/code/fig_pr_FI.do"

* Figure 3 in Manuscript: distribution of hcpcs Flag Propensity
global flag_2012 impFlag1_2012
global flag_2013 impFlag1_2013
do "$rootdir/code/fig2_hcpcs_fp.do"
		
* Figure 4 in Manuscript: distribution of hcpcs Flag Propensity by flag status
global flag_2012 impFlag1_2012
global flag_2013 impFlag1_2013
do "$rootdir/code/fig3_hcpcs_fp_by_flag.do"

* Figure 5 in Manuscript: distribution of upcoding factors
global flag_2012 impFlag1_2012
global flag_2013 impFlag1_2013
do "$rootdir/code/fig5_upcoding_factor.do"

* Figure 6 in Manuscript: comparing our study with CERT
global flag_2012 impFlag1_2012
global flag_2013 impFlag1_2013
do "$rootdir/code/fig4_hcpcs_fp_CERT.do"

************************************
*	Housekeepting
************************************
log close
