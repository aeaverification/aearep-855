************************************
**
** Supplemental results / robustness checks
**
************************************

************************************
*       housekeeping
************************************
* need -dataout- package
ssc install dataout

version 11.2
include "config.do"
clear all

local home : env HOME
if "`home'"=="" {
	local home : env HOMEPATH
}
// global rootdir "`home'/Documents/Workplace/Medicare_payment"
global datadir "$rootdir/output"
global tempdir "/tmp"
global savedir "$rootdir/output"
global tabdir "$rootdir/tex"
 
cap log close
log using "$savedir/tables_figures_supp1.txt", text replace

set more off

************************************
*	Table A3: Possible residents among flagged physicians
************************************
do "$rootdir/code/table1_supp_residents.do"

use "$savedir/table1S_residency.dta", clear
qui compress
replace r = "Weekly hours" in 1
replace r = "Year" in 2
foreach v of varlist var1-var8 {
	qui replace `v' = (int(`v'*1000)+( mod(int(`v'*10000),10)>=5) )/1000 in 5 // digit: 3-digits after .
	tostring `v', replace
	replace `v' = "" if `v'=="."
	replace `v' = "0"+`v' if strpos(`v',".")==1 in 5
	replace `v' = `v'+"00" if length(`v')-strpos(`v',".")==1 in 5
	replace `v' = `v'+"0" if length(`v')-strpos(`v',".")==2 in 5
	}
	
* ... insert separator columns
gen blank1 = ""
gen blank2 = ""
gen blank3 = ""
order r var1 var2 blank1 var3 var4 blank2 var5 var6 blank3 	
dataout, save("$tabdir/table2S_residency.tex") tex replace


************************************
*	Tables A4-A5: comparisons of physician
*	characteristics & volume decomposition
*	UNCONDITIONAL on HRR
*	--> see tables_figures_main.do
************************************


************************************
*	Main results under alternative flagging thresholds
************************************

* Tables A6-A7: physician characteristic comparison cond'l on HRR - alternative flagging criteria
global flag_2012 impFlag2_2012
global flag_2013 impFlag2_2013
do "$rootdir/code/table3_supp_phy_charac.do"

global flag_2012 impFlag3_2012
global flag_2013 impFlag3_2013
do "$rootdir/code/table3_supp_phy_charac.do"

foreach ixFlag in impFlag2 impFlag3 {
	use "$savedir/table3_`ixFlag'_2012.dta", clear
	local last = _N
	qui foreach v of varlist Ever - meanNever {
		replace `v' = (int(`v'*1000)+( mod(int(`v'*10000),10)>=5) )/1000
		gen str3 star_`v' = ""
		if "`v'"!="meanNever" {
			replace star_`v' = "*"   if mod(_n,2)==1 & abs(`v'/`v'[_n+1])>=1.64
			replace star_`v' = "**"  if mod(_n,2)==1 & abs(`v'/`v'[_n+1])>=1.96
			replace star_`v' = "***" if mod(_n,2)==1 & abs(`v'/`v'[_n+1])>=2.576
			replace star_`v' = "" if _n==_N
			}
			
		tostring `v', replace
		replace `v' = `v'+"00" if length(`v')-strpos(`v',".")==1
		replace `v' = `v'+"0" if length(`v')-strpos(`v',".")==2
		replace `v' = "0"+`v' if strpos(`v',".")==1
		replace `v' = "-0"+substr(`v',2,.) if strpos(`v',"-.")==1
		replace `v' = "0.000" if `v'=="000"
		
		replace `v' = "["+`v'+"]" if mod(_n,2)==0
		replace `v' = `v'+star_`v'
		replace `v' = subinstr(`v',".000","",.) in `last'
		}
		
	replace meanNever="" if mod(_n,2)==0
	drop star_*
	dataout, save("$tabdir/table5S_`ixFlag'.tex") tex replace
}


* Tables A8-A9: specialty FI - alternative flagging criteria
global flag_2012 impFlag2_2012
global flag_2013 impFlag2_2013
do "$rootdir/code/table4_specialty.do"

global flag_2012 impFlag3_2012
global flag_2013 impFlag3_2013
do "$rootdir/code/table4_specialty.do"

foreach ixFlag in impFlag2 impFlag3 {
	use "$savedir/table4_`ixFlag'_2012.dta", clear
	drop phyType
	order Specialty

	replace nPhyShareAll = nPhyShareAll*100
	replace fpSpec_`ixFlag'_2012 = fpSpec_`ixFlag'_2012*100
	replace fpSpec_`ixFlag'_2013 = fpSpec_`ixFlag'_2013*100
	
	qui compress
	foreach v of varlist nPhyShareAll fpSpec_`ixFlag'_2012 fpSpec_`ixFlag'_2013 {
		qui replace `v' = (int(`v'*1000)+( mod(int(`v'*10000),10)>=5) )/1000 in 1/7 // digit: 3-digits after .
		}

	tostring nPhyShareAll - fpSpec_`ixFlag'_2013 , replace force
		
	foreach v of varlist nPhyShareAll fpSpec_`ixFlag'_2012 fpSpec_`ixFlag'_2013 {
		tostring `v' , replace force
		replace `v' = substr(`v',1,strpos(`v',".")+3) if strpos(`v',".")>0 & length(`v') - strpos(`v',".") >3
		replace `v' = "" if `v'=="."
		replace `v' = "0"+`v' if strpos(`v',".")==1 in 1/7
		replace `v' = `v'+"00" if length(`v')-strpos(`v',".")==1 in 1/7
		replace `v' = `v'+"0" if length(`v')-strpos(`v',".")==2 in 1/7
		}
		
	* ... insert separator columns
	gen blank1 = ""
	gen blank2 = ""
	gen blank3 = ""
	order Specialty nPhyShareAll blank1 Num_unflagged_2012 Num_unflagged_2013 blank2 Num_flagged_2012 Num_flagged_2013 blank3 fpSpec_`ixFlag'_2012 fpSpec_`ixFlag'_2013
		
	dataout, save("$tabdir/table6S_`ixFlag'.tex") tex replace
}


* Figures A1-A2: code FI vs code Pr(flagged)
global flag_2012 impFlag2_2012
global flag_2013 impFlag2_2013
do "$rootdir/code/fig_pr_FI.do"

global flag_2012 impFlag3_2012
global flag_2013 impFlag3_2013
do "$rootdir/code/fig_pr_FI.do"

* Figures A3-A4: code FI distribution
global flag_2012 impFlag2_2012
global flag_2013 impFlag2_2013
do "$rootdir/code/fig2_hcpcs_fp.do"

global flag_2012 impFlag3_2012
global flag_2013 impFlag3_2013
do "$rootdir/code/fig2_hcpcs_fp.do"

* Figures A5-A6: distribution of hcpcs Flag Propensity by group
global flag_2012 impFlag2_2012
global flag_2013 impFlag2_2013
do "$rootdir/code/fig3_hcpcs_fp_by_flag.do"

global flag_2012 impFlag3_2012
global flag_2013 impFlag3_2013
do "$rootdir/code/fig3_hcpcs_fp_by_flag.do"

foreach ixFlag in impFlag2 impFlag3 {
	use "$savedir/table8_decomp_HRR_`ixFlag'.dta", clear
	ren meanUF2012 Unflagged2012
	ren meanUF2013 Unflagged2013
	qui compress

	qui foreach v of varlist Flagged2012 - Unflagged2013{
		replace `v' = (int(`v'*1000)+( mod(int(`v'*10000),10)>=5) )/1000
		gen str3 star_`v' = ""
		if "`v'"!="Unflagged2012" & "`v'"!="Unflagged2013" {
			replace star_`v' = "*"   if mod(_n,2)==1 & abs(`v'/`v'[_n+1])>=1.64
			replace star_`v' = "**"  if mod(_n,2)==1 & abs(`v'/`v'[_n+1])>=1.96
			replace star_`v' = "***" if mod(_n,2)==1 & abs(`v'/`v'[_n+1])>=2.576
			replace star_`v' = "" if _n==_N
			}
			
		tostring `v', replace
		replace `v' = `v'+"00" if length(`v')-strpos(`v',".")==1
		replace `v' = `v'+"0" if length(`v')-strpos(`v',".")==2
		replace `v' = "0"+`v' if strpos(`v',".")==1
		replace `v' = "-0"+substr(`v',2,.) if strpos(`v',"-.")==1
		replace `v' = "0.000" if `v'=="000"
		
		replace `v' = "["+`v'+"]" if mod(_n,2)==0
		replace `v' = `v'+star_`v'
		}
		
	replace Unflagged2012="" if mod(_n,2)==0
	replace Unflagged2013="" if mod(_n,2)==0

	drop star_*
	dataout, save("$tabdir/table8_decomp_HRR_`ixFlag'.tex") tex replace

}

* Tables A10-A11: decomposing volume byHRR
global flag_2012 impFlag2_2012
global flag_2013 impFlag2_2013
do "$rootdir/code/table5.5_decomp_HRR.do"

global flag_2012 impFlag3_2012
global flag_2013 impFlag3_2013
do "$rootdir/code/table5.5_decomp_HRR.do"

foreach ixFlag in impFlag2 impFlag3 {
	use "$savedir/table8_decomp_HRR_`ixFlag'.dta", clear
	ren meanUF2012 Unflagged2012
	ren meanUF2013 Unflagged2013
	qui compress

	foreach v of varlist Flagged2012 - Unflagged2013{
		replace `v' = (int(`v'*1000)+( mod(int(`v'*10000),10)>=5) )/1000
		gen str3 star_`v' = ""
		if "`v'"!="Unflagged2012" & "`v'"!="Unflagged2013" {
			replace star_`v' = "*"   if mod(_n,2)==1 & abs(`v'/`v'[_n+1])>=1.64
			replace star_`v' = "**"  if mod(_n,2)==1 & abs(`v'/`v'[_n+1])>=1.96
			replace star_`v' = "***" if mod(_n,2)==1 & abs(`v'/`v'[_n+1])>=2.576
			replace star_`v' = "" if _n==_N
			}
			
		tostring `v', replace
		replace `v' = `v'+"00" if length(`v')-strpos(`v',".")==1
		replace `v' = `v'+"0" if length(`v')-strpos(`v',".")==2
		replace `v' = "0"+`v' if strpos(`v',".")==1
		replace `v' = "-0"+substr(`v',2,.) if strpos(`v',"-.")==1
		replace `v' = "0.000" if `v'=="000"
		
		replace `v' = "["+`v'+"]" if mod(_n,2)==0
		replace `v' = `v'+star_`v'
	}
}
replace Unflagged2012="" if mod(_n,2)==0
replace Unflagged2013="" if mod(_n,2)==0

drop star_*
dataout, save("$tabdir/table8_decomp_HRR_impFlag1.tex") tex replace

* Tables A12-A13: Overbilling Potential Factors
global flag_2012 impFlag2_2012
global flag_2013 impFlag2_2013
do "$rootdir/code/table5.8_upcoding_factor.do"

global flag_2012 impFlag3_2012
global flag_2013 impFlag3_2013
do "$rootdir/code/table5.8_upcoding_factor.do"


foreach ixFlag in impFlag2 impFlag3 {
	use "$savedir/table9_OPF_`ixFlag'.dta", clear
	qui compress

	qui foreach v of varlist Flagged_2012 Flagged_2013 Not_flagged_2012 Not_flagged_2013 {
		replace `v' = (int(`v'*1000)+( mod(int(`v'*10000),10)>=5) )/1000
		tostring `v', replace
		replace `v' = `v'+"00" if length(`v')-strpos(`v',".")==1
		replace `v' = `v'+"0" if length(`v')-strpos(`v',".")==2
		replace `v' = "0"+`v' if strpos(`v',".")==1
		replace `v' = "-0"+substr(`v',2,.) if strpos(`v',"-.")==1
		replace `v' = "0.000" if `v'=="000"
		replace `v' = "("+`v'+")" if mod(_n,2)==0
		}

	dataout, save("$tabdir/table9_OPF_`ixFlag'.tex") tex dec(3) replace
}


* Figures A7-A8: distribution of upcoding factors
global flag_2012 impFlag2_2012
global flag_2013 impFlag2_2013
do "$rootdir/code/fig5_upcoding_factor.do"

global flag_2012 impFlag3_2012
global flag_2013 impFlag3_2013
do "$rootdir/code/fig5_upcoding_factor.do"

* Tables A14-A15: level of complexity add upcoding
global flag_2012 impFlag2_2012
global flag_2013 impFlag2_2013
do "$rootdir/code/table6_rvu_reg.do"

global flag_2012 impFlag3_2012
global flag_2013 impFlag3_2013
do "$rootdir/code/table6_rvu_reg.do"

* Figures A9-A10: comparing our study with CERT
global flag_2012 impFlag2_2012
global flag_2013 impFlag2_2013
do "$rootdir/code/fig4_hcpcs_fp_CERT.do"

global flag_2012 impFlag3_2012
global flag_2013 impFlag3_2013
do "$rootdir/code/fig4_hcpcs_fp_CERT.do"


************************************
*	Housekeepting
************************************
log close
