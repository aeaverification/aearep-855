************************************
**
** Main results under alternative flags
** Physician characteristics cond'l on HRR
**
************************************

* need -dataout- package
ssc install dataout

version 11.2
include "config.do"
clear all

local home : env HOME
if "`home'"=="" {
	local home : env HOMEPATH
}
// global rootdir "`home'/Documents/Workplace/Medicare_payment"
global datadir "$rootdir/output"
global tempdir "/tmp"
global savedir "$rootdir/output"
global tabdir "$rootdir/tex"
 
cap log close
log using "$savedir/tables_figures_supp2.txt", text replace

set more off

************************************
*	Table A17: physician characteristic comparison cond'l on HRR
************************************

foreach ixV in pc25 minimum timedOnly timedMin {
	global ixV "`ixV'"
	global flag_2012 impFlag1_2012_`ixV'
	global flag_2013 impFlag1_2013_`ixV'
	do "$rootdir/code/re_table3_phy_char.do"
}


use "$savedir/table3_impFlag1_2012_pc25.dta", clear
ren Ever pc25_ever

foreach ixV in minimum timedOnly timedMin {
	merge 1:1 _n using "$savedir/table3_impFlag1_2012_`ixV'.dta"
	ren Ever `ixV'_ever
	drop _merge
	}	
	
qui compress	

local last = _N
qui foreach v of varlist pc25_ever - timedMin_ever {
	replace `v' = (int(`v'*1000)+( mod(int(`v'*10000),10)>=5) )/1000
	gen str3 star_`v' = ""
	if "`v'"!="meanNever" {
		replace star_`v' = "*"   if mod(_n,2)==1 & abs(`v'/`v'[_n+1])>=1.64
		replace star_`v' = "**"  if mod(_n,2)==1 & abs(`v'/`v'[_n+1])>=1.96
		replace star_`v' = "***" if mod(_n,2)==1 & abs(`v'/`v'[_n+1])>=2.576
		replace star_`v' = "" if _n==_N
		}
		
	tostring `v', replace
	replace `v' = `v'+"00" if length(`v')-strpos(`v',".")==1
	replace `v' = `v'+"0" if length(`v')-strpos(`v',".")==2
	replace `v' = "0"+`v' if strpos(`v',".")==1
	replace `v' = "-0"+substr(`v',2,.) if strpos(`v',"-.")==1
	replace `v' = "0.000" if `v'=="000"
	
	replace `v' = "["+`v'+"]" if mod(_n,2)==0
	replace `v' = `v'+star_`v'
	}
	
drop star_*

order r pc25_ever minimum_ever timedOnly_ever timedMin_ever
dataout, save("$tabdir/re_table5_alter.tex") tex replace


************************************
*	Table A18: Specialties 
************************************
foreach ixV in pc25 minimum timedOnly timedMin {
	global ixV "`ixV'"
	global flag_2012 impFlag1_2012_`ixV'
	global flag_2013 impFlag1_2013_`ixV'
	do "$rootdir/code/re_table4_specialty.do"
}


* merge list using various flag measures
use "$savedir/re_table6_impFlag1_2012_pc25.dta", clear
keep phyType Specialty Num_unflagged_2012 Num_flagged_2012 fpSpec_impFlag1_2012_pc25
ren Num_unflagged_2012 nUF12_pc25
ren Num_flagged_2012 nF12_pc25
ren fpSpec_impFlag1_2012_pc25 sfp12_pc25

foreach ixV in minimum timedOnly timedMin {
	merge 1:1 phyType using "$savedir/re_table6_impFlag1_2012_`ixV'.dta", ///
		keepusing(Specialty Num_unflagged_2012 Num_flagged_2012 fpSpec_impFlag1_2012_`ixV')
	drop _merge
	
	ren Num_unflagged_2012 nUF12_`ixV'
	ren Num_flagged_2012 nF12_`ixV'
	ren fpSpec_impFlag1_2012_`ixV' sfp12_`ixV'
	
	}	

* clean
qui compress
drop phyType
order Specialty
gsort -sfp12_pc25

foreach v of varlist sfp12_pc25 sfp12_minimum sfp12_timedOnly sfp12_timedMin {
	replace `v' = `v'*100
	qui replace `v' = (int(`v'*1000)+( mod(int(`v'*10000),10)>=5) )/1000 in 1/7 // digit: 3-digits after .
	}


tostring nUF* nF*, replace force
	
foreach v of varlist sfp12_pc25 sfp12_minimum sfp12_timedOnly sfp12_timedMin {
	tostring `v' , replace force
	replace `v' = substr(`v',1,strpos(`v',".")+3) if strpos(`v',".")>0 & length(`v') - strpos(`v',".") >3
	replace `v' = "" if `v'=="."
	replace `v' = "0"+`v' if strpos(`v',".")==1 in 1/7
	replace `v' = `v'+"00" if length(`v')-strpos(`v',".")==1 in 1/7
	replace `v' = `v'+"0" if length(`v')-strpos(`v',".")==2 in 1/7
	}
	
* ... insert separator columns
gen blank1 = ""
gen blank2 = ""
gen blank3 = ""
order Specialty nUF12_pc25 nF12_pc25 sfp12_pc25 blank1 ///
	nUF12_minimum nF12_minimum sfp12_minimum blank2 ///
	nUF12_timedOnly nF12_timedOnly sfp12_timedOnly blank3 ///
	nUF12_timedMin nF12_timedMin sfp12_timedMin

dataout, save("$tabdir/re_table6_alter.tex") tex replace


************************************
*	Table A19: decomposing volume (cond't on HRR)
************************************

foreach ixV in pc25 minimum timedOnly timedMin {
	global ixV "`ixV'"
	global flag_2012 impFlag1_2012_`ixV'
	global flag_2013 impFlag1_2013_`ixV'
	do "$rootdir/code/re_table5_decomp_HRR.do"
}

use "$savedir/table8_decomp_HRR_impFlag1_2012_pc25.dta", clear
ren Flagged2012 pc25_F12
ren Flagged2013 pc25_F13

foreach ixV in minimum timedOnly timedMin {
	merge 1:1 _n using "$savedir/table8_decomp_HRR_impFlag1_2012_`ixV'.dta"
	ren Flagged2012 `ixV'_F12
	ren Flagged2013 `ixV'_F13
	drop _merge
	}	

qui compress

qui foreach v of varlist pc25_F12 - timedMin_F13 {
	replace `v' = (int(`v'*1000)+( mod(int(`v'*10000),10)>=5) )/1000
	gen str3 star_`v' = ""
	if "`v'"!="Unflagged2012" & "`v'"!="Unflagged2013" {
		replace star_`v' = "*"   if mod(_n,2)==1 & abs(`v'/`v'[_n+1])>=1.64
		replace star_`v' = "**"  if mod(_n,2)==1 & abs(`v'/`v'[_n+1])>=1.96
		replace star_`v' = "***" if mod(_n,2)==1 & abs(`v'/`v'[_n+1])>=2.576
		replace star_`v' = "" if _n==_N
		}
		
	tostring `v', replace
	replace `v' = `v'+"00" if length(`v')-strpos(`v',".")==1
	replace `v' = `v'+"0" if length(`v')-strpos(`v',".")==2
	replace `v' = "0"+`v' if strpos(`v',".")==1
	replace `v' = "-0"+substr(`v',2,.) if strpos(`v',"-.")==1
	replace `v' = "0.000" if `v'=="000"
	
	replace `v' = "["+`v'+"]" if mod(_n,2)==0
	replace `v' = `v'+star_`v'
	}
	

drop star_*
drop *F13
dataout, save("$tabdir/re_table8_alter.tex") tex replace


************************************
*	Table A20: Overbilling Potential Factors
************************************
foreach ixV in pc25 minimum timedOnly timedMin {
	global ixV "`ixV'"
	global flag_2012 impFlag1_2012_`ixV'
	global flag_2013 impFlag1_2013_`ixV'
	do "$rootdir/code/re_table9_OPF.do"
}


use "$savedir/table9_OPF_impFlag1_2012_pc25.dta", clear
ren Flagged 	pc25_F
ren Not_flagged pc25_NF

foreach ixV in minimum timedOnly timedMin {
	merge 1:1 _n using "$savedir/table9_OPF_impFlag1_2012_`ixV'.dta"
	ren Flagged `ixV'_F
	ren Not_flagged `ixV'_NF
	drop _merge
	}	

qui compress

qui foreach v of varlist pc25_F - timedMin_NF {
	replace `v' = (int(`v'*1000)+( mod(int(`v'*10000),10)>=5) )/1000
	tostring `v', replace
	replace `v' = `v'+"00" if length(`v')-strpos(`v',".")==1
	replace `v' = `v'+"0" if length(`v')-strpos(`v',".")==2
	replace `v' = "0"+`v' if strpos(`v',".")==1
	replace `v' = "-0"+substr(`v',2,.) if strpos(`v',"-.")==1
	replace `v' = "0.000" if `v'=="000"
	replace `v' = "("+`v'+")" if mod(_n,2)==0
	}

dataout, save("$tabdir/re_table9_OPF_alter.tex") tex replace



************************************
*	Tables A21-A24: level of complexity add upcoding
************************************

foreach ixV in pc25 minimum timedOnly timedMin {
	global ixV "`ixV'"
	global flag_2012 impFlag1_2012_`ixV'
	global flag_2013 impFlag1_2013_`ixV'
	do "$rootdir/code/re_table10_reg.do"
}


************************************
*	Housekeepting
************************************
log close
