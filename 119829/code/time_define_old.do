************************************
****							****
****    		time_define.do		****
****							****
************************************

************************************
*		housekeeping
************************************

clear all

include "config.do"

// global rootdir "/medicare/physician times"
global datadir "$rootdir/input"
global tempdir "$rootdir/temp"
cap mkdir $tempdir
global savedir "$rootdir/output"


set more off


************************************
*	HCPCS list: time per service
************************************
* list of E/M codes
insheet using "$datadir/hcpcslist_timed_with_min.csv", clear
drop hcpcs1
rename typical timeIntraTyp
recode timeIntraTyp(0=.)
rename expected timeIntra
recode timeIntra(0=.)
rename complexity codeComplx
rename numcomplex nComplx
rename hcpcsgr EMgroup
rename isem isEM

label variable timeIntraTyp "AMA suggested time in minutes"
label variable timeIntra "expected time (lower bound) in minutes"
label variable codeComplx "level of complexity (1=lowest)"
label variable nComplx "number of complexity levels in total (3/4/5)"
label variable EMgroup "group no. of hcpcs"
label variable isEM "indicator: E/M codes"
label define isEMlab 0 "non-E/M" 1 "E/M"
label values isEM isEMlab

gen timeFlag = 1

tostring hcpcs, replace
sort hcpcs
save "$tempdir/hcpcslist_timed.dta", replace

* list of codes with objectively measured time (Zuckerman2014)
insheet using "$rawdir/hcpcslist_untimed_survey_old.csv", clear
rename intra timeIntra
rename total timeTotal
gen timeFlag = 2
label variable timeFlag "Flag: how time is measured"

append using "$tempdir/hcpcslist_timed.dta"
sort hcpcs
save "$tempdir/hcpcslist_timed_untimed.dta", replace

* work RVU data
insheet using "$datadir/PPRRVU12.csv", clear
keep hcpcs workrvu
rename workrvu wRVU2012
gsort hcpcs -wRVU
duplicates drop hcpcs, force
save "$tempdir/hcpcslist_wrvu2012.dta", replace

insheet using "$datadir/PPRRVU13.csv", clear
keep hcpcs workrvu
rename workrvu wRVU2013
gsort hcpcs -wRVU
duplicates drop hcpcs, force
save "$tempdir/hcpcslist_wrvu2013.dta", replace

merge 1:1 hcpcs using "$tempdir/hcpcslist_wrvu2012.dta"
drop _merge

save "$tempdir/hcpcslist_wrvu.dta", replace

* all merge to master data: list of hcpcs in our sample
use "$savedir/hcpcs_list.dta", clear
rename hcpcsGroup hcpcsCateg

merge 1:1 hcpcs using "$tempdir/hcpcslist_timed_untimed.dta"
replace timeFlag = 0 if _merge == 1
drop if _merge == 2
drop _merge hcpcsdesc
rename hcpcs_description hcpcsDesc

merge 1:1 hcpcs using "$tempdir/hcpcslist_wrvu.dta"
drop if _merge == 2
drop _merge

* only keep Level I, Category I codes
* ...(level II = products; level I, cttegory III no rvu info or can't be reliably imputed)
keep if hcpcsCateg == 1
replace isEM = 0 if isEM == .

* AND only keep hcpcs that is not a drug
keep if isDrug==0

* grouping of codes (according to Gabbert2012)
gen codeGroup = .
label variable codeGroup "15 groups of level I, category I codes"
	replace codeGroup = 1 if hcpcs>="00100" & hcpcs<="01999"
	replace codeGroup = 2 if hcpcs>="10021" & hcpcs<="19499"
	replace codeGroup = 3 if hcpcs>="20005" & hcpcs<="29999"
	replace codeGroup = 4 if hcpcs>="30000" & hcpcs<="39999"
	replace codeGroup = 5 if hcpcs>="33010" & hcpcs<="39599"
	replace codeGroup = 6 if hcpcs>="40490" & hcpcs<="49999"
	replace codeGroup = 7 if hcpcs>="50010" & hcpcs<="53899"
	replace codeGroup = 8 if hcpcs>="54000" & hcpcs<="59899"
	replace codeGroup = 9 if hcpcs>="60000" & hcpcs<="60699"
	replace codeGroup = 10 if hcpcs>="61000" & hcpcs<="64999"
	replace codeGroup = 11 if hcpcs>="65091" & hcpcs<="69990"
	replace codeGroup = 12 if hcpcs>="70010" & hcpcs<="79999"
	replace codeGroup = 13 if hcpcs>="80047" & hcpcs<="89398"
	replace codeGroup = 14 if hcpcs>="90281" & isEM == 0
	replace codeGroup = 15 if isEM == 1
# delimit ;
label define codeGrouplab  1 "anesthesia" 
						2 "integumentary sys"
						3 "musculoskeletal sys"
						4 "respiratory sys"
						5 "cardiovascular/hemic/lymphatic/mediastinum"
						6 "digestive"
						7 "urinary sys"
						8 "genital sys"
						9 "endocrine sys"
						10 "nervous sys"
						11 "eye/ocular adnexa/auditory sys"
						12 "radiology"
						13 "pathology and lab"
						14 "medicine"
						15 "e/m";
# delimit cr
label values codeGroup codeGrouplab
tab codeGroup
drop isEM

* impute time I: average within group
preserve
gen tIntPerRVU = timeIntra/wRVU2012
replace tIntPerRVU = timeIntra/wRVU2013 if tIntPerRVU==.

gen tTotPerRVU = timeTotal/wRVU2012
replace tTotPerRVU = timeTotal/wRVU2013 if tTotPerRVU==.

collapse (mean) tIntPerRVU tTotPerRVU, by(codeGroup)
sort codeGroup
save "$tempdir/timePerRVU.dta", replace
restore

sort codeGroup
merge m:1 codeGroup using "$tempdir/timePerRVU.dta"
assert _merge==3
drop _merge

gen timeIntra1 = wRVU2012*tIntPerRVU if timeIntra==.
replace timeIntra1 = wRVU2013*tIntPerRVU if timeIntra==. & timeIntra1==.
gen timeTotal1 = wRVU2012*tTotPerRVU if timeTotal==.
replace timeTotal1 = wRVU2013*tTotPerRVU if timeTotal==. & timeTotal1==.

drop tIntPerRVU tTotPerRVU

* impute time II: regression within group
preserve
rename timeIntra time
xi: reg wRVU2012 i.codeGroup*time, noconst
est store impute2012a

xi: reg wRVU2013 i.codeGroup*time, noconst
est store impute2013a
drop time

rename timeTotal time
xi: reg wRVU2012 i.codeGroup*time, noconst
est store impute2012b

xi: reg wRVU2013 i.codeGroup*time, noconst
est store impute2013b

esttab impute20* using "$tempdir/timeImputeReg.csv", ///
	not nostar noobs nogaps nolines nomtitle nodep nonum nonote replace

insheet using "$tempdir/timeImputeReg.csv", clear
rename v1 codeGroup
rename v2 betaIn
rename v3 betaIn2013
rename v4 betaTot
rename v5 betaTot2013

foreach v of varlist betaIn betaIn2013 betaTot betaTot2013 {
	replace `v'=. if strpos(codeGroup,"o._Icod")>0 | `v'==0
}
replace betaIn  = betaIn2013 if betaIn==.
replace betaTot = betaTot2013 if betaTot==.
drop betaIn2013 betaTot2013
drop if betaIn==. & betaTot==.

replace codeGroup="_IcodXtime_1" if codeGroup=="time"
destring codeGroup, replace ignore("_IcodeGroup_" "o._IcodeGroup_" "_IcodXtime_" "o._IcodXtime_")

egen separator = max( _n*(codeGroup>codeGroup[_n+1]) )	//detect the last _n of beta0's
gen isInter = (_n>separator)
drop separator
reshape wide betaIn betaTot, i(codeGroup) j(isInter)
replace betaIn0=0 if codeGroup==1
replace betaTot0=0 if codeGroup==1
replace betaIn1=0 if betaIn1==. & betaIn0!=.
replace betaTot1=0 if betaTot1==. & betaTot0!=.
replace betaIn1=betaIn1+betaIn1[1] if _n>1
replace betaTot1=betaTot1+betaTot1[1] if _n>1

order codeGroup betaIn0 betaIn1 betaTot0 betaTot1
sort codeGroup
save "$tempdir/timeImputeReg.dta", replace
restore	

* combine the two imputed time measures
sort codeGroup
merge m:1 codeGroup using "$tempdir/timeImputeReg.dta"
drop _merge

gen timeIntra2 = (wRVU2012-betaIn0)/betaIn1 if wRVU2012!=0 & timeIntra!=.
replace timeIntra2 = (wRVU2013-betaIn0)/betaIn1 if timeIntra2==. & wRVU2013!=0 & timeIntra!=.
gen timeTotal2 = (wRVU2012-betaTot0)/betaTot1 if wRVU2012!=0 & timeTotal!=.
replace timeTotal2 = (wRVU2013-betaTot0)/betaTot1 if timeTotal2==. & wRVU2013!=0 & timeTotal!=.

gen tIntra = timeIntra 
	replace tIntra = max(0,timeIntra1) if timeIntra1!=. & timeIntra2==.
	replace tIntra = max(0,timeIntra2) if timeIntra1==. & timeIntra2!=.
	replace tIntra = min(max(0,timeIntra1),max(0,timeIntra2)) if tIntra==.
gen tTotal = timeTotal
	replace tTotal = max(0,timeTotal1) if timeTotal1!=. & timeTotal2==.
	replace tTotal = max(0,timeTotal2) if timeTotal1==. & timeTotal2!=.
	replace tTotal = min(max(0,timeTotal1),max(0,timeTotal2)) if tTotal==.
	replace tTotal = tIntra if tTotal<tIntra

gen tIntra85 = (codeGroup!=15)*tIntra*0.85 + (codeGroup==15)*tIntra
gen tTotal85 = (codeGroup!=15)*tTotal*0.85 + (codeGroup==15)*tTotal


local keepvar hcpcs hcpcsCateg hcpcsDesc codeGroup EMgroup codeComplx nComplx ///
	wRVU2012 wRVU2013 tIntra tTotal tIntra85 tTotal85
keep `keepvar'
order `keepvar'

label variable wRVU2012 "work RVU (PFS 2012)"
label variable wRVU2013 "work RVU (PFS 2013)"
label variable tIntra "intra-service time (imputed)"
label variable tIntra85 "tIntra*85% (if non-EM)"
label variable tTotal "total service time (imputed)"
label variable tTotal85 "tTotal*85% (if non-EM)"

save "$savedir/servTimeListold.dta", replace

************************************
*		housekeeping
************************************

*log close



