************************************
****							****
****    		time_flag.do			****
****							****
************************************
* The original verion of the program with no changes, except to filenames

************************************
*       housekeeping
************************************
clear all


include "config.do"
// global rootdir "/medicare/physician times"
// global datadir "$rootdir/input"
//global tempdir "$rootdir/temp"
//global savedir "$rootdir/output"



set more off

************************************
*	get phy-hcpcs-level time info
************************************
* load physician-service-place level data
use "$savedir/phy_serv_merged.dta", clear

* merge with hcpcs-level time-needed info
merge m:1 hcpcs using "$savedir/servTimeListold.dta"
keep if _merge == 3	// _m=2: no such hcpcs filed, _m=1: the hcpcs is not Level I, Category 1
drop _merge
assert isDrug == 0	// hcpcs is not a drug

egen anyEM = max(codeGroup==15), by(npi)	//see if the phy has ANY E/M hcpcs codes
label variable anyEM "physician has filed any E/M codes"
tab anyEM

* calculate total time needed
gen intHrs_2012   = nServ*tIntra/60
gen intHrs85_2012 = nServ*tIntra85/60
gen totHrs_2012   = nServ*tTotal/60
gen totHrs85_2012 = nServ*tTotal85/60

gen intHrs_2013   = nServ2013*tIntra/60
gen intHrs85_2013 = nServ2013*tIntra85/60
gen totHrs_2013   = nServ2013*tTotal/60
gen totHrs85_2013 = nServ2013*tTotal85/60

forvalues yr = 2012/2013 {
	label variable intHrs_`yr' "(nServ)*(intra-service time) in hours, `yr'"
	label variable intHrs85_`yr' "(nServ)*(intra-service time*85%) in hours, `yr'"
	label variable totHrs_`yr' "(nServ)*(total service time) in hours, `yr'"
	label variable totHrs85_`yr' "(nServ)*(total service time*85%) in hours, `yr'"
}


* NEW: only keep physicians who appear in both years
recode nServ(.=0)
recode nServ2013(.=0)

egen phyIn2012 = max(nServ>0), by(npi)
egen phyIn2013 = max(nServ2013>0), by(npi)
tab phyIn2012 phyIn2013
keep if phyIn2012==1 & phyIn2013==1
drop phyIn2012 phyIn2013

save "$savedir/phy_serv_time_old.dta", replace
sum
codebook npi hcpcs, compact

*See how many services IN SAMPLE do not have a time estimate
preserve
keep hcpcs tIntra
duplicates drop hcpcs, force
gen noTime = (tIntra==0)
tab noTime
restore

************************************
*	collapse to phy-level 
************************************
* collapse the rest to physician-level
*  ... first extract physician personal info
preserve

duplicates drop npi, force
keep npi male phyCred multiCred isMD phySpec phyType phyZip GPCIregion GPCIregion_vague ///
	medSchool gradYear groupPAC nPhyInGroup addState ///
	inMedicare inERX inPQRS inEHR hosAff experYear anyEM
sort npi
save "$tempdir/phy_info.dta", replace

restore

* ... then collapse to get other physician info
sort npi hcpcs inOffice
gen isNewHcpcs_2012 = (npi!=npi[_n-1] & nServ>0) | (npi==npi[_n-1] & hcpcs!=hcpcs[_n-1] & nServ>0) // indicator: a new hcpcs for this npi
gen isNewHcpcs_2013 = (npi!=npi[_n-1] & nServ2013>0) | (npi==npi[_n-1] & hcpcs!=hcpcs[_n-1] & nServ2013>0) // indicator: a new hcpcs for this npi
gen isEM = (codeGroup==15)
gen isNewEM_2012 = isEM*isNewHcpcs_2012
gen isNewEM_2013 = isEM*isNewHcpcs_2013
gen nServEM_2012 = isEM*nServ
gen nServEM_2013 = isEM*nServ2013
gen nBeneEM_2012 = isEM*nBene
gen nBeneEM_2013 = isEM*nBene2013
gen totPaid_2012 = nServ*meanPaid/1000
gen totPaid_2013 = nServ2013*meanPaid2013/1000
gen totPaidEM_2012 = isEM*nServ*meanPaid/1000
gen totPaidEM_2013 = isEM*nServ2013*meanPaid2013/1000
gen totAllow_2013 =nServ2013*meanAllow2013/1000 // (Brett added this line)
gen totwRVU_2013=nServ2013*wRVU2013 // (Brett added this line)
gen totwRVU1_2013=nServUniq2013*wRVU2013 // (Brett added this line)

collapse (sum) nCode_2012 = isNewHcpcs_2012 nCode_2013 = isNewHcpcs_2013 ///
			   nCodeEM_2012 = isNewEM_2012 nCodeEM_2013 = isNewEM_2013 ///
			   nServ_2012 = nServ nServ_2013 = nServ2013 ///
			   nServEM_2012 nServEM_2013 ///
			   nBene_2012 = nBene nBene_2013 = nBene2013 ///
			   nBeneEM_2012 nBeneEM_2013 ///
			   intHrs_2012 intHrs_2013 intHrs85_2012 intHrs85_2013 ///
			   totHrs_2012 totHrs_2013 totHrs85_2012 totHrs85_2013 ///
			   totPaid_2012 totPaid_2013 totPaidEM_2012 totPaidEM_2013 ///
			   totAllow_2013 totwRVU_2013 totwRVU1_2013 nServUniq2013 /// (Brett added this line too)
			   , by(npi)
			   
* ... merge the two
sort npi
merge 1:1 npi using "$tempdir/phy_info.dta"
drop _merge

forval yr = 2012/2013 {
	gen intHrsPerWk_`yr'   = intHrs_`yr'/51
	gen intHrs85PerWk_`yr' = intHrs85_`yr'/51
	gen totHrsPerWk_`yr'   = totHrs_`yr'/51
	gen totHrs85PerWk_`yr' = totHrs85_`yr'/51
}

forval yr = 2012/2013 {
	label variable nCode_`yr' 		"number of distinct hcpcs codes filed"
	label variable nCodeEM_`yr' 	"number of distinct E/M hcpcs codes filed"
	label variable nServ_`yr'		"number of services provided"
	label variable nServEM_`yr' 	"number of E/M services provided"
	label variable nBene_`yr'		"number of distinct(*) beneficiaries"
	label variable nBeneEM_`yr'		"number of distinct(*) beneficiaries for E/M services"
	label variable intHrs_`yr'		"intra-service time (in hours)"
	label variable intHrs85_`yr'	"intra-service time*85% (in hours)"
	label variable totHrs_`yr'		"total service time (in hours)"
	label variable totHrs85_`yr'	"total service time*85% (in hours)"
	label variable intHrsPerWk_`yr'	 	"intra-service hours/week"
	label variable intHrs85PerWk_`yr' 	"intra-service hours*85%/week"
	label variable totHrsPerWk_`yr'	 	"total service hours/week"
	label variable totHrs85PerWk_`yr' 	"total service hours*85%/week"
	label variable totHrs85PerWk_`yr' 	"total service hours*85%/week"
	label variable totPaid_`yr' 	"total Medicare payment"
	label variable totPaidEM_`yr'	"total Medicare payment on E/M services"
}

* flag implausible cases
forval yr = 2012/2013 {
	gen impFlag0_`yr' = ( totHrsPerWk_`yr'> 80 )
	gen impFlag1_`yr' = ( totHrsPerWk_`yr'>100 )
	gen impFlag2_`yr' = ( totHrsPerWk_`yr'>112 )
	gen impFlag3_`yr' = ( totHrsPerWk_`yr'>168 )
    
	// Brett added-flagged based on intraservice time - for appendix stuff
	gen impFlag0_`yr'_int = ( intHrsPerWk_`yr'> 80 )
	gen impFlag1_`yr'_int = ( intHrsPerWk_`yr'>100 )
	gen impFlag2_`yr'_int = ( intHrsPerWk_`yr'>112 )
	gen impFlag3_`yr'_int = ( intHrsPerWk_`yr'>168 )	

	label variable impFlag0_`yr' "FLAG: > 80 hours/week"
	label variable impFlag1_`yr' "FLAG: >100 hours/week"
	label variable impFlag2_`yr' "FLAG: >112 hours/week (16hr/day)"
	label variable impFlag3_`yr' "FLAG: >168 hours/week (24/7)"
}


			
************************************
*	wrap up and save
************************************

order npi impFlag* nCode_* nCodeEM_* nServ_* nServEM_* nBene_* nBeneEM_* ///
	intHrs_* intHrs85_* totHrs_* totHrs85_* intHrsPerWk_* intHrs85PerWk_* totHrsPerWk_* totHrs85PerWk_* totPaid_* totPaidEM_* ///
	male phyCred isMD multiCred medSchool gradYear experYear ///
	phySpec phyType groupPAC nPhyInGroup hosAff ///
	inMedicare inERX inPQRS inEHR anyEM phyZip GPCIregion GPCIregion_vague addState totAllow_2013 totwRVU_2013 totwRVU1_2013 nServUniq2013

save "$savedir/phy_time_old.dta", replace

drop impFlag0_2012 impFlag0_2013 impFlag1_2012 impFlag1_2013 impFlag2_2012 impFlag2_2013 impFlag3_2012 impFlag3_2013
rename (impFlag0_2012_int impFlag0_2013_int impFlag1_2012_int impFlag1_2013_int) (impFlag0_2012 impFlag0_2013 impFlag1_2012 impFlag1_2013) 
rename (impFlag2_2012_int impFlag2_2013_int impFlag3_2012_int impFlag3_2013_int) (impFlag2_2012 impFlag2_2013 impFlag3_2012 impFlag3_2013)

save "$savedir/phy_time_oldint.dta", replace 

*keep people flagged in 2013
keep if impFlag1_2013==1
keep npi intHrsPerWk_2013 totHrsPerWk_2013 

save "$savedir/flaggedlist_old.dta",replace //flagged (100+) physicians in 2013

keep npi

save "$savedir/flaggedlist_npi_old.dta",replace
