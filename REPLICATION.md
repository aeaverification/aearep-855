# AER comment AER-2018-0812: Fang and Gong Validation and Replication results

You may want to consult [Unofficial Verification Guidance](https://social-science-data-editors.github.io/guidance/Verification_guidance.html) for additional tips and criteria.

SUMMARY
-------

The author's replication is very well structured, and well explained. All data are cited. Some of the data is restricted-access, so no main text tables can be reproduced. The author didn't write code to produce any of the tables - they are constructed by hand. Appendix code was run, generating tables A10-A12. 

General
-------

There are several instances in the manuscript where the HCPCS abbreviation is written in lower-case.

Data description
----------------

### Data Sources

- Zuckerman et al (2016)
  - cited
  - provided
- Fang and Gong (2017)
  - paper cited
  - appendix cited in README
- 5% Carrier Standard Analytic File Limited Data (CMS)
  - restricted-access, detailed in README
  - cited in README
- NATIONAL PHYSICIAN FEE SCHEDULE RELATIVE VALUE FILE CALENDAR YEAR 2013 (CMS 2013a)
  - URL provided
  - cited
  - provided
- Utilization data from the 100% claims aggregate summary tables for physicians (CMS 2013b):
  - URL provided
  - cited
  - provided

### Analysis Data Files


- [ ] No analysis data file mentioned
- [X] Analysis data files mentioned, not provided (explain reasons below)
- [X] Analysis data files mentioned, provided. File names listed below.

Some files not provided (see above).

```
data/PPRRVU13_V0828.xlsx
data/RVUPUF13.docx
data/hcpcslist_untimed_survey.csv
data/hcpcslist_untimed_survey1.csv
data/hcpcslist_untimed_survey_old.csv
data/partial_service.csv
data/phys_totals.dta
```

### ICPSR data deposit

#### Requirements 

- [X] README is in TXT, MD, PDF format
- [X] openICPSR deposit has no ZIP files
- [X] Title conforms to guidance (starts with "Data and Code for:", is properly capitalized)
- [X] Authors (with affiliations) are listed in the same order as on the paper

#### Deposit Metadata

- [X] JEL Classification (required)
- [X] Manuscript Number (required)
- [ ] Subject Terms (highly recommended)
- [X] Geographic coverage (highly recommended)
- [X] Time period(s) (highly recommended)
- [ ] Collection date(s) (suggested)
- [ ] Universe (suggested)
- [X] Data Type(s) (suggested)
- [ ] Data Source (suggested)
- [ ] Units of Observation (suggested)


- [NOTE] openICPSR metadata is sufficient.

For additional guidance, see [https://aeadataeditor.github.io/aea-de-guidance/data-deposit-aea-guidance.html](https://aeadataeditor.github.io/aea-de-guidance/data-deposit-aea-guidance.html).



Data checks
-----------

All posted data are from public websites. Stata files have labels.

Code description
----------------

- original code by Fang and Gong is required. Not described here.
- 18 Stata do files. 14 are described in the README
- Code requires manual changes

The README "roughly" (author's words) identify where Tables are written or created. 

> [We REQUESTED] Please provide a mapping of programs to figures/tables. (This may point to partial tables)

- Done. Author has stated that tables are constructed manually. Programs create the (raw) appendix tables. The main manuscript tables are constructed by hand.

Stated Requirements
---------------------


- [ ] No requirements specified
- [x] Software Requirements specified as follows:
   - Stata 14
- [x] Computational Requirements specified as follows:
   - single core, 64GB (probably less)
- [ ] Time Requirements specified as follows:
   - Length of necessary computation (hours, weeks, etc.)


Replication steps
-----------------

1. Downloaded code from author and from Fang and Gong, unzipped in common directory.
2. Added the config.do generating system information. Added it to every program, and commented out other rootdir definitions:
```
 1063  sed -i 's+global rootdir+// global rootdir+' *do
 1065  sed -i 's+version 11.2+version 11.2\ninclude "config.do"+' *do
```
3. Installed `estout`, as per README/ setup.do
4. Ran code as per README
```
 1075  stata14 -b do loadPhyServ.do 
 1077  stata14 -b do loadAllPhysician.do 
 1079  stata14 -b do loadPhyFeeSchedule.do 
 1081  stata14 -b do mergePhyServ_AllPhysician.do 
```
Error found in `time_flag_old.do`:
```
. gen totAllow_2013 =nServ2013*meanAllow2013/1000 // (Brett added this line)
meanAllow2013 not found
r(111);
```
Error found in `time_flag.do`:
```
   .../code/../output/servTimeList1.dta not found
r(601);
```
> Solution: created a Linux symlink from servTimeList.dta to servTimeList1.dta

Error found in `time_flag.do`:
```
meanAllow2013 not found
r(111);
```
5. Author provided new code `loadPhyServ_1.do`
6. Generated and ran a `[master.sh](https://bitbucket.org/aeaverification/aearep-855/src/master/119829/code/master.sh)`
```{bash}
#!/bin/bash
stata14 -b do loadPhyServ_1.do
stata14 -b do loadAllPhysician.do
stata14 -b do loadPhyFeeSchedule.do
stata14 -b do mergePhyServ_AllPhysician.do
stata14 -b do time_define_old.do
stata14 -b do time_define.do
stata14 -b do time_flag_old.do
stata14 -b do time_flag.do
stata14 -b do time_flag_new.do
stata14 -b do tables.do
```


> [We REQUESTED] Please add a setup program that installs all packages as noted above. Please specify all necessary commands. An example of a setup file can be found at https://github.com/gslab-econ/template/blob/master/config/config_stata.do

- Done.

Computing Environment of the Replicator
---------------------

"openSUSE Leap 15.1"
 Intel(R) Core(TM) i7 CPU         980  @ 3.33GHz, 12 cores
23GB memory

- Stata/MP 16 (and 14)

Findings
--------

> [We REQUESTED] Please update openICPSR repository with updated code as provided to Data Editor

Done.

### Data Preparation Code

Once provided, data preparation code ran fine.

### Tables

Tables 1-5 are generated from confidential data, and could not be reproduced. Tables A10-A12 were identified in the code, and run. 


### Figures

No figures in manuscript.

### In-Text Numbers

Not checked.

Classification
--------------

Part of the data is not available (restricted access). Table output is not clear. Data creation programs for Fang and Gong data work.


- [ ] full replication
- [ ] full replication with minor issues
- [X] partial replication (see above)
- [ ] not able to replicate most or all of the results (reasons see above)
